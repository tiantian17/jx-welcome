/*
 Navicat Premium Data Transfer

 Source Server         : Localhost
 Source Server Type    : MySQL
 Source Server Version : 80030
 Source Host           : localhost:3306
 Source Schema         : jx-welcome

 Target Server Type    : MySQL
 Target Server Version : 80030
 File Encoding         : 65001

 Date: 12/11/2023 21:22:14
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for sys_invite
-- ----------------------------
DROP TABLE IF EXISTS `sys_invite`;
CREATE TABLE `sys_invite`  (
  `id` int NOT NULL COMMENT '主键',
  `invite_code` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '邀请码',
  `user_id` bigint NOT NULL COMMENT '用户ID 创建人',
  `role_id` int NOT NULL COMMENT '角色Id',
  `user_type` varchar(2) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '用户类型 00协会内用户 01新人用户',
  `remark` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '解释',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sys_invite
-- ----------------------------
INSERT INTO `sys_invite` VALUES (1, 'A2B6MT9Mli', 1, 1, '00', '分配管理员权限');
INSERT INTO `sys_invite` VALUES (2, 'U7heGwFbhw', 1, 2, '00', '分配普通用户权限');
INSERT INTO `sys_invite` VALUES (3, 'bk5gnRByoK', 1, 3, '01', '分配新人权限');

-- ----------------------------
-- Table structure for sys_judger
-- ----------------------------
DROP TABLE IF EXISTS `sys_judger`;
CREATE TABLE `sys_judger`  (
  `judger_id` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '主键',
  `user_id` bigint NOT NULL COMMENT '判题人的ID (user_id)',
  `judger_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '判踢人的姓名',
  `status` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '判题状态( 0:未完成 1:进行中 2全部:已完成)',
  `reply_ids` varchar(3000) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '判断的答题 逗号分隔',
  `reply_users` varchar(3000) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '被判题的用户ID 逗号分隔',
  `del_flag` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT '0' COMMENT '删除标记 (0:未删除 1:已删除)',
  `create_by` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '创建人',
  `create_time` datetime NULL DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '更新人',
  `update_time` datetime NULL DEFAULT NULL COMMENT '更新时间',
  PRIMARY KEY (`judger_id`, `judger_name`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci ROW_FORMAT = Dynamic;


-- ----------------------------
-- Table structure for sys_notice
-- ----------------------------
DROP TABLE IF EXISTS `sys_notice`;
CREATE TABLE `sys_notice`  (
  `notice_id` int NOT NULL AUTO_INCREMENT COMMENT '公告ID',
  `title` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '公告标题',
  `level` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '通知程度(0:普通 1:警告 2:紧急通知)',
  `html_content` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '公告内容(HTML格式)',
  `width` int NULL DEFAULT NULL COMMENT '窗口显示宽度',
  `duration` int NULL DEFAULT NULL COMMENT '显示时长',
  `create_by` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT '' COMMENT '更新者(真实姓名)',
  `create_time` datetime NULL DEFAULT NULL COMMENT '更新时间',
  `remark` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '备注',
  PRIMARY KEY (`notice_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 37 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci COMMENT = '通知公告表' ROW_FORMAT = Dynamic;


-- ----------------------------
-- Table structure for sys_oper_log
-- ----------------------------
DROP TABLE IF EXISTS `sys_oper_log`;
CREATE TABLE `sys_oper_log`  (
  `oper_id` bigint NOT NULL AUTO_INCREMENT COMMENT '日志主键',
  `title` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT '' COMMENT '模块标题',
  `business_type` int NULL DEFAULT 0 COMMENT '业务类型（0其它 1新增 2修改 3删除 4登录 5登出 6录取 7未录取）',
  `method` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT '' COMMENT '方法名称',
  `request_method` varchar(10) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT '' COMMENT '请求方式',
  `operator_type` int NULL DEFAULT 0 COMMENT '操作类别（0提交题目 1录取 2判题）',
  `oper_name` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT '' COMMENT '操作人员的昵称',
  `dept_type` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT '' COMMENT '部门类型 (0新人 0技术部 1统筹部)',
  `oper_url` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT '' COMMENT '请求URL',
  `oper_ip` varchar(128) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT '' COMMENT '主机地址',
  `oper_param` varchar(2000) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT '' COMMENT '请求参数',
  `json_result` varchar(2000) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT '' COMMENT '返回参数',
  `status` int NULL DEFAULT 0 COMMENT '操作状态（0正常 1异常）',
  `error_msg` varchar(2000) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT '' COMMENT '错误消息',
  `oper_time` datetime NULL DEFAULT NULL COMMENT '操作时间',
  `cost_time` bigint NULL DEFAULT 0 COMMENT '消耗时间',
  PRIMARY KEY (`oper_id`) USING BTREE,
  INDEX `idx_sys_oper_log_bt`(`business_type` ASC) USING BTREE,
  INDEX `idx_sys_oper_log_s`(`status` ASC) USING BTREE,
  INDEX `idx_sys_oper_log_ot`(`oper_time` ASC) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 491 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci COMMENT = '操作日志记录' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for sys_reply
-- ----------------------------
DROP TABLE IF EXISTS `sys_reply`;
CREATE TABLE `sys_reply`  (
  `reply_id` varchar(70) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '提交ID(主键)',
  `topic_id` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '题目ID (用于查询题目标题和题目详情)',
  `user_id` bigint NULL DEFAULT NULL COMMENT '提交用户的ID',
  `judger_id` bigint NULL DEFAULT NULL COMMENT '判题人的ID (user_id)',
  `score` int NULL DEFAULT 0 COMMENT '分数(默认-1: 未进行提交)',
  `judger` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '判断人的姓名',
  `status` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT '0' COMMENT '评分状态(0:未进行评分 1:评分完毕) 默认为0',
  `answer` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL COMMENT '用户提交的答案',
  `create_by` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '创建者',
  `create_time` datetime NULL DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '修改人',
  `update_time` datetime NULL DEFAULT NULL COMMENT '修改时间',
  `remark` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '备注',
  PRIMARY KEY (`reply_id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci ROW_FORMAT = Dynamic;


-- ----------------------------
-- Table structure for sys_role
-- ----------------------------
DROP TABLE IF EXISTS `sys_role`;
CREATE TABLE `sys_role`  (
  `role_id` bigint NOT NULL AUTO_INCREMENT COMMENT '角色ID',
  `role_name` varchar(30) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '角色名称',
  `role_key` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '角色权限字符串',
  `status` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '角色状态（0正常 1停用）',
  `del_flag` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT '0' COMMENT '删除标志（0代表存在 2代表删除）',
  `create_by` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT '' COMMENT '创建者',
  `create_time` datetime NULL DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT '' COMMENT '更新者',
  `update_time` datetime NULL DEFAULT NULL COMMENT '更新时间',
  `remark` varchar(500) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '备注',
  PRIMARY KEY (`role_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 100 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci COMMENT = '角色信息表' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of sys_role
-- ----------------------------
INSERT INTO `sys_role` VALUES (1, '超级管理员', 'admin', '0', '0', 'admin', '2023-04-07 19:40:18', '', NULL, '超级管理员,拥有所有权限');
INSERT INTO `sys_role` VALUES (2, '普通角色', 'common', '0', '0', 'admin', '2023-04-07 19:40:18', '', NULL, '普通角色');
INSERT INTO `sys_role` VALUES (3, '新人', 'newcomer', '0', '0', 'admin', '2023-10-07 19:37:08', '', NULL, '新人, 仅有答题和用户个人权限');

-- ----------------------------
-- Table structure for sys_role_perm
-- ----------------------------
DROP TABLE IF EXISTS `sys_role_perm`;
CREATE TABLE `sys_role_perm`  (
  `role_id` bigint NOT NULL COMMENT '角色ID',
  `permission` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '权限',
  `permission_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '权限名称',
  PRIMARY KEY (`role_id`, `permission`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci COMMENT = '角色和菜单关联表' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of sys_role_perm
-- ----------------------------
INSERT INTO `sys_role_perm` VALUES (1, 'dashboard', '仪表盘');
INSERT INTO `sys_role_perm` VALUES (1, 'judge', '判题');
INSERT INTO `sys_role_perm` VALUES (1, 'log', '日志操作');
INSERT INTO `sys_role_perm` VALUES (1, 'mail', '邮件发送');
INSERT INTO `sys_role_perm` VALUES (1, 'monitor', '监控');
INSERT INTO `sys_role_perm` VALUES (1, 'newcomer', '新人操作');
INSERT INTO `sys_role_perm` VALUES (1, 'notice', '发布通知');
INSERT INTO `sys_role_perm` VALUES (1, 'reply', '答题');
INSERT INTO `sys_role_perm` VALUES (1, 'setting', '系统设置');
INSERT INTO `sys_role_perm` VALUES (1, 'setTopic', '设置题目');
INSERT INTO `sys_role_perm` VALUES (1, 'superadmin', '超级管理员');
INSERT INTO `sys_role_perm` VALUES (1, 'system', '系统');
INSERT INTO `sys_role_perm` VALUES (1, 'user', '用户个人');
INSERT INTO `sys_role_perm` VALUES (2, 'judge', '判题');
INSERT INTO `sys_role_perm` VALUES (2, 'newcomer', '新人操作');
INSERT INTO `sys_role_perm` VALUES (2, 'reply', '答题');
INSERT INTO `sys_role_perm` VALUES (2, 'setTopic', '设置题目');
INSERT INTO `sys_role_perm` VALUES (2, 'user', '用户个人');
INSERT INTO `sys_role_perm` VALUES (3, 'enroll', '录取结果页');
INSERT INTO `sys_role_perm` VALUES (3, 'reply', '答题');
INSERT INTO `sys_role_perm` VALUES (3, 'user', '用户个人');

-- ----------------------------
-- Table structure for sys_topic
-- ----------------------------
DROP TABLE IF EXISTS `sys_topic`;
CREATE TABLE `sys_topic`  (
  `topic_id` varchar(30) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '题目ID',
  `author_id` int NOT NULL COMMENT '出题人id(user_id)',
  `author_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '出题人姓名',
  `status` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT '0' COMMENT '题目状态(0待审核 1审核未通过 2审核通过) (默认0待审核)',
  `publish_status` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT '0' COMMENT '发布状态(0未发布 1已发布) (审核未通过禁止发布)',
  `title` varchar(30) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '题目标题',
  `level` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '题目难度(0简单 1中等 2困难) ',
  `tags` varchar(500) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '题目标签(可以多个)',
  `detail` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL COMMENT '题目详细信息',
  `code_template` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL COMMENT '代码模板',
  `del_flag` varchar(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT '0' COMMENT '删除标记(0未删除 1已删除)',
  `create_by` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '创建人姓名',
  `create_time` datetime NULL DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '修改人姓名1',
  `update_time` datetime NULL DEFAULT NULL COMMENT '修改时间',
  PRIMARY KEY (`topic_id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for sys_user
-- ----------------------------
DROP TABLE IF EXISTS `sys_user`;
CREATE TABLE `sys_user`  (
  `user_id` bigint NOT NULL AUTO_INCREMENT COMMENT '用户ID',
  `student_id` varchar(15) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '学号',
  `user_name` varchar(30) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '用户账号',
  `nick_name` varchar(30) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL DEFAULT '' COMMENT '用户昵称',
  `user_type` varchar(2) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT '01' COMMENT '用户类型（00协会用户, 01新人用户）默认为01',
  `dept_type` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT '0' COMMENT '部门(0新人 1技术部 2统筹部) 默认为0',
  `email` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT '' COMMENT '用户邮箱',
  `clasz` varchar(15) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '所在班级',
  `phonenumber` varchar(11) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT '' COMMENT '手机号码',
  `sex` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT '0' COMMENT '用户性别（0男 1女 2未知）',
  `avatar` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '头像地址',
  `password` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL DEFAULT '' COMMENT '密码',
  `enroll_status` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT '0' COMMENT '录取状态(0待录取 1未录取  2已录取 3协会内成员) 默认为0',
  `total_score` int NULL DEFAULT NULL COMMENT '总分 (0-100)',
  `status` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT '0' COMMENT '帐号状态（0正常 1停用）默认为0',
  `del_flag` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT '0' COMMENT '删除标志（0代表存在 1代表删除）默认为0',
  `login_ip` varchar(128) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT '' COMMENT '最后登录IP',
  `login_date` datetime NULL DEFAULT NULL COMMENT '最后登录时间',
  `create_by` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT '' COMMENT '创建者',
  `create_time` datetime NULL DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT '' COMMENT '更新者',
  `update_time` datetime NULL DEFAULT NULL COMMENT '更新时间',
  `remark` varchar(500) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '备注',
  PRIMARY KEY (`user_id`, `nick_name`) USING BTREE,
  UNIQUE INDEX `nick_name`(`nick_name` ASC) USING BTREE COMMENT 'nickName唯一标识'
) ENGINE = InnoDB AUTO_INCREMENT = 111 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci COMMENT = '用户信息表' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of sys_user
-- ----------------------------
INSERT INTO `sys_user` VALUES (1, '202210007548', 'admin', '天天', '00', '1', '1979214063@qq.com', '22软件工程5班', '13588888888', '1', 'https://foruda.gitee.com/avatar/1677297613697026105/11407683_tiantian17_1677297613.png!avatar200', '$2a$10$A/rKjWqAdAiYyfeJYtUeoeu6xbNO8yn.IcLZ6xot7ctSv2FMoAfr6', '0', 10, '0', '0', '127.0.0.1', '2023-09-30 17:31:16', '', '2023-04-07 19:40:18', '', '2023-10-30 21:06:47', '管理员');
INSERT INTO `sys_user` VALUES (2, '202310006666', 'tiantian', '新人用户', '01', '0', '1979214062@qq.com', '23软件工程5班', '13588888888', '0', 'http://10.226.8.14:9000/jx-welcome-minio/default_avatar.png', '$2a$10$.O/5J1m.vtBJO0LV5StuY.8rZKIoJHezfnrPphtj5BgnImUe.svsK', '2', 38, '0', '0', '', NULL, '', NULL, '', '2023-11-12 10:19:00', NULL);
INSERT INTO `sys_user` VALUES (100, '202310006868', 'tiantiantest', '注册测试', '00', '0', '666@qq.com', '23软件工程6班', '13413537556', '0', 'http://10.226.8.14:9000/jx-welcome-minio/default_avatar.png', '$2a$10$eylNS.nQyhVDTvt9l4iOZeu2buP.l/9keaY3ZoXAAUw1d1bR.amhq', '0', NULL, '0', '0', '', NULL, '注册测试', '2023-11-03 19:55:31', '注册测试', '2023-11-03 20:59:10', '我的个人简介');
INSERT INTO `sys_user` VALUES (101, '202310006868', 'tiantiantest1', '注册测试2', '00', '0', '1979214061@qq.com', '23软件工程6班', '13413537555', '0', 'http://10.226.8.14:9000/jx-welcome-minio/default_avatar.png', '$2a$10$.gtCo6q1nAdZU1Favpf7KOlWC097NYFnLD6d98oX4yP9MyodgkuJu', '0', NULL, '0', '0', '', NULL, '注册测试', '2023-11-03 19:57:15', '注册测试', NULL, NULL);
INSERT INTO `sys_user` VALUES (104, '202210006666', 'tiantian123', '真实姓名', '00', '0', '13888888888@qq.com', '22软件工程5班', '13888888888', '0', 'http://10.226.8.14:9000/jx-welcome-minio/default_avatar.png', '$2a$10$A/rKjWqAdAiYyfeJYtUeoeu6xbNO8yn.IcLZ6xot7ctSv2FMoAfr6', '0', NULL, '0', '0', '', NULL, '真实姓名', '2023-11-08 15:52:40', '真实姓名', NULL, NULL);
INSERT INTO `sys_user` VALUES (105, '199800007777', 'haohao', '工作人员测试账号', '00', '0', '1979214068@qq.com', '98软件工程5班', '13413537555', '0', 'http://10.226.8.14:9000/jx-welcome-minio/default_avatar.png', '$2a$10$OG5cI4UGiVdEcH1SMxWr2eXtKBqdBcI2YZ1mC.vvTpBn6d61v7R9K', '0', NULL, '0', '0', '', NULL, '工作人员测试账号', '2023-11-09 20:41:09', '工作人员测试账号', NULL, NULL);
INSERT INTO `sys_user` VALUES (109, '202210001111', '202210001111', '陈科宇', '01', '0', '1979214069@qq.com', '22软件工程5班', '13413537553', '0', 'http://10.226.8.14:9000/jx-welcome-minio/default_avatar.png', '$2a$10$LVPqbEXs8WRpPJWhgoYKJOGWhjqGxb.96T3JlZFAuPxg.UcSASC5a', '2', 60, '0', '0', '', NULL, '陈科宇', '2023-11-11 17:24:45', '陈科宇', NULL, NULL);
INSERT INTO `sys_user` VALUES (110, '202310001111', '202310001111', '陈宇科', '01', '0', '197921sdfsd0@qq.com', '23软件工程6班', '13414561653', '0', 'http://10.226.8.14:9000/jx-welcome-minio/default_avatar.png', '$2a$10$UyZUQo/gDh4idlSSq80Sn.hL.u/Y.osd7neUunSmVKKnJk4ANDqQq', '1', 0, '0', '0', '', NULL, '陈宇科', '2023-11-12 19:57:58', '天天', NULL, NULL);

-- ----------------------------
-- Table structure for sys_user_action
-- ----------------------------
DROP TABLE IF EXISTS `sys_user_action`;
CREATE TABLE `sys_user_action`  (
  `action_id` int NOT NULL COMMENT '主键ID',
  `action` varchar(10) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '操作名称',
  `remark` varchar(10) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '描述',
  PRIMARY KEY (`action_id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sys_user_action
-- ----------------------------
INSERT INTO `sys_user_action` VALUES (1, 'add', '新增');
INSERT INTO `sys_user_action` VALUES (2, 'query', '查询');
INSERT INTO `sys_user_action` VALUES (3, 'get', '详情');
INSERT INTO `sys_user_action` VALUES (4, 'update', '修改');
INSERT INTO `sys_user_action` VALUES (5, 'delete', '删除');

-- ----------------------------
-- Table structure for sys_user_role
-- ----------------------------
DROP TABLE IF EXISTS `sys_user_role`;
CREATE TABLE `sys_user_role`  (
  `user_id` bigint NOT NULL COMMENT '用户ID',
  `role_id` bigint NOT NULL COMMENT '角色ID',
  PRIMARY KEY (`user_id`, `role_id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci COMMENT = '用户和角色关联表' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of sys_user_role
-- ----------------------------
INSERT INTO `sys_user_role` VALUES (1, 1);
INSERT INTO `sys_user_role` VALUES (2, 3);
INSERT INTO `sys_user_role` VALUES (3, 3);
INSERT INTO `sys_user_role` VALUES (100, 3);
INSERT INTO `sys_user_role` VALUES (101, 1);
INSERT INTO `sys_user_role` VALUES (104, 3);
INSERT INTO `sys_user_role` VALUES (105, 1);
INSERT INTO `sys_user_role` VALUES (109, 3);
INSERT INTO `sys_user_role` VALUES (110, 3);

-- ----------------------------
-- Table structure for xxl_job_group
-- ----------------------------
DROP TABLE IF EXISTS `xxl_job_group`;
CREATE TABLE `xxl_job_group`  (
  `id` int NOT NULL AUTO_INCREMENT,
  `app_name` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '执行器AppName',
  `title` varchar(12) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '执行器名称',
  `address_type` tinyint NOT NULL DEFAULT 0 COMMENT '执行器地址类型：0=自动注册、1=手动录入',
  `address_list` text CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL COMMENT '执行器地址列表，多地址逗号分隔',
  `update_time` datetime NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 2 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of xxl_job_group
-- ----------------------------
INSERT INTO `xxl_job_group` VALUES (1, 'xxl-job-executor-sample', '示例执行器', 0, NULL, '2023-10-24 23:18:19');
INSERT INTO `xxl_job_group` VALUES (2, 'jx-welcome-file-clear', '删除图床过期文件', 1, 'http://127.0.0.1:9999/', '2023-10-23 19:57:03');

-- ----------------------------
-- Table structure for xxl_job_info
-- ----------------------------
DROP TABLE IF EXISTS `xxl_job_info`;
CREATE TABLE `xxl_job_info`  (
  `id` int NOT NULL AUTO_INCREMENT,
  `job_group` int NOT NULL COMMENT '执行器主键ID',
  `job_desc` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  `add_time` datetime NULL DEFAULT NULL,
  `update_time` datetime NULL DEFAULT NULL,
  `author` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '作者',
  `alarm_email` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '报警邮件',
  `schedule_type` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL DEFAULT 'NONE' COMMENT '调度类型',
  `schedule_conf` varchar(128) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '调度配置，值含义取决于调度类型',
  `misfire_strategy` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL DEFAULT 'DO_NOTHING' COMMENT '调度过期策略',
  `executor_route_strategy` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '执行器路由策略',
  `executor_handler` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '执行器任务handler',
  `executor_param` varchar(512) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '执行器任务参数',
  `executor_block_strategy` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '阻塞处理策略',
  `executor_timeout` int NOT NULL DEFAULT 0 COMMENT '任务执行超时时间，单位秒',
  `executor_fail_retry_count` int NOT NULL DEFAULT 0 COMMENT '失败重试次数',
  `glue_type` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT 'GLUE类型',
  `glue_source` mediumtext CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL COMMENT 'GLUE源代码',
  `glue_remark` varchar(128) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT 'GLUE备注',
  `glue_updatetime` datetime NULL DEFAULT NULL COMMENT 'GLUE更新时间',
  `child_jobid` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '子任务ID，多个逗号分隔',
  `trigger_status` tinyint NOT NULL DEFAULT 0 COMMENT '调度状态：0-停止，1-运行',
  `trigger_last_time` bigint NOT NULL DEFAULT 0 COMMENT '上次调度时间',
  `trigger_next_time` bigint NOT NULL DEFAULT 0 COMMENT '下次调度时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 5 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of xxl_job_info
-- ----------------------------
INSERT INTO `xxl_job_info` VALUES (1, 1, '测试任务1', '2018-11-03 22:21:31', '2023-10-23 20:02:38', 'XXL', '', 'CRON', '0 0 0 * * ? *', 'DO_NOTHING', 'FIRST', 'demoJobHandler', '', 'SERIAL_EXECUTION', 0, 0, 'BEAN', '', 'GLUE代码初始化', '2018-11-03 22:21:31', '', 0, 0, 0);
INSERT INTO `xxl_job_info` VALUES (2, 1, 'HelloWorld程序', '2023-10-10 16:26:01', '2023-10-10 16:27:25', '天天', '', 'CRON', '0/20 * * * * ?', 'DO_NOTHING', 'FIRST', 'demoJobHandler', '', 'SERIAL_EXECUTION', 0, 0, 'BEAN', '', 'GLUE代码初始化', '2023-10-10 16:26:01', '', 0, 0, 0);
INSERT INTO `xxl_job_info` VALUES (3, 1, 'GLUE模式', '2023-10-10 16:31:24', '2023-10-10 16:36:21', '天天', '', 'CRON', '0/3 * * * * ?', 'DO_NOTHING', 'FIRST', '', '', 'SERIAL_EXECUTION', 0, 0, 'GLUE_GROOVY', 'package com.xxl.job.service.handler;\n\nimport com.xxl.job.executor.service.jobhandler.HelloService;\nimport org.springframework.beans.factory.annotation.Autowired;\nimport com.xxl.job.core.context.XxlJobHelper;\nimport com.xxl.job.core.handler.IJobHandler;\n\npublic class DemoGlueJobHandler extends IJobHandler {\n\n  	@Autowired\n  	private HelloService helloService;\n  \n	@Override\n	public void execute() throws Exception {\n		helloService.methodA();\n	}\n\n}\n', '执行HelloService(GLUE模式)', '2023-10-10 16:34:05', '', 0, 0, 0);
INSERT INTO `xxl_job_info` VALUES (4, 2, '删除图片过期文件', '2023-10-23 19:51:44', '2023-10-24 20:36:14', '天天', '', 'CRON', '0/2 * * * * ?', 'DO_NOTHING', 'FIRST', 'clearExpiredFile', '', 'SERIAL_EXECUTION', 0, 0, 'BEAN', '', 'GLUE代码初始化', '2023-10-23 19:51:44', '', 0, 0, 0);
INSERT INTO `xxl_job_info` VALUES (5, 2, '判题人分配', '2023-10-24 20:42:27', '2023-10-24 20:42:27', '天天', '', 'CRON', '0/2 * * * * ?', 'DO_NOTHING', 'FIRST', 'allotNewcomer', '', 'SERIAL_EXECUTION', 0, 0, 'BEAN', '', 'GLUE代码初始化', '2023-10-24 20:42:27', '', 0, 0, 0);

-- ----------------------------
-- Table structure for xxl_job_lock
-- ----------------------------
DROP TABLE IF EXISTS `xxl_job_lock`;
CREATE TABLE `xxl_job_lock`  (
  `lock_name` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '锁名称',
  PRIMARY KEY (`lock_name`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of xxl_job_lock
-- ----------------------------
INSERT INTO `xxl_job_lock` VALUES ('schedule_lock');

-- ----------------------------
-- Table structure for xxl_job_log
-- ----------------------------
DROP TABLE IF EXISTS `xxl_job_log`;
CREATE TABLE `xxl_job_log`  (
  `id` bigint NOT NULL AUTO_INCREMENT,
  `job_group` int NOT NULL COMMENT '执行器主键ID',
  `job_id` int NOT NULL COMMENT '任务，主键ID',
  `executor_address` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '执行器地址，本次执行的地址',
  `executor_handler` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '执行器任务handler',
  `executor_param` varchar(512) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '执行器任务参数',
  `executor_sharding_param` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '执行器任务分片参数，格式如 1/2',
  `executor_fail_retry_count` int NOT NULL DEFAULT 0 COMMENT '失败重试次数',
  `trigger_time` datetime NULL DEFAULT NULL COMMENT '调度-时间',
  `trigger_code` int NOT NULL COMMENT '调度-结果',
  `trigger_msg` text CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL COMMENT '调度-日志',
  `handle_time` datetime NULL DEFAULT NULL COMMENT '执行-时间',
  `handle_code` int NOT NULL COMMENT '执行-状态',
  `handle_msg` text CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL COMMENT '执行-日志',
  `alarm_status` tinyint NOT NULL DEFAULT 0 COMMENT '告警状态：0-默认、1-无需告警、2-告警成功、3-告警失败',
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `I_trigger_time`(`trigger_time` ASC) USING BTREE,
  INDEX `I_handle_code`(`handle_code` ASC) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 79 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of xxl_job_log
-- ----------------------------
INSERT INTO `xxl_job_log` VALUES (78, 2, 5, 'http://127.0.0.1:9999/', 'allotNewcomer', '', NULL, 0, '2023-10-24 21:08:09', 200, '任务触发类型：手动触发<br>调度机器：192.168.88.1<br>执行器-注册方式：手动录入<br>执行器-地址列表：[http://127.0.0.1:9999/]<br>路由策略：第一个<br>阻塞处理策略：单机串行<br>任务超时时间：0<br>失败重试次数：0<br><br><span style=\"color:#00c0ef;\" > >>>>>>>>>>>触发调度<<<<<<<<<<< </span><br>触发调度：<br>address：http://127.0.0.1:9999/<br>code：200<br>msg：null', '2023-10-24 21:08:09', 500, 'java.lang.reflect.InvocationTargetException\r\n	at sun.reflect.NativeMethodAccessorImpl.invoke0(Native Method)\r\n	at sun.reflect.NativeMethodAccessorImpl.invoke(NativeMethodAccessorImpl.java:62)\r\n	at sun.reflect.DelegatingMethodAccessorImpl.invoke(DelegatingMethodAccessorImpl.java:43)\r\n	at java.lang.reflect.Method.invoke(Method.java:498)\r\n	at com.xxl.job.core.handler.impl.MethodJobHandler.execute(MethodJobHandler.java:31)\r\n	at com.xxl.job.core.thread.JobThread.run(JobThread.java:166)\r\nCaused by: org.mybatis.spring.MyBatisSystemException: nested exception is org.apache.ibatis.builder.BuilderException: Error evaluating expression \'item.judger != null and item.judger != \'\'\'. Cause: org.apache.ibatis.ognl.NoSuchPropertyException: com.tiantian.common.core.domain.entity.SysJudger.judger\r\n	at org.mybatis.spring.MyBatisExceptionTranslator.translateExceptionIfPossible(MyBatisExceptionTranslator.java:96)\r\n	at org.mybatis.spring.SqlSessionTemplate$SqlSessionInterceptor.invoke(SqlSessionTemplate.java:441)\r\n	at com.sun.proxy.$Proxy78.update(Unknown Source)\r\n	at org.mybatis.spring.SqlSessionTemplate.update(SqlSessionTemplate.java:288)\r\n	at org.apache.ibatis.binding.MapperMethod.execute(MapperMethod.java:67)\r\n	at org.apache.ibatis.binding.MapperProxy$PlainMethodInvoker.invoke(MapperProxy.java:152)\r\n	at org.apache.ibatis.binding.MapperProxy.invoke(MapperProxy.java:85)\r\n	at com.sun.proxy.$Proxy90.updateJudgerBatch(Unknown Source)\r\n	at com.tiantian.topic.service.impl.SysJudgeServiceImpl.allotNewcomer(SysJudgeServiceImpl.java:88)\r\n	at com.tiantian.topic.service.impl.SysJudgeServiceImpl$$FastClassBySpringCGLIB$$996211d2.invoke(<generated>)\r\n	at org.springframework.cglib.proxy.MethodProxy.invoke(MethodProxy.java:218)\r\n	at org.springframework.aop.framework.CglibAopProxy.invokeMethod(CglibAopProxy.java:386)\r\n	at org.springframework.aop.framework.CglibAopProxy.access$000(CglibAopProxy.java:85)\r\n	at org.springframework.aop.framework.CglibAopProxy$DynamicAdvisedInterceptor.intercept(CglibAopProxy.java:704)\r\n	at com.tiantian.topic.service.impl.SysJudgeServiceImpl$$EnhancerBySpringCGLIB$$82ba626c.allotNewcomer(<generated>)\r\n	at com.tiantian.job.SysJobService.allotNewcomer(SysJobService.java:85)\r\n	... 6 more\r\nCaused by: org.apache.ibatis.builder.BuilderException: Error evaluating expression \'item.judger != null and item.judger != \'\'\'. Cause: org.apache.ibatis.ognl.NoSuchPropertyException: com.tiantian.common.core.domain.entity.SysJudger.judger\r\n	at org.apache.ibatis.scripting.xmltags.OgnlCache.getValue(OgnlCache.java:48)\r\n	at org.apache.ibatis.scripting.xmltags.ExpressionEvaluator.evaluateBoolean(ExpressionEvaluator.java:32)\r\n	at org.apache.ibatis.scripting.xmltags.IfSqlNode.apply(IfSqlNode.java:34)\r\n	at org.apache.ibatis.scripting.xmltags.MixedSqlNode.lambda$apply$0(MixedSqlNode.java:32)\r\n	at java.util.ArrayList.forEach(ArrayList.java:1259)\r\n	at org.apache.ibatis.scripting.xmltags.MixedSqlNode.apply(MixedSqlNode.java:32)\r\n	at org.apache.ibatis.scripting.xmltags.TrimSqlNode.apply(TrimSqlNode.java:55)\r\n	at org.apache.ibatis.scripting.xmltags.MixedSqlNode.lambda$apply$0(MixedSqlNode.java:32)\r\n	at java.util.ArrayList.forEach(ArrayList.java:1259)\r\n	at org.apache.ibatis.scripting.xmltags.MixedSqlNode.apply(MixedSqlNode.java:32)\r\n	at org.apache.ibatis.scripting.xmltags.ForEachSqlNode.apply(ForEachSqlNode.java:79)\r\n	at org.apache.ibatis.scripting.xmltags.MixedSqlNode.lambda$apply$0(MixedSqlNode.java:32)\r\n	at java.util.ArrayList.forEach(ArrayList.java:1259)\r\n	at org.apache.ibatis.scripting.xmltags.MixedSqlNode.apply(MixedSqlNode.java:32)\r\n	at org.apache.ibatis.scripting.xmltags.DynamicSqlSource.getBoundSql(DynamicSqlSource.java:39)\r\n	at org.apache.ibatis.mapping.MappedStatement.getBoundSql(MappedStatement.java:305)\r\n	at org.apache.ibatis.executor.statement.BaseStatementHandler.<init>(BaseStatementHandler.java:64)\r\n	at org.apache.ibatis.executor.statement.PreparedStatementHandler.<init>(PreparedStatementHandler.java:41)\r\n	at org.apache.ibatis.executor.statement.RoutingStatementHandler.<init>(RoutingStatementHandler.java:46)\r\n	at org.apache.ibatis.session.Configuration.newStatementHandler(Configuration.java:658)\r\n	at org.apache.ibatis.executor.SimpleExecutor.doUpdate(SimpleExecutor.java:48)\r\n	at org.apache.ibatis.executor.BaseExecutor.update(BaseExecutor.java:117)\r\n	at org.apache.ibatis.executor.CachingExecutor.update(CachingExecutor.java:76)\r\n	at sun.reflect.NativeMethodAccessorImpl.invoke0(Native Method)\r\n	at sun.reflect.NativeMethodAccessorImpl.invoke(NativeMethodAccessorImpl.java:62)\r\n	at sun.reflect.DelegatingMethodAccessorImpl.invoke(DelegatingMethodAccessorImpl.java:43)\r\n	at java.lang.reflect.Method.invoke(Method.java:498)\r\n	at org.apache.ibatis.plugin.Plugin.invoke(Plugin.java:63)\r\n	at com.sun.proxy.$Proxy135.update(Unknown Source)\r\n	at org.apache.ibatis.session.defaults.DefaultSqlSession.update(DefaultSqlSession.java:197)\r\n	at sun.reflect.NativeMethodAccessorImpl.invoke0(Native Method)\r\n	at sun.reflect.NativeMethodAccessorImpl.invoke(NativeMethodAccessorImpl.java:62)\r\n	at sun.reflect.DelegatingMethodAccessorImpl.invoke(DelegatingMethodAccessorImpl.java:43)\r\n	at java.lang.reflect.Method.invoke(Method.java:498)\r\n	at org.mybatis.spring.SqlSessionTemplate$SqlSessionInterceptor.invoke(SqlSessionTemplate.java:427)\r\n	... 20 more\r\nCaused by: org.apache.ibatis.ognl.NoSuchPropertyException: com.tiantian.common.core.domain.entity.SysJudger.judger\r\n	at org.apache.ibatis.ognl.ObjectPropertyAccessor.getProperty(ObjectPropertyAccessor.java:164)\r\n	at org.apache.ibatis.ognl.OgnlRuntime.getProperty(OgnlRuntime.java:3338)\r\n	at org.apache.ibatis.ognl.ASTProperty.getValueBody(ASTProperty.java:121)\r\n	at org.apache.ibatis.ognl.SimpleNode.evaluateGetValueBody(SimpleNode.java:212)\r\n	at org.apache.ibatis.ognl.SimpleNode.getValue(SimpleNode.java:258)\r\n	at org.apache.ibatis.ognl.ASTChain.getValueBody(ASTChain.java:141)\r\n	at org.apache.ibatis.ognl.SimpleNode.evaluateGetValueBody(SimpleNode.java:212)\r\n	at org.apache.ibatis.ognl.SimpleNode.getValue(SimpleNode.java:258)\r\n	at org.apache.ibatis.ognl.ASTNotEq.getValueBody(ASTNotEq.java:50)\r\n	at org.apache.ibatis.ognl.SimpleNode.evaluateGetValueBody(SimpleNode.java:212)\r\n	at org.apache.ibatis.ognl.SimpleNode.getValue(SimpleNode.java:258)\r\n	at org.apache.ibatis.ognl.ASTAnd.getValueBody(ASTAnd.java:61)\r\n	at org.apache.ibatis.ognl.SimpleNode.evaluateGetValueBody(SimpleNode.java:212)\r\n	at org.apache.ibatis.ognl.SimpleNode.getValue(SimpleNode.java:258)\r\n	at org.apache.ibatis.ognl.Ognl.getValue(Ognl.java:560)\r\n	at org.apache.ibatis.ognl.Ognl.getValue(Ognl.java:524)\r\n	at org.apache.ibatis.scripting.xmltags.OgnlCache.getValue(OgnlCache.java:46)\r\n	... 54 more\r\n', 2);
INSERT INTO `xxl_job_log` VALUES (79, 2, 5, 'http://127.0.0.1:9999/', 'allotNewcomer', '', NULL, 0, '2023-10-24 21:08:51', 200, '任务触发类型：手动触发<br>调度机器：192.168.88.1<br>执行器-注册方式：手动录入<br>执行器-地址列表：[http://127.0.0.1:9999/]<br>路由策略：第一个<br>阻塞处理策略：单机串行<br>任务超时时间：0<br>失败重试次数：0<br><br><span style=\"color:#00c0ef;\" > >>>>>>>>>>>触发调度<<<<<<<<<<< </span><br>触发调度：<br>address：http://127.0.0.1:9999/<br>code：200<br>msg：null', '2023-10-24 21:11:44', 500, 'java.lang.reflect.InvocationTargetException\r\n	at sun.reflect.NativeMethodAccessorImpl.invoke0(Native Method)\r\n	at sun.reflect.NativeMethodAccessorImpl.invoke(NativeMethodAccessorImpl.java:62)\r\n	at sun.reflect.DelegatingMethodAccessorImpl.invoke(DelegatingMethodAccessorImpl.java:43)\r\n	at java.lang.reflect.Method.invoke(Method.java:498)\r\n	at com.xxl.job.core.handler.impl.MethodJobHandler.execute(MethodJobHandler.java:31)\r\n	at com.xxl.job.core.thread.JobThread.run(JobThread.java:166)\r\nCaused by: org.mybatis.spring.MyBatisSystemException: nested exception is org.apache.ibatis.builder.BuilderException: Error evaluating expression \'item.judger != null and item.judger != \'\'\'. Cause: org.apache.ibatis.ognl.NoSuchPropertyException: com.tiantian.common.core.domain.entity.SysJudger.judger\r\n	at org.mybatis.spring.MyBatisExceptionTranslator.translateExceptionIfPossible(MyBatisExceptionTranslator.java:96)\r\n	at org.mybatis.spring.SqlSessionTemplate$SqlSessionInterceptor.invoke(SqlSessionTemplate.java:441)\r\n	at com.sun.proxy.$Proxy78.update(Unknown Source)\r\n	at org.mybatis.spring.SqlSessionTemplate.update(SqlSessionTemplate.java:288)\r\n	at org.apache.ibatis.binding.MapperMethod.execute(MapperMethod.java:67)\r\n	at org.apache.ibatis.binding.MapperProxy$PlainMethodInvoker.invoke(MapperProxy.java:152)\r\n	at org.apache.ibatis.binding.MapperProxy.invoke(MapperProxy.java:85)\r\n	at com.sun.proxy.$Proxy90.updateJudgerBatch(Unknown Source)\r\n	at com.tiantian.topic.service.impl.SysJudgeServiceImpl.allotNewcomer(SysJudgeServiceImpl.java:88)\r\n	at com.tiantian.topic.service.impl.SysJudgeServiceImpl$$FastClassBySpringCGLIB$$996211d2.invoke(<generated>)\r\n	at org.springframework.cglib.proxy.MethodProxy.invoke(MethodProxy.java:218)\r\n	at org.springframework.aop.framework.CglibAopProxy.invokeMethod(CglibAopProxy.java:386)\r\n	at org.springframework.aop.framework.CglibAopProxy.access$000(CglibAopProxy.java:85)\r\n	at org.springframework.aop.framework.CglibAopProxy$DynamicAdvisedInterceptor.intercept(CglibAopProxy.java:704)\r\n	at com.tiantian.topic.service.impl.SysJudgeServiceImpl$$EnhancerBySpringCGLIB$$82ba626c.allotNewcomer(<generated>)\r\n	at com.tiantian.job.SysJobService.allotNewcomer(SysJobService.java:85)\r\n	... 6 more\r\nCaused by: org.apache.ibatis.builder.BuilderException: Error evaluating expression \'item.judger != null and item.judger != \'\'\'. Cause: org.apache.ibatis.ognl.NoSuchPropertyException: com.tiantian.common.core.domain.entity.SysJudger.judger\r\n	at org.apache.ibatis.scripting.xmltags.OgnlCache.getValue(OgnlCache.java:48)\r\n	at org.apache.ibatis.scripting.xmltags.ExpressionEvaluator.evaluateBoolean(ExpressionEvaluator.java:32)\r\n	at org.apache.ibatis.scripting.xmltags.IfSqlNode.apply(IfSqlNode.java:34)\r\n	at org.apache.ibatis.scripting.xmltags.MixedSqlNode.lambda$apply$0(MixedSqlNode.java:32)\r\n	at java.util.ArrayList.forEach(ArrayList.java:1259)\r\n	at org.apache.ibatis.scripting.xmltags.MixedSqlNode.apply(MixedSqlNode.java:32)\r\n	at org.apache.ibatis.scripting.xmltags.TrimSqlNode.apply(TrimSqlNode.java:55)\r\n	at org.apache.ibatis.scripting.xmltags.MixedSqlNode.lambda$apply$0(MixedSqlNode.java:32)\r\n	at java.util.ArrayList.forEach(ArrayList.java:1259)\r\n	at org.apache.ibatis.scripting.xmltags.MixedSqlNode.apply(MixedSqlNode.java:32)\r\n	at org.apache.ibatis.scripting.xmltags.ForEachSqlNode.apply(ForEachSqlNode.java:79)\r\n	at org.apache.ibatis.scripting.xmltags.MixedSqlNode.lambda$apply$0(MixedSqlNode.java:32)\r\n	at java.util.ArrayList.forEach(ArrayList.java:1259)\r\n	at org.apache.ibatis.scripting.xmltags.MixedSqlNode.apply(MixedSqlNode.java:32)\r\n	at org.apache.ibatis.scripting.xmltags.DynamicSqlSource.getBoundSql(DynamicSqlSource.java:39)\r\n	at org.apache.ibatis.mapping.MappedStatement.getBoundSql(MappedStatement.java:305)\r\n	at org.apache.ibatis.executor.statement.BaseStatementHandler.<init>(BaseStatementHandler.java:64)\r\n	at org.apache.ibatis.executor.statement.PreparedStatementHandler.<init>(PreparedStatementHandler.java:41)\r\n	at org.apache.ibatis.executor.statement.RoutingStatementHandler.<init>(RoutingStatementHandler.java:46)\r\n	at org.apache.ibatis.session.Configuration.newStatementHandler(Configuration.java:658)\r\n	at org.apache.ibatis.executor.SimpleExecutor.doUpdate(SimpleExecutor.java:48)\r\n	at org.apache.ibatis.executor.BaseExecutor.update(BaseExecutor.java:117)\r\n	at org.apache.ibatis.executor.CachingExecutor.update(CachingExecutor.java:76)\r\n	at sun.reflect.NativeMethodAccessorImpl.invoke0(Native Method)\r\n	at sun.reflect.NativeMethodAccessorImpl.invoke(NativeMethodAccessorImpl.java:62)\r\n	at sun.reflect.DelegatingMethodAccessorImpl.invoke(DelegatingMethodAccessorImpl.java:43)\r\n	at java.lang.reflect.Method.invoke(Method.java:498)\r\n	at org.apache.ibatis.plugin.Plugin.invoke(Plugin.java:63)\r\n	at com.sun.proxy.$Proxy135.update(Unknown Source)\r\n	at org.apache.ibatis.session.defaults.DefaultSqlSession.update(DefaultSqlSession.java:197)\r\n	at sun.reflect.NativeMethodAccessorImpl.invoke0(Native Method)\r\n	at sun.reflect.NativeMethodAccessorImpl.invoke(NativeMethodAccessorImpl.java:62)\r\n	at sun.reflect.DelegatingMethodAccessorImpl.invoke(DelegatingMethodAccessorImpl.java:43)\r\n	at java.lang.reflect.Method.invoke(Method.java:498)\r\n	at org.mybatis.spring.SqlSessionTemplate$SqlSessionInterceptor.invoke(SqlSessionTemplate.java:427)\r\n	... 20 more\r\nCaused by: org.apache.ibatis.ognl.NoSuchPropertyException: com.tiantian.common.core.domain.entity.SysJudger.judger\r\n	at org.apache.ibatis.ognl.ObjectPropertyAccessor.getProperty(ObjectPropertyAccessor.java:164)\r\n	at org.apache.ibatis.ognl.OgnlRuntime.getProperty(OgnlRuntime.java:3338)\r\n	at org.apache.ibatis.ognl.ASTProperty.getValueBody(ASTProperty.java:121)\r\n	at org.apache.ibatis.ognl.SimpleNode.evaluateGetValueBody(SimpleNode.java:212)\r\n	at org.apache.ibatis.ognl.SimpleNode.getValue(SimpleNode.java:258)\r\n	at org.apache.ibatis.ognl.ASTChain.getValueBody(ASTChain.java:141)\r\n	at org.apache.ibatis.ognl.SimpleNode.evaluateGetValueBody(SimpleNode.java:212)\r\n	at org.apache.ibatis.ognl.SimpleNode.getValue(SimpleNode.java:258)\r\n	at org.apache.ibatis.ognl.ASTNotEq.getValueBody(ASTNotEq.java:50)\r\n	at org.apache.ibatis.ognl.SimpleNode.evaluateGetValueBody(SimpleNode.java:212)\r\n	at org.apache.ibatis.ognl.SimpleNode.getValue(SimpleNode.java:258)\r\n	at org.apache.ibatis.ognl.ASTAnd.getValueBody(ASTAnd.java:61)\r\n	at org.apache.ibatis.ognl.SimpleNode.evaluateGetValueBody(SimpleNode.java:212)\r\n	at org.apache.ibatis.ognl.SimpleNode.getValue(SimpleNode.java:258)\r\n	at org.apache.ibatis.ognl.Ognl.getValue(Ognl.java:560)\r\n	at org.apache.ibatis.ognl.Ognl.getValue(Ognl.java:524)\r\n	at org.apache.ibatis.scripting.xmltags.OgnlCache.getValue(OgnlCache.java:46)\r\n	... 54 more\r\n', 2);

-- ----------------------------
-- Table structure for xxl_job_log_report
-- ----------------------------
DROP TABLE IF EXISTS `xxl_job_log_report`;
CREATE TABLE `xxl_job_log_report`  (
  `id` int NOT NULL AUTO_INCREMENT,
  `trigger_day` datetime NULL DEFAULT NULL COMMENT '调度-时间',
  `running_count` int NOT NULL DEFAULT 0 COMMENT '运行中-日志数量',
  `suc_count` int NOT NULL DEFAULT 0 COMMENT '执行成功-日志数量',
  `fail_count` int NOT NULL DEFAULT 0 COMMENT '执行失败-日志数量',
  `update_time` datetime NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `i_trigger_day`(`trigger_day` ASC) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 7 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of xxl_job_log_report
-- ----------------------------
INSERT INTO `xxl_job_log_report` VALUES (1, '2023-10-10 00:00:00', 0, 10, 0, NULL);
INSERT INTO `xxl_job_log_report` VALUES (2, '2023-10-09 00:00:00', 0, 0, 0, NULL);
INSERT INTO `xxl_job_log_report` VALUES (3, '2023-10-08 00:00:00', 0, 0, 0, NULL);
INSERT INTO `xxl_job_log_report` VALUES (4, '2023-10-23 00:00:00', 0, 0, 0, NULL);
INSERT INTO `xxl_job_log_report` VALUES (5, '2023-10-22 00:00:00', 0, 0, 0, NULL);
INSERT INTO `xxl_job_log_report` VALUES (6, '2023-10-21 00:00:00', 0, 0, 0, NULL);
INSERT INTO `xxl_job_log_report` VALUES (7, '2023-10-24 00:00:00', 0, 0, 2, NULL);

-- ----------------------------
-- Table structure for xxl_job_logglue
-- ----------------------------
DROP TABLE IF EXISTS `xxl_job_logglue`;
CREATE TABLE `xxl_job_logglue`  (
  `id` int NOT NULL AUTO_INCREMENT,
  `job_id` int NOT NULL COMMENT '任务，主键ID',
  `glue_type` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT 'GLUE类型',
  `glue_source` mediumtext CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL COMMENT 'GLUE源代码',
  `glue_remark` varchar(128) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT 'GLUE备注',
  `add_time` datetime NULL DEFAULT NULL,
  `update_time` datetime NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of xxl_job_logglue
-- ----------------------------
INSERT INTO `xxl_job_logglue` VALUES (1, 3, 'GLUE_GROOVY', 'package com.xxl.job.service.handler;\n\nimport com.xxl.job.executor.service.jobhandler.HelloService;\nimport org.springframework.beans.factory.annotation.Autowired;\nimport com.xxl.job.core.context.XxlJobHelper;\nimport com.xxl.job.core.handler.IJobHandler;\n\npublic class DemoGlueJobHandler extends IJobHandler {\n\n  	@Autowired\n  	private HelloService helloService;\n  \n	@Override\n	public void execute() throws Exception {\n		helloService.methodA();\n	}\n\n}\n', '执行HelloService(GLUE模式)', '2023-10-10 16:34:05', '2023-10-10 16:34:05');

-- ----------------------------
-- Table structure for xxl_job_registry
-- ----------------------------
DROP TABLE IF EXISTS `xxl_job_registry`;
CREATE TABLE `xxl_job_registry`  (
  `id` int NOT NULL AUTO_INCREMENT,
  `registry_group` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  `registry_key` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  `registry_value` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  `update_time` datetime NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `i_g_k_v`(`registry_group` ASC, `registry_key` ASC, `registry_value` ASC) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 28 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of xxl_job_registry
-- ----------------------------

-- ----------------------------
-- Table structure for xxl_job_user
-- ----------------------------
DROP TABLE IF EXISTS `xxl_job_user`;
CREATE TABLE `xxl_job_user`  (
  `id` int NOT NULL AUTO_INCREMENT,
  `username` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '账号',
  `password` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '密码',
  `role` tinyint NOT NULL COMMENT '角色：0-普通用户、1-管理员',
  `permission` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '权限：执行器ID列表，多个逗号分割',
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `i_username`(`username` ASC) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of xxl_job_user
-- ----------------------------
INSERT INTO `xxl_job_user` VALUES (1, 'admin', 'e10adc3949ba59abbe56e057f20f883e', 1, NULL);

SET FOREIGN_KEY_CHECKS = 1;
