/* eslint-disable */
export const printANSI = () => {
  let text = `
         ██████    ████████  ████████
         ██░░██    ██░░░░██  ██░░░░██
         ██░░██    ████░░██  ██░░████
         ██░░██      ██░░░░██░░░░██
         ██░░██      ████░░░░░░████
         ██░░██        ██░░░░░░██
 ██████  ██░░██      ████░░░░░░████
 ██░░██  ██░░██      ██░░░░██░░░░██
 ██░░██████░░██    ████░░██  ██░░████
 ██░░░░░░░░░░██    ██░░░░██  ██░░░░██
 ██████████████    ████████  ████████
 
 `
  console.log(`%c${text}`, 'color: #fc4d50')
  console.log('%c欢迎使用计算机协会招新系统', 'font-family: Hiragino Sans GB,Microsoft YaHei,\\\\5FAE\\8F6F\\96C5\\9ED1,Droid Sans Fallback,Source Sans,Wenquanyi Micro Hei,WenQuanYi Micro Hei Mono,WenQuanYi Zen Hei,Apple LiGothic Medium,SimHei,ST Heiti,WenQuanYi Zen Hei Sharp,sans-serif;')
}
