// eslint-disable-next-line
import { UserLayout, BasicLayout, BlankLayout } from '@/layouts'
import { bxAnaalyse } from '@/core/icons'

const RouteView = {
  name: 'RouteView', render: h => h('router-view')
}

export const asyncRouterMap = [{
  path: '/',
  name: 'index',
  component: BasicLayout,
  meta: { title: '主页' },
  redirect: '/account/center',
  children: [
    // 仪表盘
/*    {
      path: '/dashboard',
      name: 'dashboard',
      redirect: '/dashboard/workplace',
      component: RouteView,
      meta: { title: '系统', keepAlive: true, icon: bxAnaalyse, permission: ['dashboard'] },
      children: [
        {
          path: '/dashboard/analysis',
          name: 'Analysis',
          component: () => import('@/views/dashboard/Analysis'),
          meta: { title: '监控', keepAlive: false, permission: ['dashboard'] }
        },
        // 工作台界面
        {
          path: '/dashboard/log',
          name: 'Workplace',
          component: () => import('@/views/dashboard/Workplace'),
          meta: { title: '日志', keepAlive: true, permission: ['dashboard'] }
        }
      ]
    },*/
    // 系统
    {
      path: '/system',
      name: 'system',
      redirect: '/system/notice',
      component: RouteView,
      meta: { title: '系统', keepAlive: true, icon: bxAnaalyse, permission: ['system'] },
      children: [
        // 通知
        {
          path: '/system/notice',
          name: 'Notice',
          component: () => import('@/views/system/Notice.vue'),
          meta: { title: '系统通知', keepAlive: false, permission: ['notice'] }
        },
        // 邮件
        {
          path: '/system/mail',
          name: 'Mail',
          component: () => import('@/views/system/Mail.vue'),
          meta: { title: '系统邮件', keepAlive: false, permission: ['mail'] }
        },
        // 判题人设置
        {
          path: '/system/judger',
          name: 'Judger',
          component: () => import('@/views/system/JudgerManager.vue'),
          meta: { title: '评分设置', keepAlive: false, permission: ['system'] }
        },
        // 日志
        {
          path: '/system/log',
          name: 'Log',
          component: () => import('@/views/system/Log.vue'),
          meta: { title: '系统日志', keepAlive: true, permission: ['log'] }
        },
        // 系统设置
        {
          path: '/system/setting',
          name: 'Setting',
          component: () => import('@/views/system/Setting.vue'),
          meta: { title: '系统设置', keepAlive: true, permission: ['setting'] }
        }
      ]
    },
    // 表单
    /*{
      path: '/form',
      redirect: '/form/base-form',
      component: RouteView,
      meta: { title: 'menu.form', icon: 'form', permission: ['form'] },
      children: [
        // 基础表单
        {
          path: '/form/base-form',
          name: 'BaseForm',
          component: () => import('@/views/form/basicForm'),
          meta: { title: 'menu.form.basic-form', keepAlive: true, permission: ['form'] }
        },
        // 分步表单
        {
          path: '/form/step-form',
          name: 'StepForm',
          component: () => import('@/views/form/stepForm/StepForm'),
          meta: { title: 'menu.form.step-form', keepAlive: true, permission: ['form'] }
        }, {
          path: '/form/advanced-form',
          name: 'AdvanceForm',
          component: () => import('@/views/form/advancedForm/AdvancedForm'),
          meta: { title: 'menu.form.advanced-form', keepAlive: true, permission: ['form'] }
        }]
    },*/
    // 列表页面
    /*{
      path: '/list',
      name: 'list',
      component: RouteView,
      redirect: '/list/table-list',
      meta: { title: 'menu.list', icon: 'table', permission: ['table'] },
      children: [{
        path: '/list/table-list/:pageNo([1-9]\\d*)?',
        name: 'TableListWrapper',
        hideChildrenInMenu: true, // 强制显示 MenuItem 而不是 SubMenu
        component: () => import('@/views/list/TableList'),
        meta: { title: 'menu.list.table-list', keepAlive: true, permission: ['table'] }
      }, {
        path: '/list/basic-list',
        name: 'BasicList',
        component: () => import('@/views/list/BasicList'),
        meta: { title: 'menu.list.basic-list', keepAlive: true, permission: ['table'] }
      }, {
        path: '/list/card',
        name: 'CardList',
        component: () => import('@/views/list/CardList'),
        meta: { title: 'menu.list.card-list', keepAlive: true, permission: ['table'] }
      }, {
        path: '/list/search',
        name: 'SearchList',
        component: () => import('@/views/list/search/SearchLayout'),
        redirect: '/list/search/article',
        meta: { title: 'menu.list.search-list', keepAlive: true, permission: ['table'] },
        children: [{
          path: '/list/search/article',
          name: 'SearchArticles',
          component: () => import('../views/list/search/Article'),
          meta: { title: 'menu.list.search-list.articles', permission: ['table'] }
        }, {
          path: '/list/search/project',
          name: 'SearchProjects',
          component: () => import('../views/list/search/Projects'),
          meta: { title: 'menu.list.search-list.projects', permission: ['table'] }
        }, {
          path: '/list/search/application',
          name: 'SearchApplications',
          component: () => import('../views/list/search/Applications'),
          meta: { title: 'menu.list.search-list.applications', permission: ['table'] }
        }]
      }]
    },*/
    
    // 详情页
    /*{
      path: '/profile',
      name: 'profile',
      component: RouteView,
      redirect: '/profile/basic',
      meta: { title: 'menu.profile', icon: 'profile', permission: ['profile'] },
      children: [{
        path: '/profile/basic',
        name: 'ProfileBasic',
        component: () => import('@/views/profile/basic'),
        meta: { title: 'menu.profile.basic', permission: ['profile'] }
      }, {
        path: '/profile/advanced',
        name: 'ProfileAdvanced',
        component: () => import('@/views/profile/advanced/Advanced'),
        meta: { title: 'menu.profile.advanced', permission: ['profile'] }
      }]
    },*/
    
    // 结果页面
    /*{
      path: '/result',
      name: 'result',
      component: RouteView,
      redirect: '/result/success',
      meta: { title: 'menu.result', icon: 'check-circle-o', permission: ['result'] },
      children: [// 成功
        {
          path: '/result/success',
          name: 'ResultSuccess',
          component: () => import(/!* webpackChunkName: "result" *!/ '@/views/result/Success'),
          meta: { title: 'menu.result.success', keepAlive: false, hiddenHeaderContent: true, permission: ['result'] }
        }, // 失败
        {
          path: '/result/fail',
          name: 'ResultFail',
          component: () => import(/!* webpackChunkName: "result" *!/ '@/views/result/Error'),
          meta: { title: 'menu.result.fail', keepAlive: false, hiddenHeaderContent: true, permission: ['result'] }
        }]
    },*/
    
    // 异常界面
    {
      path: '/exception',
      name: 'exception',
      component: RouteView,
      redirect: '/exception/403',
      meta: { title: 'menu.exception', icon: 'warning', permission: ['exception'] },
      children: [// 403界面
        {
          path: '/exception/403',
          name: 'Exception403',
          component: () => import( '@/views/exception/403'),
          meta: { title: 'menu.exception.not-permission', permission: ['exception'] }
        }, // 404界面
        {
          path: '/exception/404',
          name: 'Exception404',
          component: () => import( '@/views/exception/404'),
          meta: { title: 'menu.exception.not-find', permission: ['exception'] }
        }, // 500界面
        {
          path: '/exception/500',
          name: 'Exception500',
          component: () => import( '@/views/exception/500'),
          meta: { title: 'menu.exception.server-error', permission: ['exception'] }
        }]
    },
    
    // 题目设置页面
    {
      path: '/topic',
      name: 'topic',
      component: RouteView,
      redirect: '/topic/setTopic',
      meta: { title: '设题', icon: 'edit', keepAlive: true, permission: ['setTopic'] },
      children: [
        {
          path: '/topic/setTopic',
          name: 'setTopic',
          component: () => import('@/views/topic/SetTopic.vue'),
          meta: {
            title: '题目设置', keepAlive: true, permission: ['setTopic']
          }
        },
        {
          path: '/topic/updateTopic',
          name: 'updateTopic',
          component: () => import('@/views/topic/UpdateTopic.vue'),
          meta: {
            title: '我的设题', keepAlive: true, permission: ['setTopic']
          }
        },
      ]
    },
    
    // 题目页面
    {
      path: '/reply',
      name: 'reply',
      component: RouteView,
      redirect: '/reply/question',
      meta: { title: '答题', icon: 'code', keepAlive: true, permission: ['reply'] },
      children: [
        {
          path: '/reply/list',
          name: 'questionList',
          component: () => import('@/views/reply/QuestionList.vue'),
          meta: {
            title: '题目列表', keepAlive: true, permission: ['reply']
          }
        },
        {
          path: '/reply/question',
          name: 'question',
          // 不展示在菜单栏中
          hidden: true,
          component: () => import('@/views/reply/QuestionReply.vue'),
          meta: {
            title: '答题', keepAlive: true, permission: ['reply']
          }
        }
      ]
    },
    
    // 判题页面
    {
      path: '/judge',
      name: 'judge',
      component: RouteView,
      redirect: '/judge/prog',
      meta: { title: '判题', icon: 'calculator', keepAlive: true, permission: ['judge'] },
      children: [
        {
          path: '/judge/subm',
          name: 'judgeSubm',
          component: () => import('@/views/judge/SubmissionList.vue'),
          meta: {
            title: '提交情况', keepAlive: true, permission: ['judge']
          }
        },
        {
          path: '/judge/prog',
          name: 'judgeProg',
          component: () => import('@/views/judge/QuestionJudge.vue'),
          meta: {
            title: '题目评分', keepAlive: true, permission: ['judge']
          }
        },
        {
          path: '/judge/detail',
          name: 'judgeDetail',
          hidden: true,
          component: () => import('@/views/judge/JudgeDetail.vue'),
          meta: {
            title: '评分细节', keepAlive: true, permission: ['judge']
          }
        }
      ]
    },
    // 新人查询界面
    {
      path: '/newcomer',
      name: 'newcomer',
      component: RouteView,
      redirect: '/newcomer/list',
      meta: { title: '新人', icon: 'reddit', keepAlive: true, permission: ['newcomer'] },
      children: [
        {
          path: '/newcomer/list',
          name: 'newcomerList',
          component: () => import('@/views/newcomer/NewcomerPage.vue'),
          meta: {
            title: '新人查询', keepAlive: true, permission: ['newcomer']
          }
        }
      ]
    },
    
    // 用户页面
    {
      path: '/account',
      component: RouteView,
      redirect: '/account/center',
      name: 'account',
      meta: { title: '个人', icon: 'user', keepAlive: true, permission: ['user'] },
      children: [
        // 个人中心
        {
          path: '/account/center',
          name: 'center',
          component: () => import('@/views/account/center'),
          meta: { title: '个人中心', keepAlive: true, permission: ['user'] }
        },
        // 录取结果
        {
          path: '/account/enroll',
          name: 'enroll',
          component: () => import('@/views/account/enroll/EnrollResult.vue'),
          meta: { title: '录取结果', keepAlive: true, permission: ['user'] }
        },
        // 个人设置
        {
          path: '/account/settings',
          name: 'settings',
          component: () => import('@/views/account/settings/Index'),
          meta: { title: '个人设置', hideHeader: true, permission: ['user'] },
          redirect: '/account/settings/basic',
          hideChildrenInMenu: true,
          children: [
            {
              path: '/account/settings/basic',
              name: 'BasicSettings',
              component: () => import('@/views/account/settings/BasicSetting'),
              meta: { title: '基本设置', hidden: true, permission: ['user'] }
            }, {
              path: '/account/settings/security',
              name: 'SecuritySettings',
              component: () => import('@/views/account/settings/Security'),
              meta: {
                title: '安全设置', hidden: true, keepAlive: true, permission: ['user']
              }
            }, {
              path: '/account/settings/custom',
              name: 'CustomSettings',
              component: () => import('@/views/account/settings/Custom'),
              meta: { title: '个性化', hidden: true, keepAlive: true, permission: ['user'] }
            }, {
              path: '/account/settings/binding',
              name: 'BindingSettings',
              component: () => import('@/views/account/settings/Binding'),
              meta: { title: '账号绑定', hidden: true, keepAlive: true, permission: ['user'] }
            }, {
              path: '/account/settings/notification',
              name: 'NotificationSettings',
              component: () => import('@/views/account/settings/Notification'),
              meta: {
                title: '新消息通知', hidden: true, keepAlive: true, permission: ['user']
              }
            }
          ]
        }]
    },
    
    // other
    /*{
      path: '/other',
      name: 'otherPage',
      component: RouteView,
      meta: { title: '其他组件', icon: 'slack', permission: [ 'superAdmin' ] },
      redirect: '/other/icon-selector',
      children: [
        {
          path: '/other/icon-selector',
          name: 'TestIconSelect',
          component: () => import('@/views/other/IconSelectorView'),
          meta: { title: 'IconSelector', icon: 'tool', keepAlive: true, permission: [ 'superAdmin' ] }
        },
        {
          path: '/other/list',
          component: RouteView,
          meta: { title: '业务布局', icon: 'layout', permission: [ 'superAdmin' ] },
          redirect: '/other/list/tree-list',
          children: [
            {
              path: '/other/list/tree-list',
              name: 'TreeList',
              component: () => import('@/views/other/TreeList'),
              meta: { title: '树目录表格', keepAlive: true }
            },
            {
              path: '/other/list/edit-table',
              name: 'EditList',
              component: () => import('@/views/other/TableInnerEditList'),
              meta: { title: '内联编辑表格', keepAlive: true }
            },
            {
              path: '/other/list/user-list',
              name: 'UserList',
              component: () => import('@/views/other/UserList'),
              meta: { title: '用户列表', keepAlive: true }
            },
            {
              path: '/other/list/role-list',
              name: 'RoleList',
              component: () => import('@/views/other/RoleList'),
              meta: { title: '角色列表', keepAlive: true }
            },
            {
              path: '/other/list/system-role',
              name: 'SystemRole',
              component: () => import('@/views/other/RoleList'),
              meta: { title: '角色列表2', keepAlive: true }
            },
            {
              path: '/other/list/permission-list',
              name: 'PermissionList',
              component: () => import('@/views/other/PermissionList'),
              meta: { title: '权限列表', keepAlive: true }
            }
          ]
        }
      ]
    }*/
  ]
}, {
  path: '*', redirect: '/404', hidden: true
}]

/**
 * 基础路由
 * @type { *[] }
 */
export const constantRouterMap = [// 用户登录界面
  {
    path: '/user', component: UserLayout, redirect: '/user/login', hidden: true, children: [
      {
        path: 'login', name: 'login', component: () => import( '@/views/user/Login')
      }, {
        path: 'register',
        name: 'register',
        component: () => import('@/views/user/Register')
      }, {
        path: 'register-result',
        name: 'registerResult',
        component: () => import('@/views/user/RegisterResult')
      }, {
        path: 'recover', name: 'recover', component: undefined
      }
    ]
  },
  
  {
    path: '/404', component: () => import( '@/views/exception/404')
  },
  {
    path: '/403', component: () => import( '@/views/exception/403')
  },
  {
    path: '/500', component: () => import( '@/views/exception/500')
  }
]
