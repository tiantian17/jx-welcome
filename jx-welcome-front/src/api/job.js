import request from '@/utils/request'

export function allotNewcomerJob(value) {
  return request({
    url: '/job-bombed-bug/fuckAllot',
    method: 'get',
    params: value
  })
}

export function fileClearJob(value) {
  return request({
    url: '/job-bombed-bug/fuckFileClear',
    method: 'get',
    params: value
  })
}

export function calcTotalScoreJob(value) {
  return request({
    url: '/job-bombed-bug/calcTotalScore',
    method: 'get',
    params: value
  })
}

export function deleteAdminReplys(value) {
  return request({
    url: '/job-bombed-bug/deleteAdminReplys',
    method: 'get',
    params: value
  })
}

export function setStatus(value) {
  return request({
    url: '/job-bombed-bug/setStatus',
    method: 'get',
    params: value
  })
}