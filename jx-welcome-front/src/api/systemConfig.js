import request from '@/utils/request'


export function setAnsTime(parameter) {
  return request({
    url: '/system/config/setAnswerTime',
    method: 'post',
    data: parameter
  })
}

export function getAnsTime() {
  return request({
    url: '/system/config/getAnswerTime',
    method: 'get'
  })
}

export function setRegStatus(parameter) {
  return request({
    url: `/system/config/setRegStatus/${parameter}`,
    method: 'put',
  })
}

export function getRegStatus() {
  return request({
    url: '/system/config/getRegStatus',
    method: 'get'
  })
}

export function setSecondRoundInfo(parameter) {
  return request({
    url: '/system/config/setSecondRoundInfo',
    method: 'post',
    data: parameter
  })
}

export function getSecondRoundInfo() {
  return request({
    url: '/system/config/getSecondRoundInfo',
    method: 'get'
  })
}

export function setScoreLine(parameter){
  return request({
    url: '/system/config/setScoreLine',
    method: 'post',
    params: parameter
  })
}

export function getScoreLine(){
  return request({
    url: '/system/config/getScoreLine',
    method: 'get'
  })
}

