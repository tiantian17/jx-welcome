import request from '@/utils/request'


// 获取新人列表
export function getNewcomerList(parameter) {
  return request({
    url: '/newcomer/list',
    method: 'get',
    params: parameter
  })
}

// 根据用户ID查询新人
export function getNewcomerByUserId(userId) {
  return request({
    url: '/newcomer',
    method: 'get',
    params: {
      userId
    }
  })
}

// 更新新人信息
export function updateNewcomer(parameter) {
  return request({
    url: '/newcomer/update',
    method: 'put',
    data: parameter
  })
}

// 一键录取
export function onekeyEnroll(userId) {
  return request({
    url: `/newcomer/onekeyEnroll/${userId}`,
    method: 'put',
  })
}

// 获取录取步骤的信息
export function getEnrollStepInfo() {
  return request({
    url: '/system/enrollResult',
    method: 'get',
  })
}