import request from '@/utils/request'

/**
 * 获取题目列表
 * @param parameter
 * @returns {*}
 */
export function getTopicList(parameter) {
  return request({
    url: '/topic/list',
    method: 'get',
    params: parameter
  })
}

/**
 * 获取题目列表
 * @param topicId
 * @returns {*}
 */
export function getTopicDetail(topicId) {
  return request({
    url: `/topic/${topicId}`,
    method: 'get'
  })
}

export function getTopicDetailAndMyAns(topicId) {
  return request({
    url: `/reply/myAns/${topicId}`,
    method: 'get'
  })
}


export function replyAns(parameter) {
  return request({
    url: '/reply/ans',
    method: 'post',
    data: parameter
  })
}

export function updateAns(parameter) {
  return request({
    url: '/reply/updateAns',
    method: 'put',
    data: parameter
  })
}

export function getTopicTotal() {
  return request({
    url: '/topic/total',
    method: 'get'
  })
}
