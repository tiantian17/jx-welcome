import request from '@/utils/request'

export const set1Topic = () => {
  return request.get('/', {
    params: {}
  })
}

/**
 * 设置题目
 * @param parameter
 * @returns {*}
 */
export function setTopic(parameter) {
  return request({
    url: '/topic/setting',
    method: 'post',
    data: parameter
  })
}


export function getAuthorTopicList(parameter) {
  return request({
    url: '/topic/list/author',
    method: 'get',
    params: parameter
  })
}


/**
 * 修改我设置的题目
 * @param parameter
 * @returns {*}
 */
export function updateMySetTopic(parameter) {
  return request({
    url: '/topic/update',
    method: 'put',
    data: parameter
  })
}

/**
 * 删除我设置的题目
 * @param parameter
 * @returns {*}
 */
export function delMySetTopic(parameter) {
  return request({
    url: `/topic/del?topicId=${parameter}`,
    method: 'delete'
  })
}

export function getSubList(parameter) {
  return request({
    url: '/judge/subInfo',
    method: 'get',
    params: parameter
  })
}