import request from '@/utils/request'


export function setNotice(parameter) {
  return request({
    url: '/system/notice',
    method: 'post',
    data: parameter
  })
}

export function getNoticeHistoryList(parameter) {
  return request({
    url: '/system/notice/list',
    method: 'get',
    params: parameter
  })
}

export function delNoticeById(noticeId) {
  return request({
    url: `/system/notice/del/${noticeId}`,
    method: 'delete'
  })
}

export function sendAllEmail(parameter) {
  return request({
    url: '/mail/sendAll',
    method: 'post',
    data: parameter
  })
}

export function getSystemEmail(){
  return request({
    url: '/mail/all',
    method: 'get'
  })
}