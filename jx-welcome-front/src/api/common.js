import request from '@/utils/request'
import axios from 'axios'

export function getToken() {
  return JSON.parse(localStorage.getItem('jxtoken'))
}

const baseURL = process.env.VUE_APP_API_BASE_URL

// 图床:图片上传(30天后自动清理)
export const drawingBedUploadImg = ({ file }) => {
  const formData = new FormData()
  formData.append('file', file)
  return request({
    url: '/file/drawingBed',
    method: 'POST',
    data: formData,
    headers: {
      'Content-type': 'multipart/form-data'
    }
  })
}

// 普通:图片上传
export const uploadImgService = ($file) => {
  let formData = new FormData()
  formData.append('file', $file)
  return request({
    url: '/file/drawingBed',
    method: 'POST',
    data: formData,
    headers: {
      'Content-type': 'multipart/form-data'
    }
  })
}

// 图片删除
export const delImgService = (url) => {
  return request({
    url: '/file/removePic',
    params: {
      url
    },
    method: 'delete'
  })
}

export const changPwd = (parameter) => {
  return request({
    url: '/system/user/profile/updatePwd',
    method: 'post',
    data: parameter
  })
}

export const updateEmail = (parameter) => {
  return request({
    url: '/system/user/profile/updateEmail',
    method: 'put',
    params: parameter
  })
}

export const getUpdateEmailCaptcha = (parameter) => {
  return request({
    url: '/updateEmail',
    method: 'get',
    params: parameter
  })
}

export const getUserInfo = () => {
  return request({
    url: '/system/user/profile',
    method: 'get'
  })
}

export const changeUserInfo = (parameter) => {
  return request({
    url: '/system/user/profile',
    method: 'put',
    data: parameter
  })
}

export function exportExcel(type) {
  return axios({
    url: `${baseURL}/system/export/${type}`,
    method: 'get',
    responseType: 'arraybuffer',
    headers: {
      jxtoken: getToken()
    }
  })
}

export function exportJudgeResultExcel(){
  return axios({
    url: `${baseURL}/system/export/result/judge`,
    method: 'get',
    responseType: 'arraybuffer',
    headers: {
      jxtoken: getToken()
    }
  })
}

export function setAvatar(file) {
  let formData = new FormData()
  formData.append('avatarFile', file)
  return request({
    url: '/file/uploadAvatar',
    method: 'post',
    data: formData,
    headers: {
      'Content-type': 'multipart/form-data'
    }
  })
}

export function removeSourceAvatar(avatarURL) {
  return request({
    url: '/file/removeSourceAvatar',
    method: 'delete',
    params: avatarURL
  })
}