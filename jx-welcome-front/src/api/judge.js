import request from '@/utils/request'


export function getJudgeList(parameter) {
  return request({
    url: '/judge/list',
    method: 'get',
    params: parameter
  })
}

export function getJudgeInfo(parameter) {
  return request({
    url: `/judge/info/${parameter}`,
    method: 'get'
  })
}

export function getJudgeDetailList(parameter) {
  return request({
    url: `/judge/detailList/${parameter}`,
    method: 'get'
  })
}

export function getSubTotal(){
  return request({
    url: '/judge/subTotal',
    method: 'get'
  })
}

export function setJudgeScore(parameter) {
  return request({
    url: '/judge/setScore',
    method: 'post',
    data: parameter
  })
}

export function getCandidate() {
  return request({
    url: '/judge/candidate',
    method: 'get'
  })
}

export function addJudger(parameter) {
  return request({
    url: '/judge/judger',
    method: 'post',
    data: parameter
  })
}

export function getJudgerList(parameter) {
  return request({
    url: '/judge/judgerList',
    method: 'get',
    params: parameter
  })
}

export function delJudger(parameter) {
  return request({
    url: `/judge/delJudger/${parameter}`,
    method: 'delete'
  })
}