import request from '@/utils/request'


export function getLogList(parameter) {
  return request({
    url: '/log/operLog/list',
    method: 'get',
    params: parameter
  })
}