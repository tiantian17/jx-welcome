import request from '@/utils/request'

const userApi = {
  Login: '/auth/login',
  Logout: '/auth/logout',
  Register: '/auth/register',
  UserInfo: '/user/info',
  UserMenu: '/user/nav'
}

/**
 * 登录接口
 * parameter: {
 *     username: '',
 *     password: '',
 *     remember_me: true,
 * }
 * @param parameter
 * @returns {*}
 */
export function login(parameter) {
  return request({
    url: '/auth/login',
    method: 'post',
    data: parameter
  })
}

/**
 * 信息获取用户信息(需要token)
 * @returns {*}
 */
export function getInfo() {
  return request({
    url: '/auth/getInfo',
    method: 'get',
    headers: {
      'Content-Type': 'application/json;charset=UTF-8'
    }
  })
}

/**
 * 退出接口(需要token)
 * @returns {*}
 */
export function logout() {
  return request({
    url: '/auth/logout',
    method: 'post',
    headers: {
      'Content-Type': 'application/json;charset=UTF-8'
    }
  })
}

/**
 * 获取验证码
 * @param parameter
 */
export function getEmailCaptcha(parameter) {
  return request({
    url: '/captchaEmail',
    method: 'get',
    params: parameter
  })
}

export function register(parameter) {
  return request({
    url: '/register',
    method: 'post',
    data: parameter
  })
}

/**
 * 获取当前用户菜单
 * @returns {*}
 */
export function getCurrentUserNav() {
  return request({
    url: '/user/nav',
    method: 'get'
  })
}