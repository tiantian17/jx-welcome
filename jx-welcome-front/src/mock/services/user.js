import Mock from 'mockjs2'
import { builder } from '../util'

const info = options => {
  console.log('options', options)
  const result = {}
  const userInfo = {
    id: '4291d7da9005377ec9aec4a71ea837f',
    name: '天野远子',
    username: 'admin',
    password: '',
    avatar: '/avatar2.jpg',
    status: 1,
    telephone: '',
    lastLoginIp: '27.154.74.117',
    lastLoginTime: 1534837621348,
    creatorId: 'admin',
    createTime: 1497160610259,
    merchantCode: 'TLif2btpzg079h15bk',
    deleted: 0,
    roleId: 'admin'
  }
  result.userInfo = userInfo
  // role
  const roleObj = {
    id: 'admin',
    name: '管理员',
    describe: '拥有所有权限',
    status: 1,
    creatorId: 'system',
    createTime: 1497160610259,
    deleted: 0,
    permissions: [
      {
        roleId: 'admin',
        permissionId: 'dashboard',
        permissionName: '仪表盘',
        actionEntitySet: [
          {
            action: 'add',
            describe: '新增',
            defaultCheck: false
          },
          {
            action: 'query',
            describe: '查询',
            defaultCheck: false
          },
          {
            action: 'get',
            describe: '详情',
            defaultCheck: false
          },
          {
            action: 'update',
            describe: '修改',
            defaultCheck: false
          },
          {
            action: 'delete',
            describe: '删除',
            defaultCheck: false
          }
        ]
      },
      {
        roleId: 'admin',
        permissionId: 'exception',
        permissionName: '异常页面权限',
        
        actionEntitySet: [
          {
            action: 'add',
            describe: '新增',
            defaultCheck: false
          },
          {
            action: 'query',
            describe: '查询',
            defaultCheck: false
          },
          {
            action: 'get',
            describe: '详情',
            defaultCheck: false
          },
          {
            action: 'update',
            describe: '修改',
            defaultCheck: false
          },
          {
            action: 'delete',
            describe: '删除',
            defaultCheck: false
          }
        ]
      },
      {
        roleId: 'admin',
        permissionId: 'result',
        permissionName: '结果权限',
        actionEntitySet: [
          {
            action: 'add',
            describe: '新增',
            defaultCheck: false
          },
          {
            action: 'query',
            describe: '查询',
            defaultCheck: false
          },
          {
            action: 'get',
            describe: '详情',
            defaultCheck: false
          },
          {
            action: 'update',
            describe: '修改',
            defaultCheck: false
          },
          {
            action: 'delete',
            describe: '删除',
            defaultCheck: false
          }
        ]
      },
      {
        roleId: 'admin',
        permissionId: 'profile',
        permissionName: '详细页权限',
        actionEntitySet: [
          {
            action: 'add',
            describe: '新增',
            defaultCheck: false
          },
          {
            action: 'query',
            describe: '查询',
            defaultCheck: false
          },
          {
            action: 'get',
            describe: '详情',
            defaultCheck: false
          },
          {
            action: 'update',
            describe: '修改',
            defaultCheck: false
          },
          {
            action: 'delete',
            describe: '删除',
            defaultCheck: false
          }
        ]
      },
      {
        roleId: 'admin',
        permissionId: 'table',
        permissionName: '表格权限',
        actionEntitySet: [
          {
            action: 'add',
            describe: '新增',
            defaultCheck: false
          },
          {
            action: 'import',
            describe: '导入',
            defaultCheck: false
          },
          {
            action: 'get',
            describe: '详情',
            defaultCheck: false
          },
          {
            action: 'update',
            describe: '修改',
            defaultCheck: false
          }
        ]
      },
      {
        roleId: 'admin',
        permissionId: 'form',
        permissionName: '表单权限',
        actionEntitySet: [
          {
            action: 'add',
            describe: '新增',
            defaultCheck: false
          },
          {
            action: 'get',
            describe: '详情',
            defaultCheck: false
          },
          {
            action: 'query',
            describe: '查询',
            defaultCheck: false
          },
          {
            action: 'update',
            describe: '修改',
            defaultCheck: false
          },
          {
            action: 'delete',
            describe: '删除',
            defaultCheck: false
          }
        ]
      },
      {
        roleId: 'admin',
        permissionId: 'order',
        permissionName: '订单管理',
        actions:
          '[{"action":"add","defaultCheck":false,"describe":"新增"},{"action":"query","defaultCheck":false,"describe":"查询"},{"action":"get","defaultCheck":false,"describe":"详情"},{"action":"update","defaultCheck":false,"describe":"修改"},{"action":"delete","defaultCheck":false,"describe":"删除"}]',
        actionEntitySet: [
          {
            action: 'add',
            describe: '新增',
            defaultCheck: false
          },
          {
            action: 'query',
            describe: '查询',
            defaultCheck: false
          },
          {
            action: 'get',
            describe: '详情',
            defaultCheck: false
          },
          {
            action: 'update',
            describe: '修改',
            defaultCheck: false
          },
          {
            action: 'delete',
            describe: '删除',
            defaultCheck: false
          }
        ]
      },
      {
        roleId: 'admin',
        permissionId: 'permission',
        permissionName: '权限管理',
        actionEntitySet: [
          {
            action: 'add',
            describe: '新增',
            defaultCheck: false
          },
          {
            action: 'get',
            describe: '详情',
            defaultCheck: false
          },
          {
            action: 'update',
            describe: '修改',
            defaultCheck: false
          },
          {
            action: 'delete',
            describe: '删除',
            defaultCheck: false
          }
        ]
      },
      {
        roleId: 'admin',
        permissionId: 'role',
        permissionName: '角色管理',
        actions:
          '[{"action":"add","defaultCheck":false,"describe":"新增"},{"action":"get","defaultCheck":false,"describe":"详情"},{"action":"update","defaultCheck":false,"describe":"修改"},{"action":"delete","defaultCheck":false,"describe":"删除"}]',
        actionEntitySet: [
          {
            action: 'add',
            describe: '新增',
            defaultCheck: false
          },
          {
            action: 'get',
            describe: '详情',
            defaultCheck: false
          },
          {
            action: 'update',
            describe: '修改',
            defaultCheck: false
          },
          {
            action: 'delete',
            describe: '删除',
            defaultCheck: false
          }
        ]
      },
      {
        roleId: 'admin',
        permissionId: 'table',
        permissionName: '桌子管理',
        actionEntitySet: [
          {
            action: 'add',
            describe: '新增',
            defaultCheck: false
          },
          {
            action: 'get',
            describe: '详情',
            defaultCheck: false
          },
          {
            action: 'query',
            describe: '查询',
            defaultCheck: false
          },
          {
            action: 'update',
            describe: '修改',
            defaultCheck: false
          },
          {
            action: 'delete',
            describe: '删除',
            defaultCheck: false
          }
        ]
      },
      {
        roleId: 'admin',
        permissionId: 'user',
        permissionName: '用户管理',
        actionEntitySet: [
          {
            action: 'add',
            describe: '新增',
            defaultCheck: false
          },
          {
            action: 'import',
            describe: '导入',
            defaultCheck: false
          },
          {
            action: 'get',
            describe: '详情',
            defaultCheck: false
          },
          {
            action: 'update',
            describe: '修改',
            defaultCheck: false
          },
          {
            action: 'delete',
            describe: '删除',
            defaultCheck: false
          },
          {
            action: 'export',
            describe: '导出',
            defaultCheck: false
          }
        ]
      },
      // TODO 赋予权限
      {
        roleId: 'admin',
        permissionId: 'reply',
        permissionName: '答题界面',
        actionEntitySet: [
          {
            action: 'add',
            describe: '新增',
            defaultCheck: false
          },
          {
            action: 'query',
            describe: '查询',
            defaultCheck: false
          },
          {
            action: 'get',
            describe: '详情',
            defaultCheck: false
          },
          {
            action: 'update',
            describe: '修改',
            defaultCheck: false
          },
          {
            action: 'delete',
            describe: '删除',
            defaultCheck: false
          }
        ]
      },
      {
        roleId: 'admin',
        permissionId: 'newcomer',
        permissionName: '新人查询',
        actionEntitySet: [
          {
            action: 'add',
            describe: '新增',
            defaultCheck: false
          },
          {
            action: 'query',
            describe: '查询',
            defaultCheck: false
          },
          {
            action: 'get',
            describe: '详情',
            defaultCheck: false
          },
          {
            action: 'update',
            describe: '修改',
            defaultCheck: false
          },
          {
            action: 'delete',
            describe: '删除',
            defaultCheck: false
          }
        ]
      },
      {
        roleId: 'admin',
        permissionId: 'setTopic',
        permissionName: '题目设置',
        actionEntitySet: [
          {
            action: 'add',
            describe: '新增',
            defaultCheck: false
          },
          {
            action: 'query',
            describe: '查询',
            defaultCheck: false
          },
          {
            action: 'get',
            describe: '详情',
            defaultCheck: false
          },
          {
            action: 'update',
            describe: '修改',
            defaultCheck: false
          },
          {
            action: 'delete',
            describe: '删除',
            defaultCheck: false
          }
        ]
      },
      {
        roleId: 'admin',
        permissionId: 'judge',
        permissionName: '判题',
        actionEntitySet: [
          {
            action: 'query',
            describe: '查询',
            defaultCheck: false
          },
          {
            action: 'get',
            describe: '详情',
            defaultCheck: false
          },
          {
            action: 'update',
            describe: '修改',
            defaultCheck: false
          },
          {
            action: 'delete',
            describe: '删除',
            defaultCheck: false
          }
        ]
      }
    ]
  }
  result.role = roleObj
  const permissions = [
    {
      roleId: 'admin',
      permissionId: 'dashboard',
      permissionName: '仪表盘',
      actionEntitySet: [
        {
          action: 'add',
          describe: '新增',
          defaultCheck: false
        },
        {
          action: 'query',
          describe: '查询',
          defaultCheck: false
        },
        {
          action: 'get',
          describe: '详情',
          defaultCheck: false
        },
        {
          action: 'update',
          describe: '修改',
          defaultCheck: false
        },
        {
          action: 'delete',
          describe: '删除',
          defaultCheck: false
        }
      ]
    },
    {
      roleId: 'admin',
      permissionId: 'exception',
      permissionName: '异常页面权限',
      
      actionEntitySet: [
        {
          action: 'add',
          describe: '新增',
          defaultCheck: false
        },
        {
          action: 'query',
          describe: '查询',
          defaultCheck: false
        },
        {
          action: 'get',
          describe: '详情',
          defaultCheck: false
        },
        {
          action: 'update',
          describe: '修改',
          defaultCheck: false
        },
        {
          action: 'delete',
          describe: '删除',
          defaultCheck: false
        }
      ]
    },
    {
      roleId: 'admin',
      permissionId: 'result',
      permissionName: '结果权限',
      actionEntitySet: [
        {
          action: 'add',
          describe: '新增',
          defaultCheck: false
        },
        {
          action: 'query',
          describe: '查询',
          defaultCheck: false
        },
        {
          action: 'get',
          describe: '详情',
          defaultCheck: false
        },
        {
          action: 'update',
          describe: '修改',
          defaultCheck: false
        },
        {
          action: 'delete',
          describe: '删除',
          defaultCheck: false
        }
      ]
    },
    {
      roleId: 'admin',
      permissionId: 'profile',
      permissionName: '详细页权限',
      actionEntitySet: [
        {
          action: 'add',
          describe: '新增',
          defaultCheck: false
        },
        {
          action: 'query',
          describe: '查询',
          defaultCheck: false
        },
        {
          action: 'get',
          describe: '详情',
          defaultCheck: false
        },
        {
          action: 'update',
          describe: '修改',
          defaultCheck: false
        },
        {
          action: 'delete',
          describe: '删除',
          defaultCheck: false
        }
      ]
    },
    {
      roleId: 'admin',
      permissionId: 'table',
      permissionName: '表格权限',
      actionEntitySet: [
        {
          action: 'add',
          describe: '新增',
          defaultCheck: false
        },
        {
          action: 'import',
          describe: '导入',
          defaultCheck: false
        },
        {
          action: 'get',
          describe: '详情',
          defaultCheck: false
        },
        {
          action: 'update',
          describe: '修改',
          defaultCheck: false
        }
      ]
    },
    {
      roleId: 'admin',
      permissionId: 'form',
      permissionName: '表单权限',
      actionEntitySet: [
        {
          action: 'add',
          describe: '新增',
          defaultCheck: false
        },
        {
          action: 'get',
          describe: '详情',
          defaultCheck: false
        },
        {
          action: 'query',
          describe: '查询',
          defaultCheck: false
        },
        {
          action: 'update',
          describe: '修改',
          defaultCheck: false
        },
        {
          action: 'delete',
          describe: '删除',
          defaultCheck: false
        }
      ]
    },
    {
      roleId: 'admin',
      permissionId: 'order',
      permissionName: '订单管理',
      actions:
        '[{"action":"add","defaultCheck":false,"describe":"新增"},{"action":"query","defaultCheck":false,"describe":"查询"},{"action":"get","defaultCheck":false,"describe":"详情"},{"action":"update","defaultCheck":false,"describe":"修改"},{"action":"delete","defaultCheck":false,"describe":"删除"}]',
      actionEntitySet: [
        {
          action: 'add',
          describe: '新增',
          defaultCheck: false
        },
        {
          action: 'query',
          describe: '查询',
          defaultCheck: false
        },
        {
          action: 'get',
          describe: '详情',
          defaultCheck: false
        },
        {
          action: 'update',
          describe: '修改',
          defaultCheck: false
        },
        {
          action: 'delete',
          describe: '删除',
          defaultCheck: false
        }
      ]
    },
    {
      roleId: 'admin',
      permissionId: 'permission',
      permissionName: '权限管理',
      actionEntitySet: [
        {
          action: 'add',
          describe: '新增',
          defaultCheck: false
        },
        {
          action: 'get',
          describe: '详情',
          defaultCheck: false
        },
        {
          action: 'update',
          describe: '修改',
          defaultCheck: false
        },
        {
          action: 'delete',
          describe: '删除',
          defaultCheck: false
        }
      ]
    },
    {
      roleId: 'admin',
      permissionId: 'role',
      permissionName: '角色管理',
      actions:
        '[{"action":"add","defaultCheck":false,"describe":"新增"},{"action":"get","defaultCheck":false,"describe":"详情"},{"action":"update","defaultCheck":false,"describe":"修改"},{"action":"delete","defaultCheck":false,"describe":"删除"}]',
      actionEntitySet: [
        {
          action: 'add',
          describe: '新增',
          defaultCheck: false
        },
        {
          action: 'get',
          describe: '详情',
          defaultCheck: false
        },
        {
          action: 'update',
          describe: '修改',
          defaultCheck: false
        },
        {
          action: 'delete',
          describe: '删除',
          defaultCheck: false
        }
      ]
    },
    {
      roleId: 'admin',
      permissionId: 'table',
      permissionName: '桌子管理',
      actionEntitySet: [
        {
          action: 'add',
          describe: '新增',
          defaultCheck: false
        },
        {
          action: 'get',
          describe: '详情',
          defaultCheck: false
        },
        {
          action: 'query',
          describe: '查询',
          defaultCheck: false
        },
        {
          action: 'update',
          describe: '修改',
          defaultCheck: false
        },
        {
          action: 'delete',
          describe: '删除',
          defaultCheck: false
        }
      ]
    },
    {
      roleId: 'admin',
      permissionId: 'user',
      permissionName: '用户管理',
      actionEntitySet: [
        {
          action: 'add',
          describe: '新增',
          defaultCheck: false
        },
        {
          action: 'import',
          describe: '导入',
          defaultCheck: false
        },
        {
          action: 'get',
          describe: '详情',
          defaultCheck: false
        },
        {
          action: 'update',
          describe: '修改',
          defaultCheck: false
        },
        {
          action: 'delete',
          describe: '删除',
          defaultCheck: false
        },
        {
          action: 'export',
          describe: '导出',
          defaultCheck: false
        }
      ]
    },
    // TODO 赋予权限
    {
      roleId: 'admin',
      permissionId: 'reply',
      permissionName: '答题界面',
      actionEntitySet: [
        {
          action: 'add',
          describe: '新增',
          defaultCheck: false
        },
        {
          action: 'query',
          describe: '查询',
          defaultCheck: false
        },
        {
          action: 'get',
          describe: '详情',
          defaultCheck: false
        },
        {
          action: 'update',
          describe: '修改',
          defaultCheck: false
        },
        {
          action: 'delete',
          describe: '删除',
          defaultCheck: false
        }
      ]
    },
    {
      roleId: 'admin',
      permissionId: 'newcomer',
      permissionName: '新人查询',
      actionEntitySet: [
        {
          action: 'add',
          describe: '新增',
          defaultCheck: false
        },
        {
          action: 'query',
          describe: '查询',
          defaultCheck: false
        },
        {
          action: 'get',
          describe: '详情',
          defaultCheck: false
        },
        {
          action: 'update',
          describe: '修改',
          defaultCheck: false
        },
        {
          action: 'delete',
          describe: '删除',
          defaultCheck: false
        }
      ]
    },
    {
      roleId: 'admin',
      permissionId: 'setTopic',
      permissionName: '题目设置',
      actionEntitySet: [
        {
          action: 'add',
          describe: '新增',
          defaultCheck: false
        },
        {
          action: 'query',
          describe: '查询',
          defaultCheck: false
        },
        {
          action: 'get',
          describe: '详情',
          defaultCheck: false
        },
        {
          action: 'update',
          describe: '修改',
          defaultCheck: false
        },
        {
          action: 'delete',
          describe: '删除',
          defaultCheck: false
        }
      ]
    },
    {
      roleId: 'admin',
      permissionId: 'judge',
      permissionName: '判题',
      actionEntitySet: [
        {
          action: 'query',
          describe: '查询',
          defaultCheck: false
        },
        {
          action: 'get',
          describe: '详情',
          defaultCheck: false
        },
        {
          action: 'update',
          describe: '修改',
          defaultCheck: false
        },
        {
          action: 'delete',
          describe: '删除',
          defaultCheck: false
        }
      ]
    }
  ]
  result.permissions = permissions
  
  userInfo.role = roleObj
  return builder(result)
}

/**
 * 使用 用户登录的 token 获取用户有权限的菜单
 * 返回结构必须按照这个结构体形式处理，或根据
 * /src/router/generator-routers.js  文件的菜单结构处理函数对应即可
 * @param {*} options
 * @returns
 */
const userNav = options => {
  // const nav = [
  //   // dashboard
  //   {
  //     name: 'dashboard',
  //     parentId: 0,
  //     id: 1,
  //     meta: {
  //       title: 'menu.dashboard',
  //       icon: 'dashboard',
  //       show: true
  //     },
  //     component: 'RouteView',
  //     redirect: '/dashboard/workplace'
  //   },
  //   // workplace
  //   {
  //     name: 'workplace',
  //     parentId: 1,
  //     id: 7,
  //     meta: {
  //       title: 'menu.dashboard.monitor',
  //       show: true
  //     },
  //     component: 'Workplace'
  //   },
  //   // monitor
  //   {
  //     name: 'monitor',
  //     path: 'https://www.baidu.com/',
  //     parentId: 1,
  //     id: 3,
  //     meta: {
  //       title: 'menu.dashboard.workplace',
  //       target: '_blank',
  //       show: true
  //     }
  //   },
  //   // Analysis
  //   {
  //     name: 'Analysis',
  //     parentId: 1,
  //     id: 2,
  //     meta: {
  //       title: 'menu.dashboard.analysis',
  //       show: true
  //     },
  //     component: 'Analysis',
  //     path: '/dashboard/analysis'
  //   },
  //
  //   // form
  //   {
  //     name: 'form',
  //     parentId: 0,
  //     id: 10,
  //     meta: {
  //       icon: 'form',
  //       title: 'menu.form'
  //     },
  //     redirect: '/form/base-form',
  //     component: 'RouteView'
  //   },
  //   // basic-form
  //   {
  //     name: 'basic-form',
  //     parentId: 10,
  //     id: 6,
  //     meta: {
  //       title: 'menu.form.basic-form'
  //     },
  //     component: 'BasicForm'
  //   },
  //   // step-form
  //   {
  //     name: 'step-form',
  //     parentId: 10,
  //     id: 5,
  //     meta: {
  //       title: 'menu.form.step-form'
  //     },
  //     component: 'StepForm'
  //   },
  //   // advanced-form
  //   {
  //     name: 'advanced-form',
  //     parentId: 10,
  //     id: 4,
  //     meta: {
  //       title: 'menu.form.advanced-form'
  //     },
  //     component: 'AdvanceForm'
  //   },
  //
  //   // list
  //   {
  //     name: 'list',
  //     parentId: 0,
  //     id: 10010,
  //     meta: {
  //       icon: 'table',
  //       title: 'menu.list',
  //       show: true
  //     },
  //     redirect: '/list/table-list',
  //     component: 'RouteView'
  //   },
  //   // table-list
  //   {
  //     name: 'table-list',
  //     parentId: 10010,
  //     id: 10011,
  //     path: '/list/table-list/:pageNo([1-9]\\d*)?',
  //     meta: {
  //       title: 'menu.list.table-list',
  //       show: true
  //     },
  //     component: 'TableList'
  //   },
  //   // basic-list
  //   {
  //     name: 'basic-list',
  //     parentId: 10010,
  //     id: 10012,
  //     meta: {
  //       title: 'menu.list.basic-list',
  //       show: true
  //     },
  //     component: 'StandardList'
  //   },
  //   // card
  //   {
  //     name: 'card',
  //     parentId: 10010,
  //     id: 10013,
  //     meta: {
  //       title: 'menu.list.card-list',
  //       show: true
  //     },
  //     component: 'CardList'
  //   },
  //   // search
  //   {
  //     name: 'search',
  //     parentId: 10010,
  //     id: 10014,
  //     meta: {
  //       title: 'menu.list.search-list',
  //       show: true
  //     },
  //     redirect: '/list/search/article',
  //     component: 'SearchLayout'
  //   },
  //   // article
  //   {
  //     name: 'article',
  //     parentId: 10014,
  //     id: 10015,
  //     meta: {
  //       title: 'menu.list.search-list.articles',
  //       show: true
  //     },
  //     component: 'SearchArticles'
  //   },
  //   // project
  //   {
  //     name: 'project',
  //     parentId: 10014,
  //     id: 10016,
  //     meta: {
  //       title: 'menu.list.search-list.projects',
  //       show: true
  //     },
  //     component: 'SearchProjects'
  //   },
  //   // application
  //   {
  //     name: 'application',
  //     parentId: 10014,
  //     id: 10017,
  //     meta: {
  //       title: 'menu.list.search-list.applications',
  //       show: true
  //     },
  //     component: 'SearchApplications'
  //   },
  //
  //   // profile
  //   {
  //     name: 'profile',
  //     parentId: 0,
  //     id: 10018,
  //     meta: {
  //       title: 'menu.profile',
  //       icon: 'profile',
  //       show: true
  //     },
  //     redirect: '/profile/basic',
  //     component: 'RouteView'
  //   },
  //   {
  //     name: 'basic',
  //     parentId: 10018,
  //     id: 10019,
  //     meta: {
  //       title: 'menu.profile.basic',
  //       show: true
  //     },
  //     component: 'ProfileBasic'
  //   },
  //   {
  //     name: 'advanced',
  //     parentId: 10018,
  //     id: 10020,
  //     meta: {
  //       title: 'menu.profile.advanced',
  //       show: true
  //     },
  //     component: 'ProfileAdvanced'
  //   },
  //
  //   // result
  //   {
  //     name: 'result',
  //     parentId: 0,
  //     id: 10021,
  //     meta: {
  //       title: 'menu.result',
  //       icon: 'check-circle-o',
  //       show: true
  //     },
  //     redirect: '/result/success',
  //     component: 'PageView'
  //   },
  //   {
  //     name: 'success',
  //     parentId: 10021,
  //     id: 10022,
  //     meta: {
  //       title: 'menu.result.success',
  //       hiddenHeaderContent: true,
  //       show: true
  //     },
  //     component: 'ResultSuccess'
  //   },
  //   {
  //     name: 'fail',
  //     parentId: 10021,
  //     id: 10023,
  //     meta: {
  //       title: 'menu.result.fail',
  //       hiddenHeaderContent: true,
  //       show: true
  //     },
  //     component: 'ResultFail'
  //   },
  //
  //   // Exception
  //   {
  //     name: 'exception',
  //     parentId: 0,
  //     id: 10024,
  //     meta: {
  //       title: 'menu.exception',
  //       icon: 'warning',
  //       show: true
  //     },
  //     redirect: '/exception/403',
  //     component: 'RouteView'
  //   },
  //   {
  //     name: '403',
  //     parentId: 10024,
  //     id: 10025,
  //     meta: {
  //       title: 'menu.exception.not-permission',
  //       show: true
  //     },
  //     component: 'Exception403'
  //   },
  //   {
  //     name: '404',
  //     parentId: 10024,
  //     id: 10026,
  //     meta: {
  //       title: 'menu.exception.not-find',
  //       show: true
  //     },
  //     component: 'Exception404'
  //   },
  //   {
  //     name: '500',
  //     parentId: 10024,
  //     id: 10027,
  //     meta: {
  //       title: 'menu.exception.server-error',
  //       show: true
  //     },
  //     component: 'Exception500'
  //   },
  //   // TODO reply答题
  //   {
  //     name: 'reply',
  //     parentId: 0,
  //     id: 56567,
  //     meta: {
  //       title: '题目',
  //       show: true
  //     },
  //     redirect: '/reply/question',
  //     component: 'RouteView'
  //   },
  //   {
  //     name: 'question',
  //     parentId: 56567,
  //     id: 56568,
  //     meta: {
  //       title: '答题页面',
  //       show: true
  //     },
  //     component: 'QuestionPage',
  //   },
  //   // TODO 新人查询页
  //   {
  //     name: 'newcomer',
  //     parentId: 0,
  //     id: 56564,
  //     meta: {
  //       title: '新人',
  //       show: true
  //     },
  //     redirect: '/newcomer/list',
  //     component: 'RouteView'
  //   },
  //   {
  //     name: 'question',
  //     parentId: 56564,
  //     id: 5656855,
  //     meta: {
  //       title: '新人查询',
  //       show: true
  //     },
  //     component: 'NewcomerPage',
  //   },
  //   // TODO 设置题目页
  //   {
  //     name: 'setTopic',
  //     parentId: 0,
  //     id: 56564,
  //     meta: {
  //       title: '新人',
  //       show: true
  //     },
  //     redirect: '/topic/setTopic',
  //     component: 'SetTopic'
  //   },
  //   // TODO 判题页
  //   {
  //     name: 'judge',
  //     parentId: 0,
  //     id: 8080,
  //     meta: {
  //       title: '编程题',
  //       show: true
  //     },
  //     component: 'QuestionJudge'
  //   },
  //   {
  //     name: 'submission',
  //     parentId: 8080,
  //     id: 7777,
  //     meta: {
  //       title: '提交情况',
  //       show: true
  //     },
  //     component: 'SubmissionList',
  //   },
  //   {
  //     name: 'detail',
  //     parentId: 8080,
  //     id: 7776,
  //     meta: {
  //       title: '判题细节',
  //       show: true
  //     },
  //     component: 'JudgeDetail',
  //   },
  //   // TODO 用户中心页
  //   {
  //     name: 'account',
  //     parentId: 0,
  //     id: 10028,
  //     meta: {
  //       title: '个人页',
  //       icon: 'user',
  //       show: true
  //     },
  //     redirect: '/account/center',
  //     component: 'RouteView'
  //   },
  //   {
  //     name: 'center',
  //     parentId: 10028,
  //     id: 10029,
  //     meta: {
  //       title: '个人中心',
  //       show: true
  //     },
  //     component: 'AccountCenter'
  //   },
  //   // EnrollResult
  //   // 特殊三级菜单
  //   {
  //     name: 'settings',
  //     parentId: 10028,
  //     id: 10030,
  //     meta: {
  //       title: '个人设置',
  //       hideHeader: true,
  //       hideChildren: true,
  //       show: true
  //     },
  //     redirect: '/account/settings/basic',
  //     component: 'AccountSettings'
  //   },
  //   {
  //     name: 'BasicSettings',
  //     path: '/account/settings/basic',
  //     parentId: 10030,
  //     id: 10031,
  //     meta: {
  //       title: '基本设置',
  //       show: false
  //     },
  //     component: 'BasicSetting'
  //   },
  //   {
  //     name: 'SecuritySettings',
  //     path: '/account/settings/security',
  //     parentId: 10030,
  //     id: 10032,
  //     meta: {
  //       title: '安全设置',
  //       show: false
  //     },
  //     component: 'SecuritySettings'
  //   },
  //   {
  //     name: 'CustomSettings',
  //     path: '/account/settings/custom',
  //     parentId: 10030,
  //     id: 10033,
  //     meta: {
  //       title: '个性化',
  //       show: false
  //     },
  //     component: 'CustomSettings'
  //   },
  //   {
  //     name: 'BindingSettings',
  //     path: '/account/settings/binding',
  //     parentId: 10030,
  //     id: 10034,
  //     meta: {
  //       title: '账号绑定',
  //       show: false
  //     },
  //     component: 'BindingSettings'
  //   },
  //   {
  //     name: 'NotificationSettings',
  //     path: '/account/settings/notification',
  //     parentId: 10030,
  //     id: 10034,
  //     meta: {
  //       title: '新消息通知',
  //       show: false
  //     },
  //     component: 'NotificationSettings'
  //   },
  // ]
  const nav = []
  const json = builder(nav)
  console.log('json', json)
  return json
}

Mock.mock(/\/api\/user\/info/, 'get', info)
Mock.mock(/\/api\/user\/nav/, 'get', userNav)
