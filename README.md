# 计算机协会招新系统

| 架构                        | 地址                                          |
| --------------------------- | --------------------------------------------- |
| 单体架构(已停止维护)            | https://gitee.com/tiantian17/jx-welcome       |
| 微服务架构  | https://gitee.com/tiantian17/jx-welcome-cloud |

## 系统介绍

### 概述

> 计算机协会招新系统是一款专为计算机协会设计的在线招新平台。它可以帮助协会管理者高效地管理新成员的申请和审核过程，同时为协会成员提供一个方便、易用的申请渠道。

### 系统特点

1. 简单易上手：傻瓜式操作并配备详细文档。
2. 在线上传题目：协会管理者可以通过系统直接提交题目。
3. 实时审核：协会管理者可以实时查看新人的资料，题目提交情况，提高了招新效率。
4. 通知提醒：系统可以通过邮件通知新人录取结果，避免遗漏和延误。
5. 在线通知：协会管理者可以通过界面直接向答题用户推送消息。
6. 响应式布局：支持电脑、平板、手机等所有主流设备，方便用户随时随地进行监控和查看录取结果。
7. 数据导出：支持一键导出成员名单、新人名单、录取名单等常用excel数据。
8. 日志查询：完善的日志记录体系简单注解即可实现

### 内置功能

- :page_with_curl: 系统通知: 登录用户实时接收通知
- :email: 系统邮件: 管理员用户对系统所有新人用户发送邮件
- :clipboard: 系统日志: 系统正常操作日志记录和查询；系统异常信息日志记录和查询
- :chart_with_upwards_trend: 新人查看: 可对系统所有新人进行查看和修改部分信息
- :mag_right: 题目管理: 管理员用户可以对题目进行CRUD
- :globe_with_meridians: 在线图床: 对临时图片的上传
- :rocket: AI判题: 接入qianxiao大模型自动判题
- :bride_with_veil: 在线判题: 通过界面直接对题目进行评分
- :partly_sunny: 在线答题: 新人用户在线进行答题
- :alarm_clock: 定时任务: 在线执行任务调度
- :wrench: 个人设置: 对基本个人信息进行设置
- :e-mail: 录取结果查看: 新人用户可实时查看录取进程和录取结果
- :vhs: 在线监控提交情况: 监视当期系统答题成员注册人数和提交情况

## 技术选型

### 后端


|    技术     |           描述           |
| :---------: | :----------------------: |
| SpringBoot2 |     后端服务开发框架     |
|   MyBatis   |     数据库交互与管理     |
|    MySQL    |   关系型数据库存储数据   |
|    Druid    |       数据库连接池       |
|    Redis    |  数据缓存和业务逻辑实现  |
|  Sa-token   |         权限管理         |
| PageHelper  |         分页实现         |
|  Easyexcel  |        excel操作         |
| Havax-mail  |         发送邮件         |
|  WebSocket  |          长连接          |
|   Hutool    |        Java工具包        |
| Common-lang |        Java工具包        |
|    MinIO    |         文件存储         |
|   Xxl-job   |         任务调度         |
|   Lombok    | 自动插入编辑器并构建工具 |
|  Antisamy   |        防xss攻击         |
|  Ip2region  |         离线定位         |

### 前端

|         技术          |         描述          |
| :-------------------: | :-------------------: |
|         Vue2          |   前端服务开发框架    |
|         VueX          |       状态管理        |
|       VueRouter       |         路由          |
|         Axios         |       异步通信        |
|    Ant Design Vue     |        组件库         |
| Ant Design pro of Vue |  中后台管理系统模板   |
|     mavon-editor      |  构建markdown编辑器   |
|       nprogress       |        进度条         |
|      codemirror       |      代码编辑器       |
|      storage.js       |      数据持久化       |
|      vue-croppe       |       图片裁剪        |
|      file-saver       |       文件下载        |
|        moment         |       日期转化        |
|       viser-vue       | 基于AntV-G2可视化引擎 |
|    vue-clipboard2     |       一键复制        |



## 工作人员使用流程

### 注册

> 正确填写表单内容

![](./images/01Reg.png)

注册成功结果页面

![02RgSuccess.png](./images/02RgSuccess.png)





### 题目设置



![](./images/03SetTopic.png)



设置成功页面



![](./images/04SetTopicSuccess.png)



修改题目

> 若需要对题目内容进行修改/删除题目可按照下图进行操作

![](./images/05ChangMyTopic.png)





### 系统设置

> 角色: admin  (当对某项设置不清楚时鼠标悬停后会有提示弹出)



> 设置 答题开放时间,二轮招新时间地址,注册开关

![07SysSetting](./images/07SysSetting.png)

> ! 判断题人设置 设置成功后该用户会被分配题目进行评分

![08SysSetting2](./images/08SysSetting2.png)

> ! 分数线设置 当评分全部评分完成后,系统会计算总分,当总分超过最低分数线时则进行二轮面试

![09ScoreLine](./images/09ScoreLine.png)





> 系统日志监控
>
> 可查询操作类型包括 新增,修改,删除,登录,注册,录取,未录取,设置题目,提交题目,判断题目,判题人设置,删除判题人,分配新人,登出,批量注册

![10SysLog](./images/10SysLog.png)





### 新人查询与数据导出

![11Newcomer](./images/11Newcomer.png)





### !题目评分

![14CheckRole](./images/14CheckRole.png)



评分基本信息

![12JudgeInfo](./images/12JudgeInfo.png)



进行评分

![15MeJudge](./images/15MeJudge.png)



评分完毕后即可提交

![16JudgeSubmit](./images/16JudgeSubmit.png)

![17ShowJudgeRes](./images/17ShowJudgeRes.png)



查看他人评分



![13CheckOther](./images/13CheckOther.png)

### 系统通知

> 当答题时出现任何问题均可使用系统通知服务
>
> 如上次招新有错题 只是单纯发到微信 而我不看

![18Notice](./images/18Notice.png)



> 邮件服务

![31yxl](./README.assets/31yxl.png)

![32yx](./README.assets/32yx.png)



### 提交情况监控

![34tjqk](./README.assets/34tjqk.png)



### 系统日志

![33xtrz](./README.assets/33xtrz.png)



### 新人查询

![35xr](./README.assets/35xr.png)

### 个人设置

![36gr](./README.assets/36gr.png)

### 新人批量注册



>  批量注册接口说明: https://apifox.com/apidoc/shared-2f831638-4798-4adf-9cba-2cfaf9b011d1/api-123238749
>
>  访问密码: 98TRbUqR



Python代码为例

假设有一份人员名单(excel)

| 学号         | 姓名   | 班级          | 手机号码    | 邮箱               |
| ------------ | ------ | ------------- | ----------- | ------------------ |
| 199810006666 | 啦啦啦 | 98软件工程1班 | 13888888888 | 13888888888@qq.com |
| ...          | ...    | ...           | ...         | ...                |

```python
import json
import pandas as pd
import requests
from pypinyin import lazy_pinyin, Style


def get_initial(name):
    """
    :param name: 字符串
    :return: 返回首字母缩写(小写)
    """
    initial = ''.join(lazy_pinyin(name, style=Style.FIRST_LETTER))
    return initial.lower()


# 邀请码
invitationCode = ""
# 请求地址
url = "http://10.226.8.14:9091/jx-welcome/registerBatch"
# params参数
params = {
    "assessKey": "",
    "secretKey": ""
}
# 请求头
headers = {
    'User-Agent': 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/39.0.2171.71 Safari/537.36',
    'Content-Type': 'application/json'
}
# excel数据
data = pd.read_excel("./人员名单.xlsx")
# 错误消息列表
error_list = []
success_list = []

for index, row in data.iterrows():
    nickName = row["姓名"]
    username = row["学号"]
    # 自定义账号密码策略(学号后6位+姓名首字母小写)
    password = str(username)[-6:] + get_initial(nickName)
    confirmPassword = password
    payload_dict = {
        "invitationCode": invitationCode,
        "username": username,
        "password": password,
        "confirmPassword": password,
        "clasz": row["班级"],
        "nickName": nickName,
        "studentId": row["学号"],
        "phonenumber": row["手机号码"],
        "email": row["邮箱"]
    }
    payload = json.dumps(payload_dict)
    # 发送请求
    response = requests.request("POST", url=url, headers=headers, params=params, data=payload)
    json_response = response.json()
    info = {"请求参数": payload_dict, "返回的消息": json_response['msg']}
    if json_response['code'] != 200:
        error_list.append(info)
    else:
        success_list.append(info)

# 记录信息
with open(file="注册错误信息记录.txt", mode="w") as f:
    if len(error_list) == 0:
        f.write("无错误信息")
    else:
        for error in error_list:
            f.write(str(error))
            f.write("\n")

with open(file="注册成功人员名单信息.txt", mode="w") as f:
    for success in success_list:
        f.write(str(success))
        f.write("\n")

```



# 新人使用流程

## 登录

![20NewcomerLogin.png](./images/20NewcomerLogin.png)

## 题目列表

![21Reply.png](./images/21Reply.png)

## 答题

![22Use.png](./images/22Use.png)

## 提交答案

提交成功界面
![23RepSuccess.png](./images/23RepSuccess.png)

## 录取成功页

> 录取流程如下

1. 提交题解
   ![24Rnr1.png](images/24Rnr1.png)

2. 进行评分
   ![28Rnr5.png](./images/28Rnr5.png)

3. 二轮面试
   ![29Rnr6.png](./images/29Rnr6.png)

4. 成功录取
   ![27Rnr4.png](./images/27Rnr4.png)


## 录取失败页

1. 评分未通过
   ![25Rnr2.png](./images/25Rnr2.png)

2. 二轮面试未通过
   ![26Rnr3.png](images/26Rnr3.png)



## 系统详细文档

> https://gitee.com/tiantian17/jx-welcome-docs
> 内网地址: http://10.226.8.14:666/