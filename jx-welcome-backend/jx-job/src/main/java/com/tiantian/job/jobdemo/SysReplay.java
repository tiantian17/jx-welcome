package com.tiantian.job.jobdemo;

import com.tiantian.common.core.domain.BaseEntity;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotBlank;
import java.io.Serializable;

/**
 * (SysReplay)实体类
 *
 * @author tiantian
 */

@EqualsAndHashCode(callSuper = true)
@Data
@AllArgsConstructor
@NoArgsConstructor
public class SysReplay extends BaseEntity implements Serializable {
    private static final long serialVersionUID = 897434702834958953L;
    /**
     * 提交ID(主键)
     */
    private String replyId;

    /**
     * 题目ID (用于查询题目标题和题目详情)
     */
    @NotBlank(message = "topicId不能为空")
    private String topicId;

    /**
     * 提交用户的ID
     */
    private Long userId;

    /**
     * 分数(默认-1: 未进行提交)
     */
    private Integer score = -1;

    /**
     * 判断人的的ID (userId)
     */
    private Long judger;

    /**
     * 评分状态(0:未进行评分 1:评分完毕) 默认为0
     */
    private String status = "0";

    /**
     * 用户提交的代码
     */
    @NotBlank(message = "答案不能为空")
    private String answer;

    /**
     * 备注
     */
    private String remark;

}

