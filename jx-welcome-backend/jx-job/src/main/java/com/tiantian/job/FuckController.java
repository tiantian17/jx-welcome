package com.tiantian.job;

import cn.dev33.satoken.annotation.SaCheckPermission;
import com.tiantian.common.annotation.Log;
import com.tiantian.common.core.domain.ResponseResult;
import com.tiantian.common.enums.BusinessType;
import com.tiantian.common.exception.BusinessException;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * 当xxl-job挂了/出现不可控Bug 之后可以来手动调用这个接口
 *
 * @author TianTian
 */
@RequiredArgsConstructor
@RequestMapping("/job-bombed-bug")
@RestController
public class FuckController {

    private final SysJobService sysJobService;

    @Value("${xxlJobBombedOrBug.assessKey}")
    private String assessKey;

    @Value("${xxlJobBombedOrBug.secretKey}")
    private String secretKey;

    /**
     * 分配用户
     */
    @GetMapping("/fuckAllot")
    @SaCheckPermission("setting")
    @Log(title = "执行分配新人任务", businessType = BusinessType.ALLOT_NEWCOMER, isSaveRequestData = false)
    public ResponseResult<Void> fuckAllot(String assessKey, String secretKey) {
        // 密钥检测
        if (this.assessKey.equals(assessKey.trim()) && this.secretKey.equals(secretKey.trim())) {
            sysJobService.allotNewcomer();
        } else {
            throw new BusinessException("assessKey或secretKey有误, 最大重试次数不能超过5次");
        }
        return ResponseResult.ok();
    }

    /**
     * 清理文件
     */
    @GetMapping("/fuckFileClear")
    public ResponseResult<Void> fuckFileClear(String assessKey, String secretKey) {
        // 密钥检测
        if (this.assessKey.equals(assessKey.trim()) && this.secretKey.equals(secretKey.trim())) {
            sysJobService.clearExpiredFile();
        } else {
            throw new BusinessException("assessKey或secretKey有误, 最大重试次数不能超过5次");
        }
        return ResponseResult.ok();
    }

    /**
     * 计算零分用户
     */
    @GetMapping("/calcTotalScore")
    @Log(title = "执行分配新人任务", businessType = BusinessType.CALC_SCORE, isSaveRequestData = false)
    public ResponseResult<Void> calcTotalScore(String assessKey, String secretKey) {
        // 密钥检测
        if (this.assessKey.equals(assessKey.trim()) && this.secretKey.equals(secretKey.trim())) {
            sysJobService.calcTotalScore();
        } else {
            throw new BusinessException("assessKey或secretKey有误, 最大重试次数不能超过5次");
        }
        return ResponseResult.ok();
    }

    /**
     * 删除工作人员答题记录
     */
    @GetMapping("/deleteAdminReplys")
    @Log(title = "删除做题记录", businessType = BusinessType.DEL_ADMIN_REPLYS, isSaveRequestData = false)
    public ResponseResult<Void> deleteAdminReplys(String assessKey, String secretKey) {
        // 密钥检测
        if (this.assessKey.equals(assessKey.trim()) && this.secretKey.equals(secretKey.trim())) {
            sysJobService.deleteAdminReplys();
        } else {
            throw new BusinessException("assessKey或secretKey有误, 最大重试次数不能超过5次");
        }
        return ResponseResult.ok();
    }

    /**
     * 设置录取状态
     */
    @GetMapping("/setStatus")
    @Log(title = "设置录取状态", businessType = BusinessType.SET_ENR_STATUS, isSaveRequestData = false)
    public ResponseResult<Void> setStatus(String assessKey, String secretKey) {
        // 密钥检测
        if (this.assessKey.equals(assessKey.trim()) && this.secretKey.equals(secretKey.trim())) {
            sysJobService.setStatus();
        } else {
            throw new BusinessException("assessKey或secretKey有误, 最大重试次数不能超过5次");
        }
        return ResponseResult.ok();
    }
}
