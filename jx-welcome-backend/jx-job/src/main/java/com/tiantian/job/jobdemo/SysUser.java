package com.tiantian.job.jobdemo;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.tiantian.common.constant.UserConstants;
import com.tiantian.common.core.domain.BaseEntity;
import com.tiantian.common.validator.annotation.Xss;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;
import java.io.Serializable;
import java.util.Date;

/**
 * 用户信息表(SysUser)实体类
 *
 * @author tiantian
 */

@EqualsAndHashCode(callSuper = true)
@Data
@AllArgsConstructor
public class SysUser extends BaseEntity implements Serializable {

    private static final long serialVersionUID = 342942976475190488L;

    /**
     * 用户ID
     */
    private Long userId;

    /**
     * 用户账号
     */
    @Xss(message = "用户账号不能包含脚本字符")
    @NotBlank(message = "用户账号不能为空")
    @Size(max = 30, message = "用户账号长度不能超过30个字符")
    private String userName;

    /**
     * 学号
     */
    @NotBlank(message = "学号不能为空")
    @Size(max = 15, message = "学号长度不符合规则")
    private String studentId;

    /**
     * 用户昵称
     */
    @Xss(message = "用户账号不能包含脚本字符")
    @NotBlank(message = "用户昵称不能为空")
    @Size(max = 30, message = "用户昵称长度不能超过30个字符")
    private String nickName;

    /**
     * 用户类型（00协会用户, 01新人用户）
     */
    private String userType;

    /**
     * 部门(0新人 0技术部 1统筹部)
     */
    private String deptType;

    /**
     * 用户邮箱
     */
    @Email(message = "邮箱格式不正确")
    @Size(max = 50, message = "邮箱长度不能超过50个字符")
    private String email;

    /**
     * 所在班级
     */
    private String clasz;

    /**
     * 手机号码
     */
    private String phonenumber;

    /**
     * 用户性别（0男 1女 2未知）
     */
    private String sex;

    /**
     * 头像地址
     */
    private String avatar;

    /**
     * 密码
     */
    private String password;

    @JsonIgnore
    @JsonProperty
    public String getPassword() {
        return password;
    }

    /**
     * 录取状态(0待录取 1未录取  2已录取 3协会内成员)
     */
    private String enrollStatus;

    /**
     * 帐号状态（0正常 1停用）
     */
    private String status;

    /**
     * 删除标志（0代表存在 2代表删除）
     */
    private String delFlag;

    /**
     * 最后登录IP
     */
    private String loginIp;

    /**
     * 最后登录时间
     */
    private Date loginDate;

    /**
     * 备注
     */
    private String remark;

    public SysUser(Long userId) {
        this.userId = userId;
    }

    public SysUser() {

    }

    public boolean isAdmin() {
        return UserConstants.ADMIN_ID.equals(this.userId);
    }

}

