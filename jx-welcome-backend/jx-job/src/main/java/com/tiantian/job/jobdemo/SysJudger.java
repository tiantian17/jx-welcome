package com.tiantian.job.jobdemo;

import com.tiantian.common.core.domain.BaseEntity;
import com.tiantian.common.validator.group.AddGroup;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import javax.validation.constraints.Null;
import java.io.Serializable;
import java.util.List;

/**
 * (SysJudger)实体类
 *
 * @author tiantian
 */

@EqualsAndHashCode(callSuper = true)
@Data
@AllArgsConstructor
@NoArgsConstructor
public class SysJudger extends BaseEntity implements Serializable {
    private static final long serialVersionUID = -93789646270713752L;

    /**
     * 主键
     */
    @Null(message = "参数错误", groups = AddGroup.class)
    private String judgerId;

    /**
     * 判题人的ID (user_id)
     */
    private Long userId;

    /**
     * 判踢人的姓名
     */
    @Null(message = "参数错误", groups = AddGroup.class)
    private String judgerName;

    /**
     * 判题状态( 0:未完成 1:进行中 2全部:已完成 3:未知状态)
     */
    @Null(message = "参数错误", groups = AddGroup.class)
    private String status;

    /**
     * 判断的答题
     */
    @Null(message = "参数错误", groups = AddGroup.class)
    private List<String> replyIds;

    /**
     * 被判题的用户ID
     */
    @Null(message = "参数错误", groups = AddGroup.class)
    private List<Long> replyUsers;

    /**
     * 删除标记 ("0":未删除 "1":已删除)
     */
    @Null(message = "参数错误", groups = AddGroup.class)
    private String delFlag;

    public SysJudger(Long userId, List<Long> replyUsers){
        this.userId = userId;
        this.replyUsers = replyUsers;
    }

}

