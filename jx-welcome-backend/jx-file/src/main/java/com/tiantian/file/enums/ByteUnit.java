package com.tiantian.file.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * @author TianTian
 */
@Getter
@AllArgsConstructor
public enum ByteUnit {
    B("B"),
    K("K"),
    M("M"),
    G("G");

    private final String ByteUnit;
}
