package com.tiantian.file.config;

import com.tiantian.file.config.properties.MinioProp;
import io.minio.MinioClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * minio 核心配置类
 */
@Configuration
@EnableConfigurationProperties(MinioProp.class)
public class MinioConfig {

    @Autowired
    private MinioProp minioProp;

    /**
     * 获取 MinioClient
     */
    @Bean
    public MinioClient minioClient() {
        // 使用MinIO服务器、其访问密钥和密钥创建minioClient
        return MinioClient.builder()
                // MinIO服务器访问网址端口
                .endpoint(minioProp.getEndpoint())
                // MinIO服务器登录账号和密码
                .credentials(minioProp.getAccessKey(), minioProp.getSecretKey())
                .build();
    }
}

