package com.tiantian.file.service;

import com.tiantian.file.enums.ByteUnit;
import io.minio.messages.Item;
import org.springframework.web.multipart.MultipartFile;

import java.io.InputStream;
import java.util.List;

/**
 * 文件管理业务类
 *
 * @author TianTian
 */
public interface JxFileService {
    /**
     * 查询bucket中文件列表信息
     *
     * @param bucketName 桶名称
     * @return Iterable<Result < Item>>
     */
    List<Item> listObjects(String bucketName, String prefix);

    /**
     * 文件上传服务
     *
     * @param file       文件
     * @param bucketName 桶名称
     * @return 文件地址
     */
    String uploadObject(MultipartFile file, String extension, String bucketName);

    /**
     * 大文件上传服务
     *
     * @param file       文件
     * @param bucketName 桶名称
     * @return 文件地址
     */
    String uploadFile(MultipartFile file, String bucketName);

    /**
     * 文件下载服务
     *
     * @param bucketName 桶名称
     * @param objectName 文件名称
     * @return
     */
    InputStream downloadFile(String bucketName, String objectName);

    /**
     * 删除服务
     *
     * @param bucketName 桶名称
     * @param objectName 要删除的桶下根目录的对象/子目录下的对象 格式: 文件(path/file.png)   文件夹(path/to/)
     * @return 是否删除成功
     */
    boolean removeObject(String bucketName, String objectName);

    /**
     * 批量删除服务
     *
     * @param bucketName  桶名称
     * @param objectNames 要删除的桶下根目录的对象/子目录下的对象 格式: 文件(path/file.png)   文件夹(path/to/)
     */
    void removeObjects(String bucketName, String[] objectNames);

    /**
     * 判断桶是否存在
     *
     * @param bucketName 桶名称
     * @return 是否存在
     */
    boolean isFoundBucket(String bucketName);

    /**
     * 判断桶是否存在 不存在则创建
     *
     * @param bucketName bucketName
     */
    void bucketExistsAndCreate(String bucketName);

    /**
     * 检查文件大小是否符合规范
     *
     * @param len
     * @param size
     * @param unit
     */
    void checkFileSize(Long len, int size, ByteUnit byteUnit);


}
