package com.tiantian.web.controller.system;

import cn.dev33.satoken.annotation.SaCheckPermission;
import cn.dev33.satoken.annotation.SaIgnore;
import cn.hutool.core.date.LocalDateTimeUtil;
import cn.hutool.core.util.ObjUtil;
import com.tiantian.common.annotation.Log;
import com.tiantian.common.constant.SystemConfigConstants;
import com.tiantian.common.core.domain.ResponseResult;
import com.tiantian.common.core.domain.dto.AnswerTimeDAO;
import com.tiantian.common.enums.BusinessType;
import com.tiantian.common.exception.BusinessException;
import com.tiantian.common.utils.DateUtils;
import com.tiantian.common.utils.redis.RedisUtils;
import com.tiantian.system.domain.SecondRoundInfo;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import java.time.Duration;
import java.time.LocalDateTime;
import java.util.HashMap;
import java.util.Map;


/**
 * 系统配置
 *
 * @author TianTian
 */
@RequiredArgsConstructor
@RestController
@RequestMapping("/system/config")
@SaCheckPermission("setting")
public class SystemConfigController {


    /**
     * 设置答题时间
     *
     * @param answerTimeDAO 请求参数
     */
    @PostMapping("/setAnswerTime")
    @Log(title = "设置答题时间", businessType = BusinessType.ANS_TIME)
    public ResponseResult<Map<String, Object>> setAnswerTime(@RequestBody @Valid AnswerTimeDAO answerTimeDAO) {
        // 解析参数
        checkParameter(answerTimeDAO.getDate(), answerTimeDAO.getStart(), answerTimeDAO.getEnd());

        Map<String, Object> ajax = new HashMap<>();
        // 保存到Redis中
        RedisUtils.set(SystemConfigConstants.SYSTEM_CONFIG_ANSWER_TIME_KEY, answerTimeDAO);
        ajax.put("isSetting", true);
        return ResponseResult.ok(ajax);
    }

    /**
     * 获取答题时间
     */
    @GetMapping("/getAnswerTime")
    public ResponseResult<AnswerTimeDAO> getAnswerTime() {
        AnswerTimeDAO answerTimeDAOForRedis = RedisUtils.get(SystemConfigConstants.SYSTEM_CONFIG_ANSWER_TIME_KEY, AnswerTimeDAO.class);
        if (ObjUtil.isNotEmpty(answerTimeDAOForRedis)) {
            return ResponseResult.ok(answerTimeDAOForRedis);
        }
        return ResponseResult.fail("未设置答题时间");
    }

    /**
     * 更新注册开关
     */
    @PutMapping("/setRegStatus/{flag}")
    @Log(title = "设置注册开关", businessType = BusinessType.REGISTER_SETTING)
    public ResponseResult<Void> setRegStatus(@NotNull(message = "参数不能为空") @PathVariable Boolean flag) {
        RedisUtils.set(SystemConfigConstants.SYSTEM_ENABLE_REGISTER_KEY, flag);
        return ResponseResult.ok();
    }

    /**
     * 获取注册开关的状态
     */
    @GetMapping("/getRegStatus")
    public ResponseResult<Boolean> getRegisterStatus() {
        Boolean o = (Boolean) RedisUtils.get(SystemConfigConstants.SYSTEM_ENABLE_REGISTER_KEY);
        if (ObjUtil.isNull(o)) {
            // 默认不允许注册
            RedisUtils.set(SystemConfigConstants.SYSTEM_ENABLE_REGISTER_KEY, false);
        }
        return ResponseResult.ok(o);
    }

    /**
     * 设置二轮招新时间地点
     */
    @PostMapping("/setSecondRoundInfo")
    public ResponseResult<Map<String, Object>> setSecondRoundInfo(@RequestBody @Valid SecondRoundInfo secondRoundInfo) {
        checkParameter(secondRoundInfo.getDate(), secondRoundInfo.getStart(), secondRoundInfo.getEnd());
        Map<String, Object> ajax = new HashMap<>();
        // 保存到Redis中
        RedisUtils.set(SystemConfigConstants.SYSTEM_CONFIG_SECOND_ROUND_INFO_KEY, secondRoundInfo);
        ajax.put("isSetting", true);
        return ResponseResult.ok(ajax);
    }

    /**
     * 获取二轮招新时间地点
     */
    @GetMapping("/getSecondRoundInfo")
    public ResponseResult<SecondRoundInfo> getSecondRoundInfo() {
        SecondRoundInfo secondRoundInfoForRedis = RedisUtils.get(SystemConfigConstants.SYSTEM_CONFIG_SECOND_ROUND_INFO_KEY, SecondRoundInfo.class);
        if (ObjUtil.isNotEmpty(secondRoundInfoForRedis)) {
            return ResponseResult.ok(secondRoundInfoForRedis);
        }
        return ResponseResult.fail("未设置二轮面试时间和地点");
    }

    @PostMapping("/setScoreLine")
    @Log(title = "设置最低分数线", businessType = BusinessType.SCORE_LINE)
    @SaIgnore
    public ResponseResult<Void> setScoreLine(@NotNull(message = "最低分数线不能为空")
                                             @Max(message = "参数有错误", value = 100L)
                                             @Min(message = "参数有误", value = 5L)
                                             Integer scoreLine) {
        // 保存到Redis中
        HashMap<String, Integer> scoreLineMap = new HashMap<>();
        scoreLineMap.put("scoreLine", scoreLine);
        RedisUtils.set(SystemConfigConstants.SYSTEM_CONFIG_INTERVIEW_SCORE_LINE, scoreLineMap);
        return ResponseResult.ok();
    }

    @GetMapping("/getScoreLine")
    @SuppressWarnings("all")
    public ResponseResult<Map<String, Integer>> getScoreLine() {
        Map<String, Integer> scoreLineMap = RedisUtils.get(SystemConfigConstants.SYSTEM_CONFIG_INTERVIEW_SCORE_LINE, HashMap.class);
        return ResponseResult.ok(scoreLineMap);
    }


    /**
     * 参数校验
     *
     * @param dateStr  日期      yyyy-MM-dd
     * @param startStr 开始时间  HH:mm:ss
     * @param endStr   结束时间  HH:mm:ss
     */
    private void checkParameter(String dateStr, String startStr, String endStr) {
        LocalDateTime date;
        LocalDateTime start;
        LocalDateTime end;
        // 解析参数
        try {
            date = LocalDateTimeUtil.parse(dateStr, DateUtils.YYYY_MM_DD);
            start = LocalDateTimeUtil.parse(startStr, DateUtils.HH_MM_SS);
            end = LocalDateTimeUtil.parse(endStr, DateUtils.HH_MM_SS);
        } catch (Exception e) {
            throw new BusinessException("时间格式错误");
        }

        // 判断时间是否符合规则
        Duration between = LocalDateTimeUtil.between(start, end);
        if (between.toMinutes() < 0) {
            throw new BusinessException("结束时间不能小于开始时间");
        }

        // 获取当前时间
        LocalDateTime localDateTimeNow = LocalDateTimeUtil.parse(
                LocalDateTimeUtil.format(LocalDateTime.now(), DateUtils.YYYY_MM_DD),
                DateUtils.YYYY_MM_DD);

        Duration betweenNow = LocalDateTimeUtil.between(localDateTimeNow, date);
        if (betweenNow.toMinutes() < 0) {
            throw new BusinessException("设置的答题日期不能小于当天");
        }
    }
}
