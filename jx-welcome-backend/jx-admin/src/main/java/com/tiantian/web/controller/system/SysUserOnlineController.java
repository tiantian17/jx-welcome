package com.tiantian.web.controller.system;

import com.tiantian.common.core.controller.BaseController;
import com.tiantian.common.core.domain.ResponseResult;
import com.tiantian.framework.handler.NoticeWebSocketHandler;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.HashMap;
import java.util.Map;

/**
 * 在线用户监控
 */
@RestController
@RequestMapping("/monitor/online")
public class SysUserOnlineController extends BaseController {


    /**
     * 获取在线人数和提交人数[部分提交+完全提交]
     */
    @GetMapping
    @Deprecated
    public ResponseResult<Map<String, Object>> getOlineNewcomerCount(){
        int onlineCount = NoticeWebSocketHandler.onlineNum.get();
        Map<String, Object> ajax = new HashMap<>();
        ajax.put("onlineCount", onlineCount);
        ajax.put("subCount", 100);
        return ResponseResult.ok(ajax);
    }
}
