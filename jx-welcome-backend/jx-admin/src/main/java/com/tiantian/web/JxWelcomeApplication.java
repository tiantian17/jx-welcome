package com.tiantian.web;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.web.servlet.ServletComponentScan;

/**
 * 启动类
 *
 * @author TianTian
 */
// @ServletComponentScan(basePackages = "com.tiantian.common.filter") // 开启Xss过滤器
@SpringBootApplication(scanBasePackages = {
        "com.tiantian.*.service",
        "com.tiantian.*.controller",
        "com.tiantian.framework.*",
        "com.tiantian.system.*",
        "com.tiantian.file.*",
        "com.tiantian.job.**"
})
@MapperScan(basePackages = "com.tiantian.**.mapper")
public class JxWelcomeApplication {
    public static void main(String[] args) {
        SpringApplication.run(JxWelcomeApplication.class, args);
    }
}
