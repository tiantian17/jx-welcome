package com.tiantian.web.controller.newcomer;

import cn.hutool.core.util.ObjUtil;
import com.tiantian.common.constant.EnrollResultConstants;
import com.tiantian.common.constant.SystemConfigConstants;
import com.tiantian.common.core.controller.BaseController;
import com.tiantian.common.core.domain.ResponseResult;
import com.tiantian.common.core.domain.entity.SysUser;
import com.tiantian.common.utils.StringUtils;
import com.tiantian.common.utils.redis.RedisUtils;
import com.tiantian.system.domain.SecondRoundInfo;
import com.tiantian.system.mapper.SysUserMapper;
import com.tiantian.topic.mapper.SysReplayMapper;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.HashMap;
import java.util.Map;

/**
 * 构造录取步骤的信息
 *
 * @author TianTian
 */
@RequestMapping("/system/enrollResult")
@RestController
@RequiredArgsConstructor
public class EnrollResultController extends BaseController {
    /* 前端所需数据格式
     *       data: {
     *         index: 0,        //   0:提交解题  1:进行评分  2:二轮面试  3:录取成功
     *         enrollStatus: 0, //  "0":进行中  "1":录取未通过  "2":录取成功
     *         subTime: '2023-10-26 16:58:00',
     *         nickName: '浩浩',
     *         judger: '谢天浩',
     *         SecondRoundInfo: {
     *           date: '2023-10-27',
     *           start: '9:00:00',
     *           end: '17:30:00',
     *           location: 'S1-706'
     *         },
     *         judgeResult: '1',     //  评分结果  "0"进行中 "1"未通过 "2"评分通过
     *         interviewResult: '1'  //  面试结果 "0"进行中 "1"未通过 "2"面试通过
     *       }
     */

    private final SysReplayMapper sysReplayMapper;
    private final SysUserMapper sysUserMapper;

    @GetMapping
    public ResponseResult<Map<String, Object>> getEnrollInfo() {
        Map<String, Object> ajax = new HashMap<>();
        // 设置Index[0 1 2 3] [提交解题 进行评分 二轮面试 录取成功]
        int index = EnrollResultConstants.ENROLL_RESULT_REPLY_INDEX; // 默认0
        String judgeResult;
        String interviewResult;
        Long userId = getUserId();
        SysUser sysUser = sysUserMapper.selectUserById(getUserId());

        // ==========获取最后一次的提交时间==========
        String lastReplyTime = sysReplayMapper.selectLastReplyTime(getUserId());

        // 是否进行提交过的凭据
        boolean isReply = StringUtils.isNotEmpty(lastReplyTime);

        // ==========设置二轮面试的信息==========
        SecondRoundInfo secondRoundInfo = RedisUtils.get(SystemConfigConstants.SYSTEM_CONFIG_SECOND_ROUND_INFO_KEY, SecondRoundInfo.class);

        // ==========!设置评分是否通过==========
        boolean noPassFlag = false;
        Map<String, Integer> scoreLineMap = RedisUtils.get(SystemConfigConstants.SYSTEM_CONFIG_INTERVIEW_SCORE_LINE, HashMap.class);
        Integer totalScore = sysUser.getTotalScore();
        if (ObjUtil.isNotNull(scoreLineMap) && isReply) {
            // 判断分数是否符合最低录取分数线
            if (totalScore >= scoreLineMap.get("scoreLine")) {
                // 达到则分数符合 judgeResult=2
                judgeResult = EnrollResultConstants.SCORE_THROUGH;
            } else {
                judgeResult = EnrollResultConstants.SCORE_NOT_THROUGH;
            }
            index = EnrollResultConstants.ENROLL_RESULT_JUDGE_INDEX;
        } else {
            // 未提交
            judgeResult = EnrollResultConstants.SCORE_IN_PROGRESS;
        }

        // ==========!设置二轮面试结果==========
        String enrollStatus = sysUser.getEnrollStatus();
        if (EnrollResultConstants.ENROLL_PASS.equals(enrollStatus)) {
            // 若录取状态为2时则证明 二轮通过
            index = EnrollResultConstants.ENROLL_RESULT_INDEX;
            interviewResult = EnrollResultConstants.INTERVIEW_IN_PROGRESS;
        } else if (EnrollResultConstants.ENROLL_NOT_PASS.equals(enrollStatus)) {
            interviewResult = EnrollResultConstants.INTERVIEW_NOT_PASS;
        } else {
            interviewResult = EnrollResultConstants.INTERVIEW_IN_PROGRESS;
        }

        // ==========设置评分负责人==========
        String judgerName = sysReplayMapper.selectJudgerByUserId(userId);

        // 构造数据
        ajax.put("index", index);
        ajax.put("SecondRoundInfo", secondRoundInfo);
        ajax.put("subTime", isReply ? lastReplyTime : "未进行提交");
        ajax.put("nickName", isReply ? getNickName() : "未进行提交");
        ajax.put("judger", StringUtils.isEmpty(judgerName) ? "未进行评分" : judgerName);
        ajax.put("enrollStatus", enrollStatus);
        ajax.put("judgeResult", judgeResult);
        ajax.put("interviewResult", interviewResult);
        return ResponseResult.ok(ajax);
    }
}
