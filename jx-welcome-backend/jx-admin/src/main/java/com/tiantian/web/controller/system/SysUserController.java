package com.tiantian.web.controller.system;

import com.tiantian.common.core.controller.BaseController;
import lombok.RequiredArgsConstructor;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.List;

/**
 * 用户信息
 *
 * @author TianTian
 */
@Validated
@RequiredArgsConstructor
@RestController
@RequestMapping("/system/user")
public class SysUserController extends BaseController {

    /**
     * 已废弃
     * 获取用户导航(没必要增加系统的复杂度)
     *
     * @return 空集合
     */
    @Deprecated
    @GetMapping("/nav")
    public List<String> getUserNav() {
        return new ArrayList<>();
    }


}
