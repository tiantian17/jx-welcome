package com.tiantian.web.controller.system;

import cn.dev33.satoken.annotation.SaIgnore;
import cn.hutool.core.util.BooleanUtil;
import com.tiantian.common.annotation.Log;
import com.tiantian.common.annotation.RateLimiter;
import com.tiantian.common.annotation.RepeatSubmit;
import com.tiantian.common.constant.CacheConstants;
import com.tiantian.common.constant.SystemConfigConstants;
import com.tiantian.common.core.controller.BaseController;
import com.tiantian.common.core.domain.ResponseResult;
import com.tiantian.common.core.domain.model.RegisterBody;
import com.tiantian.common.enums.BusinessType;
import com.tiantian.common.enums.LimitType;
import com.tiantian.common.enums.OperatorType;
import com.tiantian.common.utils.StringUtils;
import com.tiantian.common.utils.redis.RedisUtils;
import com.tiantian.system.service.SysRegisterService;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.constraints.NotBlank;
import java.util.List;


/**
 * 注册验证
 */
@Validated
@RequiredArgsConstructor
@RestController
public class SysRegisterController extends BaseController {

    private final SysRegisterService registerService;


    @Value("${jx-batch.assessKey}")
    private String assessKey;

    @Value("${jx-batch.secretKey}")
    private String secretKey;

    /**
     * 用户注册
     */
    @SaIgnore
    @PostMapping("/register")
    @RateLimiter(limitType = LimitType.IP, time = 50, count = 3, message = "注册频繁, 请稍后重试")
    @Log(title = "用户注册", businessType = BusinessType.REGISTER, operatorType = OperatorType.NEWCOMER)
    public ResponseResult<Void> register(@Validated @RequestBody RegisterBody user) {
        // 校验是否开发注册功能
        Boolean o = (Boolean) RedisUtils.get(SystemConfigConstants.SYSTEM_ENABLE_REGISTER_KEY);
        if (BooleanUtil.isFalse(o)) {
            return ResponseResult.fail("系统注册功能未开放");
        }
        // 从Redis中获取验证码
        String key = CacheConstants.CAPTCHA_CODE_KEY + user.getEmail();
        String code = RedisUtils.get(key, String.class);
        if (StringUtils.isBlank(code)) {
            return ResponseResult.fail("注册失败, 验证码超时");
        }
        if (!StringUtils.equals(code, user.getCaptcha())) {
            return ResponseResult.fail("验证码错误");
        }
        registerService.register(user);
        return ResponseResult.ok();
    }

    /**
     * 批量注册
     */
    @SaIgnore
    @PostMapping("/registerBatch")
    @Log(title = "批量注册", businessType = BusinessType.REGISTER_BATCH, operatorType = OperatorType.ADMIN)
    public ResponseResult<List<Object>> registerBatch(@Validated @RequestBody RegisterBody user,
                                                      @NotBlank(message = "assessKey不能为空") String assessKey,
                                                      @NotBlank(message = "密钥不能为空") String secretKey) {
        // 密钥检测
        if (this.assessKey.equals(assessKey) && this.secretKey.equals(secretKey)) {
            registerService.register(user);
        } else {
            return ResponseResult.fail("assessKey或secretKey有误");
        }
        return ResponseResult.ok();
    }
}
