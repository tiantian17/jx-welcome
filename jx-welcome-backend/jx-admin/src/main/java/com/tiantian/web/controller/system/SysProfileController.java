package com.tiantian.web.controller.system;

import cn.dev33.satoken.secure.BCrypt;
import cn.hutool.core.bean.BeanUtil;
import com.tiantian.common.annotation.Log;
import com.tiantian.common.constant.CacheConstants;
import com.tiantian.common.core.controller.BaseController;
import com.tiantian.common.core.domain.ResponseResult;
import com.tiantian.common.core.domain.entity.SysUser;
import com.tiantian.common.enums.BusinessType;
import com.tiantian.common.helper.LoginHelper;
import com.tiantian.common.utils.StringUtils;
import com.tiantian.common.utils.redis.RedisUtils;
import com.tiantian.system.domain.dto.ChangPwdDTO;
import com.tiantian.system.domain.dto.UserProfileDTO;
import com.tiantian.system.service.ISysUserService;
import lombok.RequiredArgsConstructor;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;


/**
 * 个人信息 业务处理
 */
@Validated
@RequiredArgsConstructor
@RestController
@RequestMapping("/system/user/profile")
public class SysProfileController extends BaseController {

    private final ISysUserService userService;

    /**
     * 获取用户个人信息
     */
    @GetMapping
    public ResponseResult<SysUser> profile() {
        SysUser user = userService.selectUserById(getUserId());
        return ResponseResult.ok(user);
    }

    /**
     * 修改用户信息
     */
    @PutMapping
    @Log(title = "更新用户信息", businessType = BusinessType.UPDATE_PROFILE)
    public ResponseResult<Void> updateProfile(@RequestBody @Valid UserProfileDTO userProfileDTO) {
        SysUser user = BeanUtil.copyProperties(userProfileDTO, SysUser.class);
        Long userId = getUserId();

        SysUser sysUser = userService.selectUserById(userId);
        if (!sysUser.getPhonenumber().equals(userProfileDTO.getPhonenumber()) && !userService.checkPhoneUnique(user)) {
            return ResponseResult.fail("修改手机号码[" + user.getPhonenumber() + "]失败，该手机号码已存在");
        }

        user.setUserId(userId);
        if (userService.updateUserProfile(user) > 0) {
            return ResponseResult.ok();
        }
        return ResponseResult.fail("修改个人信息异常,请联系管理员");
    }

    @PutMapping("/updateEmail")
    @Log(title = "重置邮箱", businessType = BusinessType.UPDATE_PROFILE)
    public ResponseResult<Void> updateEmail(@NotBlank(message = "邮箱不能为空") String email,
                                            @Email(message = "新邮箱格式不正确") String newEmail,
                                            @NotBlank(message = "验证码不能为空") String captcha) {
        String key = CacheConstants.UPDATE_EMAIL_CAPTCHA_CODE_KEY + email;
        String captchaForRedis = RedisUtils.get(key, String.class);

        if (StringUtils.isEmpty(captchaForRedis) || !captcha.equals(captchaForRedis)) {
            return ResponseResult.fail("验证码不正确");
        }

        SysUser user = new SysUser();
        user.setUserId(getUserId());
        user.setEmail(newEmail);
        if (!userService.checkEmailUnique(user)) {
            return ResponseResult.fail("修改用户[" + user.getEmail() + "]失败，邮箱账号已存在");
        }
        if (userService.updateUserProfile(user) > 0) {
            return ResponseResult.ok();
        }
        return ResponseResult.fail("修改邮箱,请联系管理员");
    }

    /**
     * 重置密码
     */
    @PostMapping("/updatePwd")
    @Log(title = "更新密码", businessType = BusinessType.UPDATE_PROFILE)
    public ResponseResult<Void> updatePwd(@RequestBody @Valid ChangPwdDTO changPwdDTO) {
        String newPassword = changPwdDTO.getNewPassword();
        String oldPassword = changPwdDTO.getOldPassword();
        String confirmPassword = changPwdDTO.getConfirmPassword();

        if (!newPassword.equals(confirmPassword)) {
            return ResponseResult.fail("两次密码输入不一致");
        }
        SysUser user = userService.selectUserById(LoginHelper.getUserId());
        String password = user.getPassword();
        if (!BCrypt.checkpw(oldPassword, password)) {
            return ResponseResult.fail("修改密码失败,旧密码错误");
        }
        if (BCrypt.checkpw(newPassword, password)) {
            return ResponseResult.fail("新密码不能与旧密码相同");
        }

        if (userService.resetUserPwd(getUserId(), BCrypt.hashpw(newPassword)) > 0) {
            return ResponseResult.ok();
        }
        return ResponseResult.fail("修改密码异常,请联系管理员");
    }


}
