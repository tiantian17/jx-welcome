package com.tiantian.web.controller.newcomer;

import cn.hutool.core.bean.BeanUtil;
import com.alibaba.excel.EasyExcel;
import com.tiantian.common.annotation.Log;
import com.tiantian.common.constant.EnrollResultConstants;
import com.tiantian.common.constant.SystemConfigConstants;
import com.tiantian.common.core.domain.entity.SysUser;
import com.tiantian.common.enums.BusinessType;
import com.tiantian.common.enums.OperatorType;
import com.tiantian.common.utils.redis.RedisUtils;
import com.tiantian.system.domain.vo.JudgeResultForExcel;
import com.tiantian.system.domain.vo.NewcomerForExcel;
import com.tiantian.system.service.ISysUserService;
import com.tiantian.topic.domain.vo.SysJudgeVO;
import com.tiantian.topic.service.SysJudgeService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * 数据导出通用接口
 *
 * @author TianTian
 */
@RestController
@RequestMapping("/system/export")
@RequiredArgsConstructor
public class ExportController {

    private final ISysUserService sysUserService;
    private final SysJudgeService sysJudgeService;


    @GetMapping("/{type}")
    @Log(title = "文件导出", businessType = BusinessType.FILE_EXPORT, isSaveResponseData = false, isSaveRequestData = false, operatorType = OperatorType.ADMIN)
    public void exportNewcomerExcel(HttpServletResponse response, @PathVariable String type) throws IOException {
        SysUser sysUser = new SysUser();
        List<List<String>> head = new ArrayList<>();
        List<String> title = new ArrayList<>();
        if ("newcomer".equals(type)) {
            title.add("新人信息");
            sysUser = null;
        } else if ("scoreThrough".equals(type)) {
            title.add("评分通过名单");
            HashMap<String, Integer> scoreLineMap = RedisUtils.get(SystemConfigConstants.SYSTEM_CONFIG_INTERVIEW_SCORE_LINE, HashMap.class);
            Integer scoreLine = scoreLineMap.get("scoreLine");
            sysUser.setTotalScore(scoreLine);
        } else if ("enrollThrough".equals(type)) {
            title.add("录取通过名单");
            sysUser.setEnrollStatus(EnrollResultConstants.ENROLL_PASS);
        } else {
            return;
        }
        head.add(title);

        // 查询数据
        List<SysUser> sysUsers = sysUserService.selectPageUserList(sysUser);
        // 属性拷贝
        List<NewcomerForExcel> newcomerForExcels = BeanUtil.copyToList(sysUsers, NewcomerForExcel.class);
        for (NewcomerForExcel newcomerForExcel : newcomerForExcels) {
            if ("0".equals(newcomerForExcel.getSex())) {
                newcomerForExcel.setSex("女");
            } else if ("1".equals(newcomerForExcel.getSex())) {
                newcomerForExcel.setSex("男");
            } else {
                newcomerForExcel.setSex("未知");
            }
        }
        response.setContentType("application/vnd.ms-excel;chartset=utf-8");
        response.setCharacterEncoding("utf-8");
        String fileName = URLEncoder.encode(System.currentTimeMillis() + "导出", "utf-8");
        response.setHeader("Content-disposition", "attachment;filename=" + fileName + ".xlsx");
        EasyExcel.write(response.getOutputStream(), NewcomerForExcel.class)
                .head(head)
                .head(NewcomerForExcel.class)
                .autoCloseStream(true)
                .sheet("基本信息")
                .doWrite(newcomerForExcels);
    }

    // 导出判题结果
    @GetMapping("/result/judge")
    @Log(title = "导出判题结果", businessType = BusinessType.FILE_EXPORT, isSaveResponseData = false, isSaveRequestData = false, operatorType = OperatorType.ADMIN)
    public void exportJudgeResultExcel(HttpServletResponse response) throws IOException {
        List<SysJudgeVO> sysJudgeVOList = sysJudgeService.selectJudgeList(null);
        // 属性拷贝
        List<JudgeResultForExcel> judgeResultForExcels = BeanUtil.copyToList(sysJudgeVOList, JudgeResultForExcel.class);
        response.setContentType("application/vnd.ms-excel;chartset=utf-8");
        response.setCharacterEncoding("utf-8");
        String fileName = URLEncoder.encode(System.currentTimeMillis() + "导出", "utf-8");
        response.setHeader("Content-disposition", "attachment;filename=" + fileName + ".xlsx");

        EasyExcel.write(response.getOutputStream(), NewcomerForExcel.class)
                .head(JudgeResultForExcel.class)
                .autoCloseStream(true)
                .sheet("基本信息")
                .doWrite(judgeResultForExcels);
    }


}
