package com.tiantian.web.controller.system;

import cn.dev33.satoken.annotation.SaCheckPermission;
import com.tiantian.common.annotation.Log;
import com.tiantian.common.core.controller.BaseController;
import com.tiantian.common.core.domain.ResponseResult;
import com.tiantian.common.core.domain.SysOperLog;
import com.tiantian.common.core.page.TableDataInfo;
import com.tiantian.common.enums.BusinessType;
import com.tiantian.system.service.ISysOperLogService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * 操作日志记录
 */
@RequiredArgsConstructor
@RestController
@RequestMapping("/log/operLog")
public class SysOperLogController extends BaseController {

    private final ISysOperLogService operLogService;

    /**
     * 获取操作日志记录列表
     */
    @GetMapping("/list")
    @SaCheckPermission("log")
    public TableDataInfo list(SysOperLog operLog) {
        startPage();
        List<SysOperLog> sysOperLogs = operLogService.selectOperLogList(operLog);
        return getDataTable(sysOperLogs);
    }

    /**
     * 根据id查询日志数据
     *
     * @param operId 日志id
     */
    @GetMapping("/{operId}")
    @SaCheckPermission("log")
    public ResponseResult<SysOperLog> getOperLogById(@PathVariable Long operId) {
        SysOperLog sysOperLog = operLogService.selectOperLogById(operId);
        return ResponseResult.ok(sysOperLog);
    }

    /**
     * 批量删除操作日志记录
     *
     * @param operIds 日志ids
     */
    @DeleteMapping("/{operIds}")
    @SaCheckPermission("log")
    @Log(title = "删除操作日志", businessType = BusinessType.DEL_LOG)
    public ResponseResult<Void> remove(@PathVariable Long[] operIds) {
        return toAjax(operLogService.deleteOperLogByIds(operIds));
    }

    /**
     * 清理操作日志记录
     */
    @DeleteMapping("/clean")
    @SaCheckPermission("log")
    @Log(title = "清空操作日志", businessType = BusinessType.CLEAR_LOG)
    public ResponseResult<Void> clean() {
        operLogService.cleanOperLog();
        return ResponseResult.ok();
    }
}
