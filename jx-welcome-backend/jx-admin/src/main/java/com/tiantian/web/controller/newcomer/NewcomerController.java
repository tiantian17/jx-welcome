package com.tiantian.web.controller.newcomer;

import cn.dev33.satoken.annotation.SaCheckPermission;
import cn.hutool.core.util.ObjectUtil;
import com.tiantian.common.annotation.Log;
import com.tiantian.common.constant.UserConstants;
import com.tiantian.common.core.controller.BaseController;
import com.tiantian.common.core.domain.ResponseResult;
import com.tiantian.common.core.domain.dto.NewcomerDTO;
import com.tiantian.common.core.domain.entity.SysUser;
import com.tiantian.common.core.page.TableDataInfo;
import com.tiantian.common.enums.BusinessType;
import com.tiantian.common.enums.OperatorType;
import com.tiantian.common.exception.BusinessException;
import com.tiantian.common.utils.StringUtils;
import com.tiantian.system.service.ISysUserService;
import com.tiantian.topic.mapper.SysReplayMapper;
import com.tiantian.topic.service.SysJudgeService;
import com.tiantian.topic.service.SysReplyService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import javax.validation.constraints.NotNull;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.List;

/**
 * 新人
 *
 * @author TianTian
 */
@RequiredArgsConstructor
@RestController
@RequestMapping("/newcomer")
public class NewcomerController extends BaseController {

    private final ISysUserService sysUserService;
    private final SysReplayMapper sysReplayMapper;

    /**
     * 获取用户列表
     *
     * @param user 查询参数
     * @return 对象列表
     */
    @GetMapping("/list")
    @SaCheckPermission("newcomer")
    public TableDataInfo selectByPage(SysUser user) {
        startPage();
        List<SysUser> list = sysUserService.selectPageUserList(user);
        // 设置提交时间
        for (SysUser sysUser : list) {
            String lastReplyTime = sysReplayMapper.selectLastReplyTime(sysUser.getUserId());
            if (StringUtils.isNotEmpty(lastReplyTime)) {
                sysUser.setUpdateTime(LocalDateTime.parse(lastReplyTime, DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss")));
            }
        }
        return getDataTable(list);
    }

    @GetMapping()
    @SaCheckPermission("newcomer")
    public ResponseResult<SysUser> selectNewcomerByStudentId(@NotNull(message = "学生ID不能为空") Long userId) {
        SysUser newcomer = sysUserService.selectNewcomerByUserId(userId);
        return ResponseResult.ok(newcomer);
    }

    @PutMapping("/update")
    @SaCheckPermission("newcomer")
    @Log(title = "更新新人信息", businessType = BusinessType.UPDATE_NEWCOMER, isSaveResponseData = false, operatorType = OperatorType.ADMIN)
    public ResponseResult<Void> updateNewcomer(@RequestBody NewcomerDTO newcomerDTO) {
        newcomerDTO.setUpdateBy(getNickName());
        newcomerDTO.setUpdateTime(LocalDateTime.now());
        int row = sysUserService.updateNewcomer(newcomerDTO);
        return toAjax(row);
    }

    /**
     * 一键录取
     */
    @PutMapping("/onekeyEnroll/{userId}")
    @SaCheckPermission("newcomer")
    @Log(title = "一键录取", businessType = BusinessType.ENROLL, isSaveResponseData = false, operatorType = OperatorType.ADMIN)
    public ResponseResult<Void> onekeyEnroll(@PathVariable @NotNull(message = "用户ID不能为空") Long userId) {
        SysUser sysUser = sysUserService.selectUserById(userId);
        if (ObjectUtil.isEmpty(sysUser)) {
            throw new BusinessException("用户不存在, 录取失败");
        }
        NewcomerDTO newcomerDTO = new NewcomerDTO();
        newcomerDTO.setUserId(userId);
        newcomerDTO.setEnrollStatus(UserConstants.ENROLL_STATUS_ADMITTED);
        newcomerDTO.setUpdateBy(getNickName());
        newcomerDTO.setUpdateTime(LocalDateTime.now());
        int row = sysUserService.updateNewcomer(newcomerDTO);
        return toAjax(row);
    }

}
