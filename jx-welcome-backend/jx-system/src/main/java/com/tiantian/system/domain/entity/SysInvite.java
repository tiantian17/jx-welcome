package com.tiantian.system.domain.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

/**
 * (SysInvite)实体类
 *
 * @author tiantian
 */

@Data
@AllArgsConstructor
@NoArgsConstructor
public class SysInvite implements Serializable {
    private static final long serialVersionUID = -96143710140122860L;
    /**
     * 主键
     */
    private Integer id;

    /**
     * 邀请码
     */
    private String inviteCode;

    /**
     * 用户ID 创建人
     */
    private Long userId;

    /**
     * 角色Id
     */
    private Long roleId;

    /**
     * 用户类型 "00"协会内用户 "01"新人用户
     */
    private String userType;

    /**
     * 解释
     */
    private String remark;

}

