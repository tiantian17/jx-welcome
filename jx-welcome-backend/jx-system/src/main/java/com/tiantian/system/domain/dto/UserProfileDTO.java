package com.tiantian.system.domain.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotBlank;

/**
 * @author TianTian
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class UserProfileDTO {

    /**
     * 姓名
     */
    @NotBlank(message = "姓名不能为空")
    private String nickName;

    /**
     * 班级
     */
    @NotBlank(message = "班级不能为空")
    private String clasz;

    /**
     * 手机号码
     */
    @NotBlank(message = "手机号码不能为空")
    private String phonenumber;

    /**
     * 个人简介
     */
    private String remark;

    /**
     * 性别
     */
    @NotBlank(message = "性别不能为空")
    private String sex;
}
