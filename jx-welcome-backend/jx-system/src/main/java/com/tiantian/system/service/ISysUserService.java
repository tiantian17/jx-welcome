package com.tiantian.system.service;


import com.tiantian.common.core.domain.dto.NewcomerDTO;
import com.tiantian.common.core.domain.entity.SysUser;

import java.util.List;


/**
 * 用户 业务层
 */
public interface ISysUserService {


    /**
     * 通过用户ID查询用户
     *
     * @param userId 用户ID
     * @return 用户对象信息
     */
    SysUser selectUserById(Long userId);

    /**
     * 根据用户ID查询新人
     * @param userId 用户ID
     * @return
     */
    SysUser selectNewcomerByUserId(Long userId);

    /**
     * 通过用户id查询新人
     *
     * @param userId 用户ID
     */
    SysUser selectNewcomerById(Long userId);

    /**
     * 检查用户名是否唯一
     *
     * @param username 用户名
     * @return 结果
     */
    boolean checkUserNameUnique(String username);

    /**
     * 修改用户头像
     *
     * @param userId    用户id
     * @param avatarURL 头像地址
     * @return 结果
     */
    int updateUserAvatar(Long userId, String avatarURL);

    /**
     * 校验手机号码是否唯一
     *
     * @param user 用户信息
     * @return 结果
     */
    boolean checkPhoneUnique(SysUser user);

    /**
     * 校验email是否唯一
     *
     * @param user 用户信息
     * @return 结果
     */
    boolean checkEmailUnique(SysUser user);


    /**
     * 注册用户信息
     *
     * @param user 用户信息
     * @return 结果
     */
    boolean registerUser(SysUser user);

    /**
     * 重置用户密码
     *
     * @param user 用户信息
     * @return 结果
     */
    int resetPwd(SysUser user);

    /**
     * 更新新人的信息
     */
    int updateNewcomer(NewcomerDTO newcomerDTO);

    /**
     * 修改用户基本信息
     *
     * @param user 用户信息
     * @return 结果
     */
    int updateUserProfile(SysUser user);

    /**
     * 重置用户密码
     *
     * @param userId   用户id
     * @param password 密码
     * @return 结果
     */
    int resetUserPwd(Long userId, String password);

    /**
     * 查询新人信息
     *
     * @param user      查询参数
     * @return 分页结果
     */
    List<SysUser> selectPageUserList(SysUser user);

    /**
     * 查询候选人
     */
    List<SysUser> selectCandidate();

    int setNewcomerTotalScore(Long userId, int totalScore);

    /**
     * 统计新人数量
     */
    int countNewcomer();
}
