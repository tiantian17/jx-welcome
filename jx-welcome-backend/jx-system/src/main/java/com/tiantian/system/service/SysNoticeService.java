package com.tiantian.system.service;

import com.tiantian.common.core.domain.entity.SysNotice;

import java.util.List;

/**
 * 通知公告表(SysNotice)表服务接口
 *
 * @author tiantian
 * @since 2023-10-17 15:00:45
 */
public interface SysNoticeService {


    /**
     * 分页查询
     *
     * @param sysNotice 实体筛选条件
     * @return 对象列表
     */
    List<SysNotice> selectList(SysNotice sysNotice);

    /**
     * 新增数据
     *
     * @param sysNotice 实例对象
     * @return 新增标记
     */
    void insert(SysNotice sysNotice);

    /**
     * 通过主键删除数据
     *
     * @param noticeId 主键
     * @return 修改标记
     */
    Boolean deleteById(Integer noticeId);
}


