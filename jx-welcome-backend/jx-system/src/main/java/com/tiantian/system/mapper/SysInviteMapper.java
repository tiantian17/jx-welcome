package com.tiantian.system.mapper;

import com.tiantian.system.domain.entity.SysInvite;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

/**
 * (SysInvite)表数据库访问层
 *
 * @author tiantian
 */
@Mapper
public interface SysInviteMapper {

    /**
     * 通过ID查询单条数据
     *
     * @param id 主键
     * @return 实例对象
     */
    SysInvite selectById(Integer id);

    /**
     * 通过ID查询单条数据
     *
     * @param inviteCode 邀请码
     * @return 实例对象
     */
    SysInvite selectByInviteCode(String  inviteCode);

    /**
     * 通过实体作为筛选条件查询
     *
     * @param sysInvite 实例对象
     * @return 对象列表
     */
    List<SysInvite> selectList(SysInvite sysInvite);

    /**
     * 新增数据
     *
     * @param sysInvite 实例对象
     * @return 影响行数
     */
    Boolean insert(SysInvite sysInvite);

    /**
     * 修改数据
     *
     * @param sysInvite 实例对象
     * @return 影响行数
     */
    Boolean updateById(SysInvite sysInvite);

    /**
     * 通过主键删除数据
     *
     * @param id 主键
     * @return 影响行数
     */
    Boolean deleteById(Integer id);
}


