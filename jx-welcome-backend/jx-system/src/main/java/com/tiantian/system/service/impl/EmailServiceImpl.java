package com.tiantian.system.service.impl;

import com.tiantian.common.enums.EmailType;
import com.tiantian.common.utils.email.MailUtils;
import com.tiantian.system.domain.dto.MailRequestDTO;
import com.tiantian.system.service.EmailService;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;

/**
 * @author TianTian
 */
@Service
public class EmailServiceImpl implements EmailService {

    @Value("${mail.brand}")
    private String brand;

    /**
     * 发送单个普通邮件
     *
     * @param email   邮箱号
     * @param subject 标题
     * @param content 内容
     */
    @Async
    public void sendEmail(String email, String subject, String content) {
        MailUtils.sendText(email, subject, brand + content);
    }

    /**
     * 群发系统普通邮件
     *
     * @param mailRequestDTO
     */
    @Async
    public void sendAllEmail(MailRequestDTO mailRequestDTO) {
        MailUtils.send(
                mailRequestDTO.getTos(),
                mailRequestDTO.getSubject(),
                brand + mailRequestDTO.getContent(),
                EmailType.HTML.getType().equals(mailRequestDTO.getType())
        );
    }
}
