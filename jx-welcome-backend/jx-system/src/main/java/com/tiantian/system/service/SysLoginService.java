package com.tiantian.system.service;

import cn.dev33.satoken.exception.NotLoginException;
import cn.dev33.satoken.secure.BCrypt;
import cn.dev33.satoken.stp.StpUtil;
import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.util.ObjectUtil;
import com.tiantian.common.constant.CacheConstants;
import com.tiantian.common.core.domain.dto.RoleDTO;
import com.tiantian.common.core.domain.entity.SysRole;
import com.tiantian.common.core.domain.entity.SysRolePerm;
import com.tiantian.common.core.domain.entity.SysUser;
import com.tiantian.common.core.domain.entity.SysUserAction;
import com.tiantian.common.core.domain.model.LoginUser;
import com.tiantian.common.core.domain.vo.SysRolePermVO;
import com.tiantian.common.exception.BusinessException;
import com.tiantian.common.helper.LoginHelper;
import com.tiantian.common.utils.StringUtils;
import com.tiantian.common.utils.redis.RedisUtils;
import com.tiantian.common.utils.spring.BeanCopyUtil;
import com.tiantian.system.mapper.SysRolePermissionMapper;
import com.tiantian.system.mapper.SysUserActionMapper;
import com.tiantian.system.mapper.SysUserMapper;
import com.tiantian.system.mapper.SysUserRoleMapper;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Set;
import java.util.function.Supplier;

/**
 * 登录校验方法
 */
@RequiredArgsConstructor
@Slf4j
@Service
public class SysLoginService {

    private final SysUserMapper userMapper;
    private final SysRolePermissionMapper sysRolePermissionMapper;
    private final SysUserRoleMapper sysUserRoleMapper;
    private final SysUserActionMapper sysUserActionMapper;

    // 最大重试次数
    @Value("${user.password.maxRetryCount}")
    private Integer maxRetryCount;

    // 账号锁定时间
    @Value("${user.password.lockTime}")
    private Integer lockTime;

    /**
     * 登录验证
     *
     * @param username 用户名
     * @param password 密码
     * @return token
     */
    public String login(String username, String password) {
        // 根据用户名查询用户
        SysUser user = loadUserByUsername(username);
        // 使用checkpw方法检查被加密的字符串是否与原始字符串匹配
        checkLogin(username, () -> !BCrypt.checkpw(password, user.getPassword()));
        LoginUser loginUser = buildLoginUser(user);
        // 生成token
        LoginHelper.login(loginUser);
        // 记录信息
        return StpUtil.getTokenValue();
    }

    /**
     * 退出登录
     */
    public void logout() {
        try {
            LoginUser loginUser = LoginHelper.getLoginUser();
            // 记录用户退出的信息
        } catch (NotLoginException ignored) {
        } finally {
            try {
                // 退出登录
                StpUtil.logout();
            } catch (NotLoginException ignored) {
            }
        }
    }

    /**
     * 根据账号查询用户是否存在
     *
     * @param username 账号
     * @return SysUser
     */
    private SysUser loadUserByUsername(String username) {
        // 查询出用户信息
        SysUser user = userMapper.selectUserByUserName(username);
        if (ObjectUtil.isNull(user)) {
            log.info("登录用户：{} 不存在.", username);
            throw new BusinessException("用户不存在");
        }
        return user;
    }

    /**
     * 构建登录用户
     */
    private LoginUser buildLoginUser(SysUser user) {
        // 属性拷贝
        LoginUser loginUser = BeanUtil.copyProperties(user, LoginUser.class);

        // 设置用户角色
        Long userId = user.getUserId();
        SysRole sysRole = sysUserRoleMapper.selectRoleByUserId(userId);
        RoleDTO roleDTO = BeanCopyUtil.copy(sysRole, RoleDTO.class);
        loginUser.setRoleDTO(roleDTO);

        // 设置角色权限
        Set<SysRolePerm> rolePermission = sysRolePermissionMapper.getRolePermissionByUserId(userId);
        List<SysRolePermVO> sysRolePermVO = BeanUtil.copyToList(rolePermission, SysRolePermVO.class);

        // 设置角色操作权限(默认所有)
        List<SysUserAction> userActions = sysUserActionMapper.getUserActions();
        for (SysRolePermVO rolePermVO : sysRolePermVO) {
            rolePermVO.setActionEntitySet(userActions);
        }
        loginUser.setPermissions(sysRolePermVO);

        return loginUser;
    }

    /**
     * 登录校验<br/>
     * 1 密码错误超过5次账号进行锁定<br/>
     * 2 查询账号是否被锁定<br/>
     */
    private void checkLogin(String username, Supplier<Boolean> supplier) {
        String errorKey = CacheConstants.PWD_ERR_CNT_KEY + username;

        // 获取用户登录错误次数，查询为空时默认为0
        int errorNumber = (int) ObjectUtil.defaultIfNull(RedisUtils.get(errorKey), 0);
        // 锁定时间内登录 则踢出
        if (errorNumber >= maxRetryCount) {
            throw new BusinessException(StringUtils.format("锁定时间内不能登录, 重试次数超过{}次, 账号已被锁定{}秒", maxRetryCount, lockTime));
        }

        // 如果密码与数据库密码不匹配
        if (supplier.get()) {
            // 错误次数递增
            errorNumber++;
            RedisUtils.set(errorKey, errorNumber, lockTime);
            // 达到规定错误次数 则锁定登录
            if (errorNumber >= maxRetryCount) {
                throw new BusinessException(StringUtils.format("重试次数超过{}次, 账号已被锁定{}秒", maxRetryCount, lockTime));
            } else {
                // 未达到规定错误次数
                throw new BusinessException("账号或者密码错误");
            }
        }
        // 登录成功 清空错误次数
        RedisUtils.del(errorKey);
    }
}
