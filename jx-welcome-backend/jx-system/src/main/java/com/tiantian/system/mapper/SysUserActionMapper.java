package com.tiantian.system.mapper;

import com.tiantian.common.core.domain.entity.SysUserAction;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

/**
 * @author TianTian
 */
@Mapper
public interface SysUserActionMapper {

    List<SysUserAction> getUserActions();

}
