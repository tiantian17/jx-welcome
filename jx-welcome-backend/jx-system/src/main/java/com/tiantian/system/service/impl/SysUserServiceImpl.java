package com.tiantian.system.service.impl;

import cn.hutool.core.util.ObjectUtil;
import com.tiantian.common.core.domain.dto.NewcomerDTO;
import com.tiantian.common.core.domain.entity.SysUser;
import com.tiantian.system.mapper.SysUserMapper;
import com.tiantian.system.service.ISysUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @author TianTian
 */
@Service
public class SysUserServiceImpl implements ISysUserService {

    @Autowired
    private SysUserMapper baseMapper;

    /**
     * 通过用户ID查询用户
     *
     * @param userId 用户ID
     * @return 用户对象信息
     */
    @Override
    public SysUser selectUserById(Long userId) {
        return baseMapper.selectUserById(userId);
    }

    @Override
    public SysUser selectNewcomerByUserId(Long userId) {
        return baseMapper.selectNewcomerByUserId(userId);
    }

    /**
     * 通过用户ID查询用户
     *
     * @param userId 用户ID
     * @return 用户对象信息
     */
    @Override
    public SysUser selectNewcomerById(Long userId) {
        return baseMapper.selectNewcomerById(userId);
    }

    /**
     * 校验用户名称是否已经存在
     *
     * @param username 用户名
     * @return 是否唯一
     */
    @Override
    public boolean checkUserNameUnique(String username) {
        return ObjectUtil.isNull(baseMapper.checkUserNameUnique(username));
    }

    /**
     * 修改用户头像
     *
     * @param userId    用户id
     * @param avatarURL 头像地址
     * @return 结果
     */
    @Override
    public int updateUserAvatar(Long userId, String avatarURL) {
        return baseMapper.updateUserAvatar(userId, avatarURL);
    }

    /**
     * 校验手机号码是否唯一
     *
     * @param user 用户信息
     * @return
     */
    @Override
    public boolean checkPhoneUnique(SysUser user) {
        SysUser sysUser = baseMapper.checkPhoneUnique(user.getPhonenumber());
        return ObjectUtil.isNull(sysUser);
    }

    /**
     * 校验email是否唯一
     *
     * @param user 用户信息
     * @return
     */
    @Override
    public boolean checkEmailUnique(SysUser user) {
        SysUser sysUser = baseMapper.checkEmailUnique(user.getEmail());
        return ObjectUtil.isNull(sysUser);
    }

    /**
     * 注册用户信息
     *
     * @param user 用户信息
     * @return 结果
     */
    @Override
    public boolean registerUser(SysUser user) {
        user.setCreateBy(user.getNickName());
        user.setUpdateBy(user.getNickName());
        return baseMapper.insertUser(user) > 0;
    }


    /**
     * 重置用户密码
     *
     * @param user 用户信息
     * @return 结果
     */
    @Override
    public int resetPwd(SysUser user) {
        return baseMapper.updateUser(user);
    }

    /**
     * 更新新人的信息
     */
    @Override
    public int updateNewcomer(NewcomerDTO newcomerDTO) {
        return baseMapper.updateNewcomer(newcomerDTO);
    }

    /**
     * 重置用户密码
     *
     * @param userId   用户id
     * @param password 密码
     * @return 结果
     */
    @Override
    public int resetUserPwd(Long userId, String password) {
        SysUser sysUser = new SysUser();
        sysUser.setPassword(password);
        sysUser.setUserId(userId);
        return baseMapper.updateUser(sysUser);
    }

    /**
     * 查询新人信息
     *
     * @param user      查询参数
     * @return 分页结果
     */
    @Override
    public List<SysUser> selectPageUserList(SysUser user) {
        return baseMapper.selectNewcomerList(user);
    }

    /**
     * 查询候选人
     */
    @Override
    public List<SysUser> selectCandidate() {
        return baseMapper.selectCandidate();
    }

    /**
     * 修改用户基本信息
     *
     * @param user 用户信息
     * @return 结果
     */
    @Override
    public int updateUserProfile(SysUser user) {
        return baseMapper.updateUser(user);
    }

    @Override
    public int setNewcomerTotalScore(Long userId ,int totalScore) {
        System.out.println("设置总分 userId:"+  userId + "总分: " + totalScore);
        return baseMapper.updateNewcomerTotalScore(userId, totalScore);
    }

    @Override
    public int countNewcomer() {
        return baseMapper.countNewcomer();
    }
}
