package com.tiantian.system.mapper;

import com.tiantian.common.core.domain.entity.SysRolePerm;
import com.tiantian.common.core.domain.entity.SysUser;
import org.apache.ibatis.annotations.Mapper;

import java.util.Set;

/**
 * @author TianTian
 */
@Mapper
public interface SysRolePermissionMapper {
    /**
     * 根据用户ID查询用户权限
     * @param userId 用户id
     * @return 权限set集合
     */
    Set<SysRolePerm> getRolePermissionByUserId(Long userId);
}
