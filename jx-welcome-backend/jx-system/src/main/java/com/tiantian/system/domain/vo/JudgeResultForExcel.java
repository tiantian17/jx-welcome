package com.tiantian.system.domain.vo;

import com.alibaba.excel.annotation.ExcelProperty;
import com.alibaba.excel.annotation.write.style.ColumnWidth;
import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;

import java.time.LocalDateTime;

/**
 * @author TianTian
 */
@Data
public class JudgeResultForExcel {

    @ExcelProperty("学号")
    @ColumnWidth(22)
    private String studentId;

    @ExcelProperty("姓名")
    @ColumnWidth(20)
    private String nickName;

    @ExcelProperty("班级")
    @ColumnWidth(20)
    private String clasz;

    @ExcelProperty("判题人")
    @ColumnWidth(20)
    private String judger;

    @ExcelProperty("提交题目数量")
    @ColumnWidth(20)
    private Integer subCount;

    @ExcelProperty("总分")
    @ColumnWidth(20)
    private Integer totalScore;

}
