package com.tiantian.system.mapper;

import com.tiantian.common.core.domain.entity.SysRole;
import org.apache.ibatis.annotations.Mapper;


/**
 * 用户与角色关联表 数据层
 *

 */
@Mapper
public interface SysUserRoleMapper {
    /**
     * 根据userId查询角色
     * @param userId 用户id
     * @return SysRole
     */
    SysRole selectRoleByUserId(Long userId);

    /**
     * 设置用户角色
     * @param userId 用户Id
     * @param roleId 角色Id
     * @return 影响行数
     */
    int insertUser(Long userId, Long roleId);
}
