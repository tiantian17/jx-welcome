package com.tiantian.system.mapper;


import com.tiantian.common.core.domain.SysOperLog;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

/**
 * 操作日志 数据层
 */
@Mapper
public interface SysOperLogMapper {

    /**
     * 插入日志数据
     */
    void insertOperLog(SysOperLog operLog);

    /**
     * 查询日志列表
     */
    List<SysOperLog> selectOperLogList(SysOperLog sysOperLog);

    /**
     * 批量删除系统操作日志
     *
     * @param operIds 需要删除的操作日志ID
     * @return 结果
     */
    int deleteOperLogByIds(Long[] operIds);

    /**
     * 根据operId查询日志数据
     * @param operId 日志主键
     */
    SysOperLog selectOperLogById(Long operId);

    /**
     * 清空操作日志
     */
    void cleanOperLog();
}
