package com.tiantian.system.domain.dto;

import com.tiantian.common.validator.annotation.EmailTypeCheck;
import lombok.Data;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.util.List;

/**
 * 邮件请求
 *
 * @author TianTian
 */
@Data
public class MailRequestDTO {

    /**
     * 接收人 单个或者多个
     */
    @Size(message = "收件人邮箱不能为空")
    private List<String> tos;

    /**
     * 类型 text html
     */
    @EmailTypeCheck(message = "邮件类型不正确")
    private String type;

    /**
     * 邮件主题
     */
    @NotBlank(message = "标题不能为空")
    private String subject;

    /**
     * 邮件内容
     */
    @NotBlank(message = "内容不能为空")
    private String content;

    /**
     * 附件
     */
    private List<String> filePath;
}