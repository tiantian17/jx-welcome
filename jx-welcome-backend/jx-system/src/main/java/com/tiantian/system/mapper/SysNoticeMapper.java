package com.tiantian.system.mapper;

import com.tiantian.common.core.domain.entity.SysNotice;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

/**
 * 通知公告表(SysNotice)表数据库访问层
 *
 * @author tiantian
 * @since 2023-10-17 15:00:43
 */
@Mapper
public interface SysNoticeMapper {

    /**
     * 通过ID查询单条数据
     *
     * @param noticeId 主键
     * @return 实例对象
     */
    SysNotice selectById(Long noticeId);

    /**
     * 通过实体作为筛选条件查询
     *
     * @param sysNotice 实例对象
     * @return 对象列表
     */
    List<SysNotice> selectList(SysNotice sysNotice);

    /**
     * 新增数据
     *
     * @param sysNotice 实例对象
     * @return 影响行数
     */
    Boolean insert(SysNotice sysNotice);


    /**
     * 通过主键删除数据
     *
     * @param noticeId 主键
     * @return 影响行数
     */
    Boolean deleteById(Integer noticeId);
}


