package com.tiantian.system.service.impl;

import com.tiantian.system.domain.entity.SysInvite;
import com.tiantian.system.mapper.SysInviteMapper;
import com.tiantian.system.service.SysInviteService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * (SysInvite表)服务实现类
 *
 * @author tiantian
 * @since 2023-11-03 17:16:26
 */
@Service("sysInviteService")
public class SysInviteServiceImpl implements SysInviteService {
    @Autowired
    private SysInviteMapper sysInviteMapper;

    /**
     * 通过ID查询单条数据
     *
     * @param id 主键
     * @return 实例对象
     */
    @Override
    public SysInvite selectById(Integer id) {
        return sysInviteMapper.selectById(id);
    }

    @Override
    public SysInvite selectByInviteCode(String inviteCode) {
        return sysInviteMapper.selectByInviteCode(inviteCode);
    }

    /**
     * 分页查询
     *
     * @param pageNum  查询起始位置
     * @param pageSize 查询条数
     * @return 对象列表
     */
    @Override
    public List<SysInvite> selectByPage(Integer pageNum, Integer pageSize, SysInvite sysInvite) {
        return sysInviteMapper.selectList(sysInvite);
    }

    /**
     * 根据条件查询
     *
     * @return 实例对象的集合
     */
    @Override
    public List<SysInvite> selectList(SysInvite sysInvite) {
        return sysInviteMapper.selectList(sysInvite);
    }

    /**
     * 新增数据
     *
     * @param sysInvite 实例对象
     * @return 实例对象
     */
    @Override
    public Boolean insert(SysInvite sysInvite) {
        return sysInviteMapper.insert(sysInvite);
    }

    /**
     * 修改数据
     *
     * @param sysInvite 实例对象
     * @return 实例对象
     */
    @Override
    public Boolean updateById(SysInvite sysInvite) {
        return sysInviteMapper.updateById(sysInvite);
    }

    /**
     * 通过主键删除数据
     *
     * @param id 主键
     * @return 是否成功
     */
    @Override
    public Boolean deleteById(Integer id) {
        return sysInviteMapper.deleteById(id);
    }

}


