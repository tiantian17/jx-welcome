package com.tiantian.system.domain.vo;

import com.alibaba.excel.annotation.ExcelProperty;
import com.alibaba.excel.annotation.write.style.ColumnWidth;
import lombok.Data;

/**
 * @author TianTian
 */
@Data
public class NewcomerForExcel {
    @ExcelProperty("姓名")
    @ColumnWidth(20)
    private String nickName;

    @ExcelProperty("学号")
    @ColumnWidth(22)
    private String studentId;

    @ExcelProperty("班级")
    @ColumnWidth(20)
    private String clasz;

    @ExcelProperty("性别")
    @ColumnWidth(10)
    private String sex;

    @ExcelProperty("手机号码")
    @ColumnWidth(20)
    private String phonenumber;

    @ExcelProperty("邮箱")
    @ColumnWidth(20)
    private String email;
}
