package com.tiantian.system.service.impl;

import com.tiantian.common.core.domain.entity.SysNotice;
import com.tiantian.system.mapper.SysNoticeMapper;
import com.tiantian.system.service.SysNoticeService;
import lombok.RequiredArgsConstructor;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * 通知公告表(SysNotice表)服务实现类
 *
 * @author tiantian
 * @since 2023-10-17 15:00:45
 */
@RequiredArgsConstructor
@Service("sysNoticeService")
public class SysNoticeServiceImpl implements SysNoticeService {

    private final SysNoticeMapper sysNoticeMapper;


    /**
     * 分页查询
     *
     * @param sysNotice 实体筛选条件
     */
    @Override
    public List<SysNotice> selectList(SysNotice sysNotice) {
        return sysNoticeMapper.selectList(sysNotice);
    }

    /**
     * 新增数据
     *
     * @param sysNotice 实例对象
     */
    @Override
    @Async
    public void insert(SysNotice sysNotice) {
        sysNoticeMapper.insert(sysNotice);
    }


    /**
     * 通过主键删除数据
     *
     * @param noticeId 主键
     * @return 是否成功
     */
    @Override
    public Boolean deleteById(Integer noticeId) {
        return sysNoticeMapper.deleteById(noticeId);
    }

}


