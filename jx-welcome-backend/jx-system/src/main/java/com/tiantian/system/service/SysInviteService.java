package com.tiantian.system.service;


import com.tiantian.system.domain.entity.SysInvite;

import java.util.List;

/**
 * (SysInvite)表服务接口
 *
 * @author tiantian
 * @since 2023-11-03 17:16:26
 */
public interface SysInviteService {

    /**
     * 通过ID查询单条数据
     *
     * @param id 主键
     * @return 实例对象
     */
    SysInvite selectById(Integer id);

    /**
     * 查询邀请码查询数据
     *
     * @param inviteCode 邀请码
     */
    SysInvite selectByInviteCode(String inviteCode);

    /**
     * 分页查询
     *
     * @param pageNo    查询起始位置
     * @param pageSize  查询条数
     * @param sysInvite 实体筛选条件
     * @return 对象列表
     */
    List<SysInvite> selectByPage(Integer pageNo, Integer pageSize, SysInvite sysInvite);

    /**
     * 通过实体作为筛选条件查询
     *
     * @param sysInvite 实例对象
     * @return 对象列表
     */
    List<SysInvite> selectList(SysInvite sysInvite);

    /**
     * 新增数据
     *
     * @param sysInvite 实例对象
     * @return 新增标记
     */
    Boolean insert(SysInvite sysInvite);

    /**
     * 修改数据
     *
     * @param sysInvite 实例对象
     * @return 删除标记
     */
    Boolean updateById(SysInvite sysInvite);

    /**
     * 通过主键删除数据
     *
     * @param id 主键
     * @return 修改标记
     */
    Boolean deleteById(Integer id);
}


