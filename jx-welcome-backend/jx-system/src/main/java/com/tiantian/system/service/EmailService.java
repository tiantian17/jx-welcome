package com.tiantian.system.service;

import com.tiantian.system.domain.dto.MailRequestDTO;

/**
 * 邮件服务
 *
 * @author TianTian
 */
public interface EmailService {
    /**
     * 发送单个普通邮件
     *
     * @param email   邮箱号
     * @param subject 标题
     * @param content 内容
     */
    void sendEmail(String email, String subject, String content);

    /**
     * 群发系统普通邮件
     *
     * @param mailRequestDTO
     */
    void sendAllEmail(MailRequestDTO mailRequestDTO);


}
