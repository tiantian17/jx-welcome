package com.tiantian.system.domain;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotBlank;

/**
 * @author TianTian
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class SecondRoundInfo {

    /**
     * 日期
     */
    @NotBlank(message = "日期不能为空")
    private String date;

    /**
     * 开始时间
     */
    @NotBlank(message = "开始时间不能为空")
    private String start;

    /**
     * 结束时间
     */
    @NotBlank(message = "结束时间不能为空")
    private String end;

    /**
     * 二轮面试地点
     */
    @NotBlank(message = "地点不能为空")
    private String location;
}
