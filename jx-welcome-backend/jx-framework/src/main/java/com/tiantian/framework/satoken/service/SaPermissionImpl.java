package com.tiantian.framework.satoken.service;

import cn.dev33.satoken.stp.StpInterface;
import cn.hutool.core.collection.CollUtil;
import cn.hutool.core.collection.CollectionUtil;
import cn.hutool.core.util.ObjectUtil;
import com.tiantian.common.core.domain.model.LoginUser;
import com.tiantian.common.core.domain.vo.SysRolePermVO;
import com.tiantian.common.helper.LoginHelper;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

/**
 * sa-token 权限管理实现类
 *
 * @author TianTian
 */
@SuppressWarnings("all")
public class SaPermissionImpl implements StpInterface {

    /**
     * 返回一个账号所拥有的权限码集合
     */
    @Override
    public List<String> getPermissionList(Object loginId, String loginType) {
        LoginUser loginUser = LoginHelper.getLoginUser();

        List<SysRolePermVO> permissions = ObjectUtil.defaultIfNull(loginUser.getPermissions(), new ArrayList<>());
        return CollectionUtil.newArrayList(permissions).stream()
                .map(SysRolePermVO::getPermission)
                .collect(Collectors.toList());
    }

    /**
     * 返回一个账号所拥有的角色标识集合 (权限与角色可分开校验)
     */
    @Override
    public List<String> getRoleList(Object loginId, String loginType) {
        LoginUser loginUser = LoginHelper.getLoginUser();
        String roleKey = loginUser.getRoleDTO().getRoleKey();
        return CollUtil.newArrayList(roleKey);
    }
}
