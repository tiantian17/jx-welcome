package com.tiantian.framework.config;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.PropertyAccessor;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.jsontype.impl.LaissezFaireSubTypeValidator;
import org.springframework.boot.SpringBootConfiguration;
import org.springframework.context.annotation.Bean;
import org.springframework.data.redis.connection.RedisConnectionFactory;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.core.script.DefaultRedisScript;
import org.springframework.data.redis.serializer.*;

/**
 * Redis序列化配置
 *
 * @author TianTian
 */
@SpringBootConfiguration
public class RedisConfig {

    @Bean
    public RedisTemplate<String, Object> redisTemplate(RedisConnectionFactory redisConnectionFactory) {
        RedisTemplate<String, Object> redisTemplate = new RedisTemplate<>();
        redisTemplate.setConnectionFactory(redisConnectionFactory);
        // 创建一个json的序列化对象
        GenericJackson2JsonRedisSerializer jackson2JsonRedisSerializer = new GenericJackson2JsonRedisSerializer();
        // 设置默认序列化器为
        redisTemplate.setDefaultSerializer(jackson2JsonRedisSerializer);
        // 设置value的序列化方式json
        redisTemplate.setValueSerializer(jackson2JsonRedisSerializer);
        // 设置key序列化方式String
        redisTemplate.setKeySerializer(new StringRedisSerializer());
        // 设置hash key序列化方式String
        redisTemplate.setHashKeySerializer(new StringRedisSerializer());
        // 设置hash value序列化json
        redisTemplate.setHashValueSerializer(jackson2JsonRedisSerializer);
        // 设置支持事务
        redisTemplate.setEnableTransactionSupport(true);
        redisTemplate.afterPropertiesSet();
        return redisTemplate;
    }

    @Bean
    public RedisSerializer<Object> redisSerializer() {
        // 创建JSON序列化器
        ObjectMapper objectMapper = new ObjectMapper();
        objectMapper.setVisibility(PropertyAccessor.ALL, JsonAutoDetect.Visibility.ANY);
        // 必须设置，否则无法将JSON转化为对象，会转化成Map类型
        objectMapper.activateDefaultTyping(LaissezFaireSubTypeValidator.instance, ObjectMapper.DefaultTyping.NON_FINAL);
        return new GenericJackson2JsonRedisSerializer(objectMapper);
    }

    @Bean
    public DefaultRedisScript<Long> limitScript()
    {
        DefaultRedisScript<Long> redisScript = new DefaultRedisScript<>();
        redisScript.setScriptText(limitScriptText());
        redisScript.setResultType(Long.class);
        return redisScript;
    }

    /**
     * <h2>限流脚本</h2>
     * 解释:
     * 这段 Lua 脚本是在 Redis 中使用的一个脚本，通常用于原子性地增加一个键的值，如果该键不存在的话，还会设置该键的过期时间。<br>
     * local key = KEYS[1]：从KEYS数组中获取第一个键，KEYS是一个由 Redis 预定义好的全局变量，包含了所有传递给 Lua 脚本的键。<br>
     * local count = tonumber(ARGV[1])：从 ARGV 数组中获取第一个参数，并将其转换为数字，这里假设该参数是一个预设的数值。<br>
     * local time = tonumber(ARGV[2])：从 ARGV 数组中获取第二个参数，并将其转换为数字，这里假设该参数是键的过期时间。<br>
     * local current = redis.call('get', key);：使用 Redis 命令 'get' 获取 key 的当前值。<br>
     * if current and tonumber(current) > count then return tonumber(current) end：如果 key 存在且其值大于预设的 count，则返回该值。<br>
     * current = redis.call('incr', key)：使用 Redis 命令 'incr' 递增 key 的值，如果 key 不存在，则创建它并设置其值为 1。<br>
     * if tonumber(current) == 1 then redis.call('expire', key, time) end：如果递增后的值为 1，则设置该键的过期时间。<br>
     * return tonumber(current);：返回 key 的新值。<br>
     * 这个脚本的主要用途是实现一种“计数器”的功能，可以原子性地递增一个键的值，同时还可以设置该键的过期时间。这在很多情况下都非常有用，例如统计点击次数、限制某些操作次数等。<br>
     */
    private String limitScriptText()
    {
        return "local key = KEYS[1]\n" +
                "local count = tonumber(ARGV[1])\n" +
                "local time = tonumber(ARGV[2])\n" +
                "local current = redis.call('get', key);\n" +
                "if current and tonumber(current) > count then\n" +
                "    return tonumber(current);\n" +
                "end\n" +
                "current = redis.call('incr', key)\n" +
                "if tonumber(current) == 1 then\n" +
                "    redis.call('expire', key, time)\n" +
                "end\n" +
                "return tonumber(current);";
    }
    
}

