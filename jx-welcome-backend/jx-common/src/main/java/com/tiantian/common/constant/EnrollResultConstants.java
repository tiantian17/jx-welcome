package com.tiantian.common.constant;

/**
 * 录取结果及其索引常量
 *
 * @author TianTian
 */
public interface EnrollResultConstants {

    // ===========录取结果相关===========

    /**
     * 录取进行中
     */
    String ENROLL_IN_PROGRESS = "0";

    /**
     * 未录取
     */
    String ENROLL_NOT_PASS = "1";

    /**
     * 录取成功
     */
    String ENROLL_PASS = "2";

    //===========二轮面试相关===========


    /**
     * 二轮面试进行中
     */
    String INTERVIEW_IN_PROGRESS = "0";

    /**
     * 二轮面试未通过
     */
    String INTERVIEW_NOT_PASS = "1";

    /**
     * 二轮面试通过
     */
    String INTERVIEW_PASS = "2";

    // ===========索引相关===========

    /**
     * 提交解题index
     */
    int ENROLL_RESULT_REPLY_INDEX = 0;

    /**
     * 进行评分index
     */
    int ENROLL_RESULT_JUDGE_INDEX = 1;

    /**
     * 二轮面试index
     */
    int ENROLL_RESULT_INTERVIEW_INDEX = 2;

    /**
     * 录取成功index
     */
    int ENROLL_RESULT_INDEX = 3;


    // ===========评分相关===========
    /**
     * 评分进行中
     */
    String SCORE_IN_PROGRESS = "0";

    /**
     * 评分未通过
     */
    String SCORE_NOT_THROUGH = "1";

    /**
     * 评分通过
     */
    String SCORE_THROUGH = "2";
}
