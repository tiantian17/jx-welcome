package com.tiantian.common.core.page;


import cn.hutool.core.util.ObjectUtil;
import com.tiantian.common.utils.StringUtils;
import lombok.Data;

/**
 * 分页数据
 *
 * @author ruoyi
 */
@Data
public class PageDomain {
    /**
     * 当前记录起始索引
     */
    private Integer pageNum;

    /**
     * 每页显示记录数
     */
    private Integer pageSize;

    /**
     * 要排序的字段
     */
    private String field;

    /**
     * 排序的方向
     */
    private String order;

    /**
     * 分页参数合理化
     */
    private Boolean reasonable = true;

}
