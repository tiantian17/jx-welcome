package com.tiantian.common.constant;

/**
 * 用户常量信息
 *
 * @author TianTian
 */
public interface UserConstants {

    /**
     * 平台内系统用户的唯一标志
     */
    String SYS_USER = "SYS_USER";

    /**
     * 正常状态
     */
    String NORMAL = "0";

    /**
     * 异常状态
     */
    String EXCEPTION = "1";

    /**
     * 用户正常状态
     */
    String USER_NORMAL = "0";

    /**
     * 用户封禁状态
     */
    String USER_DISABLE = "1";

    /**
     * 角色正常状态
     */
    String ROLE_NORMAL = "0";

    /**
     * 角色封禁状态
     */
    String ROLE_DISABLE = "1";

    /**
     * 用户名长度限制
     */
    int USERNAME_MIN_LENGTH = 5;
    int USERNAME_MAX_LENGTH = 20;

    /**
     * 密码长度限制
     */
    int PASSWORD_MIN_LENGTH = 5;
    int PASSWORD_MAX_LENGTH = 20;

    /**
     * 管理员ID
     */
    Long ADMIN_ID = 1L;


    /**
     * 录取状态 0待录取
     */
    String ENROLL_STATUS_UNDERWAY = "0";


    /**
     * 录取状态 1未录取
     */
    String ENROLL_STATUS_REJECTED = "1";


    /**
     * 录取状态 2已录取
     */
    String ENROLL_STATUS_ADMITTED = "2";


    /**
     * 录取状态 协会内成员
     */
    String ENROLL_STATUS_MEMBER = "3";


}
