package com.tiantian.common.validator;

import com.tiantian.common.validator.annotation.LevelCheck;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

public class TopicLevelValidator implements ConstraintValidator<LevelCheck, String> {
    @Override
    public boolean isValid(String value, ConstraintValidatorContext context) {
        return "0".equals(value) || "1".equals(value) || "2".equals(value);
    }

}