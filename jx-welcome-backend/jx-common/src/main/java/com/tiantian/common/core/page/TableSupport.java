package com.tiantian.common.core.page;


import cn.hutool.core.convert.Convert;
import com.tiantian.common.utils.ServletUtils;
import com.tiantian.common.utils.StringUtils;

/**
 * 表格数据处理
 *
 * @author ruoyi
 */
public class TableSupport {
    /**
     * 当前记录起始索引
     */
    public static final String PAGE_NUM = "pageNum";

    /**
     * 每页显示记录数
     */
    public static final String PAGE_SIZE = "pageSize";

    /**
     * 分页参数合理化
     */
    public static final String REASONABLE = "reasonable";

    /**
     * 排序字段
     */
    public static final String FIELD = "field";

    /**
     * 排序方向
     */
    public static final String ORDER = "order";


    /**
     * 封装分页对象
     */
    public static PageDomain getPageDomain() {
        PageDomain pageDomain = new PageDomain();
        pageDomain.setPageNum(Convert.toInt(ServletUtils.getParameter(PAGE_NUM), 1));
        pageDomain.setPageSize(Convert.toInt(ServletUtils.getParameter(PAGE_SIZE), 10));
        pageDomain.setField(Convert.toStr(ServletUtils.getParameter(FIELD), ""));
        pageDomain.setOrder(Convert.toStr(ServletUtils.getParameter(ORDER), ""));
        pageDomain.setReasonable(ServletUtils.getParameterToBool(REASONABLE));
        return pageDomain;
    }

    public static PageDomain buildPageRequest() {
        return getPageDomain();
    }
}
