package com.tiantian.common.core.domain;

import lombok.Data;

/**
 * @author TianTian
 */
@Data
public class PageQuery {
    private static final long serialVersionUID = 1L;

    /**
     * 分页大小
     */
    private Integer pageSize = 1;

    /**
     * 当前页数
     */
    private Integer pageNum = 10;

}
