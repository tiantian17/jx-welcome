package com.tiantian.common.annotation;

import com.tiantian.common.enums.BusinessType;
import com.tiantian.common.enums.OperatorType;

import java.lang.annotation.*;

/**
 * 自定义操作日志记录注解
 *
 */
@Target({ElementType.PARAMETER, ElementType.METHOD})
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface Log {
    /**
     * 模块
     */
    String title() default "";

    /**
     * 业务类型（0其它 1新增 2修改 3删除 4登录 5登出<br>
     * 6录取 7未录取 8设置题目 9提交题目 10判断题目<br>
     * 11判题人设置 12删除判题人 13新人分配
     */
    BusinessType businessType() default BusinessType.OTHER;

    /**
     * 操作人的昵称
     */
    OperatorType operatorType() default OperatorType.OTHER;

    /**
     * 是否保存请求的参数
     */
    boolean isSaveRequestData() default true;

    /**
     * 是否保存响应的参数
     */
    boolean isSaveResponseData() default true;

    /**
     * 排除指定的请求参数
     */
    String[] excludeParamNames() default {};

}
