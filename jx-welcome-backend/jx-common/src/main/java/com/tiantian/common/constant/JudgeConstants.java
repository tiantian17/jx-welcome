package com.tiantian.common.constant;

/**
 * @author TianTian
 */
public interface JudgeConstants {
    /**
     * 未提交
     */
    String NOT_SUBMITTED = "0";

    /**
     * 部分提交
     */
    String PARTIAL_SUBMISSION = "1";

    /**
     * 完全提交
     */
    String FULL_SUBMISSION = "2";

    /**
     * 零
     */
    Integer ZERO = 0;

    /**
     * 未进行评分
     */
    String JUDGE_NOT_IN_PROGRESS = "0";

    /**
     * 评分完毕
     */
    String JUDGE_FINISH = "1";

}
