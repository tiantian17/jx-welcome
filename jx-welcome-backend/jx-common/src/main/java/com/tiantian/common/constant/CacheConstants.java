package com.tiantian.common.constant;

/**
 * 缓存的key 常量
 *

 */
public interface CacheConstants {

    /**
     * 用户信息 redis key
     */
    String USER_INFO_KEY ="user_info:";

    /**
     * 限流 redis key
     */
    public static final String RATE_LIMIT_KEY = "rate_limit:";

    /**
     * 防重提交 redis key
     */
    String REPEAT_SUBMIT_KEY = "repeat_submit:";

    /**
     * 登录账户密码错误次数 redis key
     */
    String PWD_ERR_CNT_KEY = "pwd_err_cnt:";

    /**
     * 验证码 redis key
     */
    String CAPTCHA_CODE_KEY = "captcha_codes:";

    /**
     * 更新邮箱 redis key
     */
    String UPDATE_EMAIL_CAPTCHA_CODE_KEY = "update_email_captcha_codes:";
}
