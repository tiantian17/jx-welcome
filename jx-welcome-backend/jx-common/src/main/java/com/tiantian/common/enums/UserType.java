package com.tiantian.common.enums;

import com.tiantian.common.utils.StringUtils;
import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * 用户体系
 */
@Getter
@AllArgsConstructor
public enum UserType {

    /**
     * 管理员用户(协会内成员)
     */
    SYS_USER("00"),

    /**
     * 新人用户
     */
    SYS_NEWCOMER("01");

    private final String userType;

    public static UserType getUserType(String str) {
        for (UserType value : values()) {
            if (StringUtils.contains(str, value.getUserType())) {
                return value;
            }
        }
        throw new RuntimeException("用户类型查找失败");
    }
}
