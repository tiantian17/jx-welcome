package com.tiantian.common.validator;

import com.tiantian.common.validator.annotation.EmailTypeCheck;
import com.tiantian.common.enums.EmailType;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

public class EmailTypeValidator implements ConstraintValidator<EmailTypeCheck, String> {
    @Override
    public boolean isValid(String value, ConstraintValidatorContext context) {
        return EmailType.getEmailType(value);
    }

}