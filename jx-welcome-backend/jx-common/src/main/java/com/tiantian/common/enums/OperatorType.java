package com.tiantian.common.enums;

/**
 * 操作人类别
 *

 */
public enum OperatorType {
    /**
     * 其它
     */
    OTHER,

    /**
     * 协会内用户
     */
    ADMIN,

    /**
     * 新人
     */
    NEWCOMER
}
