package com.tiantian.common.core.controller;


import com.github.pagehelper.PageInfo;
import com.tiantian.common.core.domain.BaseEntity;
import com.tiantian.common.core.domain.ResponseResult;
import com.tiantian.common.core.domain.model.LoginUser;
import com.tiantian.common.core.page.TableDataInfo;
import com.tiantian.common.helper.LoginHelper;
import com.tiantian.common.utils.PageUtils;

import java.time.LocalDateTime;
import java.util.List;

/**
 * web层通用数据处理
 *
 * @author TianTian
 */
public class BaseController {

    /**
     * 响应返回结果
     *
     * @param rows 影响行数
     * @return 操作结果
     */
    protected ResponseResult<Void> toAjax(int rows) {
        return rows > 0 ? ResponseResult.ok() : ResponseResult.fail();
    }

    /**
     * 响应返回结果
     *
     * @param result 结果
     * @return 操作结果
     */
    protected ResponseResult<Void> toAjax(boolean result) {
        return result ? ResponseResult.ok() : ResponseResult.fail();
    }

    /**
     * 获取用户缓存信息
     */
    public LoginUser getLoginUser() {
        return LoginHelper.getLoginUser();
    }

    /**
     * 获取登录用户id
     */
    public Long getUserId() {
        return LoginHelper.getUserId();
    }

    /**
     * 获取登录用户名
     */
    public String getUsername() {
        return LoginHelper.getUsername();
    }

    /**
     * 获取登录用户人真实姓名
     */
    public String getNickName() {
        return LoginHelper.getNickName();
    }

    /**
     * 设置请求分页数据
     */
    protected void startPage() {
        PageUtils.startPage();
    }

    /**
     * 响应请求分页数据
     */
    @SuppressWarnings({"rawtypes", "unchecked"})
    protected TableDataInfo getDataTable(List<?> list) {
        TableDataInfo rspData = new TableDataInfo();
        rspData.setCode(ResponseResult.SUCCESS);
        rspData.setMsg("查询成功");
        rspData.setRows(list);
        rspData.setTotal(new PageInfo(list).getTotal());
        return rspData;
    }

    /**
     * 公共字段填充 更新操作
     */
    protected void setCommonFieldUpdate(BaseEntity baseEntity){
        baseEntity.setUpdateBy(getNickName());
        baseEntity.setUpdateTime(LocalDateTime.now());

    }

    /**
     * 公共字段填充 插入操作
     */
    protected void setCommonFieldInsert(BaseEntity baseEntity){
        baseEntity.setCreateBy(getNickName());
        baseEntity.setCreateTime(LocalDateTime.now());
        baseEntity.setUpdateBy(getNickName());
        baseEntity.setUpdateTime(LocalDateTime.now());
    }
}
