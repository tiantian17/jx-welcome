package com.tiantian.common.validator.annotation;

import com.tiantian.common.validator.TopicLevelValidator;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.*;

/**
 * @author TianTian
 */
@Target({ElementType.FIELD})
@Retention(RetentionPolicy.RUNTIME)
@Documented
@Constraint(validatedBy = TopicLevelValidator.class)
public @interface LevelCheck {
    String message() default "难度类型错误";

    Class<?>[] groups() default {};

    Class<? extends Payload>[] payload() default {};
}
