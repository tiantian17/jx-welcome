package com.tiantian.common.core.domain.model;

import com.tiantian.common.core.domain.dto.RoleDTO;
import com.tiantian.common.core.domain.entity.SysRolePerm;
import com.tiantian.common.core.domain.vo.SysRolePermVO;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.util.List;
import java.util.Set;

/**
 * 登录用户身份权限
 *
 * @author TianTian
 */

@Data
@NoArgsConstructor
public class LoginUser implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 用户ID
     */
    private Long userId;

    /**
     * 用户名
     */
    private String userName;

    /**
     * 真实姓名
     */
    private String nickName;

    /**
     * 部门ID
     */
    private Long deptType;

    /**
     * 用户唯一标识
     */
    private String token;

    /**
     * 用户类型
     */
    private String userType;

    /**
     * 用户角色
     */
    private RoleDTO roleDTO;

    /**
     * 用户权限
     */
    private List<SysRolePermVO> permissions;


    /**
     * 数据权限 当前角色ID
     */
    private Long roleId;

    /**
     * 获取登录id
     */
    public String getLoginId() {
        if (userType == null) {
            throw new IllegalArgumentException("用户类型不能为空");
        }
        if (userId == null) {
            throw new IllegalArgumentException("用户ID不能为空");
        }
        return userType + ":" + userId;
    }

}
