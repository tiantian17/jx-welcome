package com.tiantian.common.core.domain;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

/**
 * 响应信息主体
 *
 * @author TianTian
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class ResponseResult<T> implements Serializable {
    private static final long serialVersionUID = 1L;

    /**
     * 成功
     */
    public static final int SUCCESS = 200;
    /**
     * 失败
     */
    public static final int FAIL = 400;
    /**
     * 系统警告消息
     */
    public static final int WARN = 601;

    private int code;

    private String msg;

    private T data;

    public static <T> ResponseResult<T> ok() {
        return restResult(null, SUCCESS, "操作成功");
    }

    public static <T> ResponseResult<T> ok(T data) {
        return restResult(data, SUCCESS, "操作成功");
    }

    public static <T> ResponseResult<T> ok(String msg) {
        return restResult(null, SUCCESS, msg);
    }

    public static <T> ResponseResult<T> ok(String msg, T data) {
        return restResult(data, SUCCESS, msg);
    }

    public static <T> ResponseResult<T> fail() {
        return restResult(null, FAIL, "操作失败");
    }

    public static <T> ResponseResult<T> fail(String msg) {
        return restResult(null, FAIL, msg);
    }

    public static <T> ResponseResult<T> fail(T data) {
        return restResult(data, FAIL, "操作失败");
    }

    public static <T> ResponseResult<T> fail(String msg, T data) {
        return restResult(data, FAIL, msg);
    }

    public static <T> ResponseResult<T> fail(int code, String msg) {
        return restResult(null, code, msg);
    }

    /**
     * 返回警告消息
     *
     * @param msg 返回内容
     * @return 警告消息
     */
    public static <T> ResponseResult<T> warn(String msg) {
        return restResult(null, WARN, msg);
    }

    /**
     * 返回警告消息
     *
     * @param msg 返回内容
     * @param data 数据对象
     * @return 警告消息
     */
    public static <T> ResponseResult<T> warn(String msg, T data) {
        return restResult(data, WARN, msg);
    }

    private static <T> ResponseResult<T> restResult(T data, int code, String msg) {
        ResponseResult<T> responseResult = new ResponseResult<>();
        responseResult.setCode(code);
        responseResult.setData(data);
        responseResult.setMsg(msg);
        return responseResult;
    }

    public static <T> Boolean isError(ResponseResult<T> ret) {
        return !isSuccess(ret);
    }

    public static <T> Boolean isSuccess(ResponseResult<T> ret) {
        return ResponseResult.SUCCESS == ret.getCode();
    }
}
