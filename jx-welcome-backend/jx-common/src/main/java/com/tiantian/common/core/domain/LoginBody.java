package com.tiantian.common.core.domain;

import com.tiantian.common.constant.UserConstants;
import com.tiantian.common.validator.annotation.Xss;
import lombok.Data;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.NotBlank;

/**
 * 用户登录对象
 */
@Data
public class LoginBody {

    /**
     * 用户名
     */
    @NotBlank(message = "账号不能为空")
    @Xss(message = "用户账号不能包含任何脚本字符")
    @Length(min = UserConstants.PASSWORD_MIN_LENGTH, max = UserConstants.PASSWORD_MAX_LENGTH, message = "用户名长度需要在5-20之间")
    private String username;

    /**
     * 用户密码
     */
    @NotBlank(message = "密码不能为空")
    @Length(min = UserConstants.PASSWORD_MIN_LENGTH, max = UserConstants.PASSWORD_MAX_LENGTH, message = "密码长度不符合规则")
    private String password;

    /**
     * 验证码
     */
    private String captcha;

    /**
     * 唯一标识
     */
    private String uuid;

}
