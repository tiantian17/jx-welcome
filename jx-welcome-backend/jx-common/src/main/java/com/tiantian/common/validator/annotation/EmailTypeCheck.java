package com.tiantian.common.validator.annotation;

import com.tiantian.common.validator.EmailTypeValidator;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.*;

/**
 * 邮件类型校验
 *
 * @author TianTian
 */
@Target({ElementType.FIELD})
@Retention(RetentionPolicy.RUNTIME)
@Documented
@Constraint(validatedBy = EmailTypeValidator.class)
public @interface EmailTypeCheck {
    String message() default "邮件类型错误";

    Class<?>[] groups() default {};

    Class<? extends Payload>[] payload() default {};
}