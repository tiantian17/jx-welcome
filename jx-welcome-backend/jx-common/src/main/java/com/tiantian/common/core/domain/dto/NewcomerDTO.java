package com.tiantian.common.core.domain.dto;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import java.time.LocalDateTime;

/**
 * @author TianTian
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class NewcomerDTO {

    /**
     * 用户id
     */
    private Long userId;

    /**
     * 真实姓名
     */
    @NotBlank(message = "真实姓名姓名不能为空")
    private String nickName;

    /**
     * 班级
     */
    @NotBlank(message = "班级不能为空")
    private String clasz;

    @NotBlank(message = "电话号码不能为空")
    private String phonenumber;

    /**
     * 录取状态 0:进行中 1:未录取 2:已录取
     */
    @NotBlank(message = "录取状态不能为空")
    private String enrollStatus;

    /**
     * 用户邮箱
     */
    @Email(message = "邮箱格式不正确")
    private String email;

    /**
     * 用户性别 0女 1男
     */
    @NotBlank(message = "用户性别不能为空")
    private String sex;

    /**
     * 更新人
     */
    private String updateBy;

    /**
     * 更新时间
     */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private LocalDateTime updateTime;
}
