package com.tiantian.common.core.domain.entity;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.time.LocalDateTime;

/**
 * 通知公告表(SysNotice)实体类
 *
 * @author tiantian
 */

@Data
@AllArgsConstructor
@NoArgsConstructor
public class SysNotice implements Serializable {

    private static final long serialVersionUID = -35671373296685655L;

    /**
     * 公告ID
     */
    private Long noticeId;

    /**
     * 公告标题
     */
    private String title;

    /**
     * 通知程度(0:普通 1:警告 2:紧急通知)
     */
    private String level;

    /**
     * 公告内容(HTML格式)
     */
    private String htmlContent;

    /**
     * 窗口显示宽度
     */
    private Integer width;

    /**
     * 显示时长
     */
    private Integer duration;

    /**
     * 更新者(真实姓名)
     */
    private String createBy;

    /**
     * 更新时间
     */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private LocalDateTime createTime;

    /**
     * 备注
     */
    private String remark;

}

