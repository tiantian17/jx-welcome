package com.tiantian.common.core.domain.vo;

import lombok.Data;

import java.util.List;

/**
 * @author TianTian
 */
@Data
public class SysJudgerVO {

    /**
     * 主键
     */
    private String judgerId;


    /**
     * 判题人的ID (user_id)
     */
    private Long userId;


    /**
     * 判踢人的姓名
     */
    private String judgerName;


    /**
     * 判题状态( 0:未完成 1:进行中 2全部:已完成 3:未知状态)
     */
    private String status;

    /**
     * 被判题的用户ID
     */
    private List<Long> replyUsers;

    /**
     * 用户姓名集合
     */
    private List<String> nickNames;


    /**
     * 判断的答题的ID
     */
    private List<String> replyIds;

    /**
     * 需要判题的数量
     */
    private Integer count;

}
