package com.tiantian.common.core.domain.vo;

import com.tiantian.common.core.domain.entity.SysUserAction;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.util.List;

/**
 * 角色和菜单关联表(SysRolePerm)实体类
 *
 * @author tiantian
 */

@Data
@AllArgsConstructor
@NoArgsConstructor
public class SysRolePermVO implements Serializable {

    private static final long serialVersionUID = -18684801009015010L;
    /**
     * 角色ID
     */
    private Long roleId;
    /**
     * 权限
     */
    private String permission;
    /**
     * 权限
     */
    private String permissionName;
    /**
     * 操作权限
     */
    private List<SysUserAction> actionEntitySet;

}

