package com.tiantian.common.core.domain.entity;

import java.io.Serializable;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * 角色和菜单关联表(SysRolePerm)实体类
 *
 * @author tiantian
 */

@Data
@AllArgsConstructor
@NoArgsConstructor
public class SysRolePerm implements Serializable {

    private static final long serialVersionUID = -18684801009015010L;
    /**
     * 角色ID
     */
    private Long roleId;
    /**
     * 权限
     */
    private String permission;
    /**
     * 权限
     */
    private String permissionName;

}

