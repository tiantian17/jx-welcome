package com.tiantian.common.core.domain.dto;

import lombok.Data;

import javax.validation.constraints.NotBlank;

/**
 * 答题时间
 *
 * @author TianTian
 */
@Data
public class AnswerTimeDAO {

    /**
     * 日期
     */
    @NotBlank(message = "日期不能为空")
    private String date;

    /**
     * 开始时间
     */
    @NotBlank(message = "开始时间不能为空")
    private String start;

    /**
     * 结束时间
     */
    @NotBlank(message = "结束时间不能为空")
    private String end;

}
