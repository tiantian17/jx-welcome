package com.tiantian.common.enums;

import com.tiantian.common.utils.StringUtils;
import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * 邮件类型
 *
 * @author TianTian
 */
@Getter
@AllArgsConstructor
public enum EmailType {

    /**
     * HTML格式
     */
    HTML("html"),

    /**
     * 文本格式
     */
    TEXT("text");

    private final String type;

    public static boolean getEmailType(String str) {
        for (EmailType value : values()) {
            if (StringUtils.contains(str, value.getType())) {
                return true;
            }
        }
        return false;
    }
}
