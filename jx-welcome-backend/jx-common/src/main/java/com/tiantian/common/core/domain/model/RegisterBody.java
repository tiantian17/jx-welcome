package com.tiantian.common.core.domain.model;

import com.tiantian.common.constant.UserConstants;
import com.tiantian.common.core.domain.LoginBody;
import lombok.Data;
import lombok.EqualsAndHashCode;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;

/**
 * 用户注册对象
 */
@Data
@EqualsAndHashCode(callSuper = true)
public class RegisterBody extends LoginBody {

    /**
     * 确认密码
     */
    @NotBlank(message = "密码不能为空")
    @Length(min = UserConstants.PASSWORD_MIN_LENGTH, max = UserConstants.PASSWORD_MAX_LENGTH, message = "密码长度不符合规则应在5-20位之间")
    private String confirmPassword;

    /**
     * 班级
     */
    @NotBlank(message = "班级不能为空")
    private String clasz;

    /**
     * 姓名
     */
    @NotBlank(message = "姓名不能为空")
    private String nickName;

    /**
     * 学号
     */
    @NotBlank(message = "学号不能为空")
    @Length(max = 12, message = "学号长度不符合规范")
    private String studentId;

    /**
     * 班级
     */
    @NotBlank(message = "电话号码不能为空")
    @Length(max = 11, message = "学号长度不符合规范")
    private String phonenumber;

    /**
     * 班级
     */
    @Email(message = "邮箱格式不符合规划")
    @NotBlank(message = "邮箱不能为空")
    private String email;

    /**
     * 邀请码
     */
    @NotBlank(message = "邀请码不能为空")
    private String invitationCode;

}
