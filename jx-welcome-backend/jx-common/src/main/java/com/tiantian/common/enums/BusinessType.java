package com.tiantian.common.enums;

/**
 * 业务操作类型
 *
 */
public enum BusinessType {

    /**
     * 获取验证码0
     */
    CAPTCHA,

    /**
     * 登录1
     */
    LOGIN,

    /**
     * 注册2
     */
    REGISTER,

    /**
     * 批量注册3
     */
    REGISTER_BATCH,

    /**
     * 设置题目4
     */
    SET_TOPIC,

    /**
     * 更新题目5
     */
    UPDATE_TOPIC,

    /**
     * 删除题目6
     */
    DEL_TOPIC,

    /**
     * 新人提交题目7
     */
    NEWCOMER_SUBMIT_TOPIC,

    /**
     * 新人修改答案8
     */
    NEWCOMER_UPDATE_ANS,

    /**
     * 管理员更新新人信息9
     */
    UPDATE_NEWCOMER,

    /**
     * 判题人设置10
     */
    JUDGE_SETTING,

    /**
     * 删除判题人11
     */
    JUDGER_DELETE,

    /**
     * 判断题目12
     */
    JUDGE_TOPIC,

    /**
     * 进行录取13
     */
    ENROLL,

    /**
     * 未录取14
     */
    NOT_ENROLL,

    /**
     * 分数线设置15
     */
    SCORE_LINE,

    /**
     * 答题时间设置16
     */
    ANS_TIME,

    /**
     * 修改注册开关17
     */
    REGISTER_SETTING,

    /**
     * 系统邮件发送18
     */
    EMAIL,

    /**
     * 文件导出19
     */
    FILE_EXPORT,

    /**
     * 更新个人信息20
     */
    UPDATE_PROFILE,

    /**
     * 分配新人21
     */
    ALLOT_NEWCOMER,

    /**
     * 计算零分用户22
     */
    CALC_SCORE,

    /**
     * 删除工作人员作答记录23
     */
    DEL_ADMIN_REPLYS,

    /**
     * 设置录取状态24
     */
    SET_ENR_STATUS,

    /**
     * 删除操作日志25
     */
    DEL_LOG,

    /**
     * 清空操作日志26
     */
    CLEAR_LOG,

    /**
     * 文件上传27
     */
    FILE_UPLOAD,

    /**
     * 其它28
     */
    OTHER,

}
