package com.tiantian.common.constant;

/**
 * 题目常量
 *
 * @author TianTian
 */
public interface TopicConstants {
    /**
     * 题目未完成
     */
    String UNCOMPLETED = "0";

    /**
     * 题目已完成
     */
    String FINISHED = "1";

    /**
     * 题目设置审核进行中
     */
    String EXAMINE_WAIT = "0";

    /**
     * 题目设置审核未通过
     */
    String EXAMINE_NOT_PASS = "1";

    /**
     * 题目设置审核通过
     */
    String EXAMINE_PASS = "2";

    /**
     * 评分正在进行中
     */
    String JUDGE_IN_PROGRESS = "0";
    /**
     * 评分进行中
     */
    String JUDGE_FINISHED = "1";

}
