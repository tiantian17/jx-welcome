package com.tiantian.common.constant;

/**
 * 系统常量
 *
 * @author TianTian
 */
public interface SystemConfigConstants {

    /**
     * 答题时间
     */
    String SYSTEM_CONFIG_ANSWER_TIME_KEY = "system:config:answer_time";

    /**
     * 是否开启注册
     */
    String SYSTEM_ENABLE_REGISTER_KEY = "system:config:enable_register";

    /**
     * 二轮面试信息
     */
    String SYSTEM_CONFIG_SECOND_ROUND_INFO_KEY = "system:config:second_round";

    /**
     * 二轮面试分数线
     */
    String SYSTEM_CONFIG_INTERVIEW_SCORE_LINE = "system:config:interview_score_line";
}
