package com.tiantian.common.core.domain.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotBlank;

/**
 * @author TianTian
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class SysNoticeDTO {

    /**
     * 消息类型 "0": 普通  "1"警告  "2"紧急通知
     */
    @NotBlank(message = "消息程度不能为空")
    private String level;

    /**
     * 消息体
     */
    @NotBlank(message = "消息内容不能为空")
    private String htmlContent;

    /**
     * 标题
     */
    @NotBlank(message = "标题不能为空")
    private String title;

    /**
     * 显示时间
     */
    @Min(value = -1, message = "宽度不能小于500")
    private Integer duration;

    /**
     * 通知占屏幕的宽度
     */
    @Max(value = 1500, message = "宽度不能超过1500")
    @Min(value = 500, message = "宽度不能小于500")
    private Integer width;
}
