package com.tiantian.common.core.domain.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

/**
 * (SysUserAction)实体类
 *
 * @author tiantian
 * @since 2023-10-07 20:58:37
 */

@Data
@AllArgsConstructor
@NoArgsConstructor
public class SysUserAction implements Serializable {

    private static final long serialVersionUID = -80166437015700546L;
    /**
     * 主键ID
     */
    private Integer actionId;
    /**
     * 操作名称
     */
    private String action;
    /**
     * 描述
     */
    private String remark;

}

