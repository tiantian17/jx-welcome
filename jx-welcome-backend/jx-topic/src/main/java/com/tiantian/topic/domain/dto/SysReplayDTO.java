package com.tiantian.topic.domain.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotBlank;

/**
 * SysReplayDTO用于接收用户修改的题目
 *
 * @author TianTian
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class SysReplayDTO {
    private static final long serialVersionUID = 1L;

    /**
     * 题目ID (用于查询题目标题和题目详情)
     */
    @NotBlank(message = "topicId不能为空")
    private String topicId;

    /**
     * 用户提交的代码
     */
    @NotBlank(message = "答案不能为空")
    private String answer;
}
