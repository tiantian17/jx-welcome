package com.tiantian.topic.domain.dto;

import lombok.Data;

/**
 * 查询提交参数
 *
 * @author TianTian
 */
@Data
public class QueryReplySubDTO {

    /**
     * 姓名
     */
    private String nickName;

    /**
     * 学号
     */
    private String studentId;

    /**
     * 是否提交
     */
    private Boolean isSub;
}
