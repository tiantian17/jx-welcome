package com.tiantian.topic.service;

import com.tiantian.common.core.domain.entity.SysJudger;
import com.tiantian.topic.domain.dto.QueryReplySubDTO;
import com.tiantian.topic.domain.sdo.SysJudgeScoreSDO;
import com.tiantian.topic.domain.vo.SubInfoVO;
import com.tiantian.topic.domain.vo.SysJudgeDetailVO;
import com.tiantian.topic.domain.vo.SysJudgeVO;

import java.util.List;

/**
 * @author TianTian
 */
public interface SysJudgeService {
    /**
     * 获取判题列表
     *
     * @param sysJudgeVO 查询条件
     * @return 分页数据
     */
    List<SysJudgeVO> selectJudgeList(SysJudgeVO sysJudgeVO);

    /**
     * 根据用户ID查询判题信息
     *
     * @param userId 用户ID
     * @return SysJudgeVO
     */
    SysJudgeVO selectJudgeInfoByUserId(Long userId);

    /**
     * 根据用户ID查询判断详细信息列表
     *
     * @param userId 用户ID
     * @return 答题信息列表
     */
    List<SysJudgeDetailVO> selectJudgeDetailListByUserId(String userId);

    /**
     * 根据replyId批量更新分数
     *
     * @param sysJudgeScoreList sysJudgeScoreList
     * @return 影响行数
     */
    int updateScoreByReplyId(List<SysJudgeScoreSDO> sysJudgeScoreList);


    List<SubInfoVO> selectSubInfo(QueryReplySubDTO queryReplySubDTO);

    /**
     * 获取判题人列表
     *
     */
    List<SysJudger> selectJudgerList(SysJudger sysJudger);


    /**
     * 新增判题人
     */
    int insertJudger(SysJudger sysJudger);

    /**
     * 根据ID获取判题人信息
     */
    SysJudger selectJudgerByUserId(Long userId);

    int allotNewcomer(List<SysJudger> sysJudgerList);

    int deleteJudgerById(String judgerId);

    int allotReplyIds(SysJudger sysJudger);

    int selectSubTotal();

    int selectNotSubTotal();
}
