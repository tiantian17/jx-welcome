package com.tiantian.topic.domain.vo;

import lombok.Data;

/**
 * 候选人
 *
 * @author TianTian
 */
@Data
public class CandidateVO {

    /**
     * 用户ID
     */
    private Long userId;

    /**
     * 学生ID
     */
    private String studentId;

    /**
     * 真实姓名
     */
    private String nickName;
}
