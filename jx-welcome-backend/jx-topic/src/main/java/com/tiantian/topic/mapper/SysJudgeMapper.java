package com.tiantian.topic.mapper;

import com.tiantian.topic.domain.dto.QueryReplySubDTO;
import com.tiantian.topic.domain.sdo.SysJudgeScoreSDO;
import com.tiantian.topic.domain.vo.SubInfoVO;
import com.tiantian.topic.domain.vo.SysJudgeDetailVO;
import com.tiantian.topic.domain.vo.SysJudgeVO;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * @author TianTian
 */
@Mapper
public interface SysJudgeMapper {

    /**
     * 获取判题列表
     *
     * @param sysJudgeVO 查询条件
     * @return 分页数据
     */
    List<SysJudgeVO> selectJudgeList(SysJudgeVO sysJudgeVO);

    /**
     * 根据用户ID查询判题信息
     *
     * @param userId 用户ID
     * @return SysJudgeVO
     */
    SysJudgeVO selectJudgeInfoByUserId(Long userId);

    /**
     * 根据用户ID查询判断详细信息列表
     *
     * @param userId 用户ID
     * @return 答题信息列表
     */
    List<SysJudgeDetailVO> selectJudgeDetailListByUserId(String userId);

    /**
     * 批量更新分数
     * @param sysJudgeScoreList 分数列表
     * @return 影响行数
     */
    int updateScoreByReplyIdBatch(List<SysJudgeScoreSDO> sysJudgeScoreList);

    /**
     * 获取提交信息
     * @param queryReplySubDTO 查询参数
     * @return SubInfoVO集合
     */
    List<SubInfoVO> selectSubInfo(QueryReplySubDTO queryReplySubDTO);

    /**
     * 获取评分状态 ("0"未评分 "1"评分完毕)
     */
    String selectJudgeStatusByUserId(Long userId);

    /**
     * 根据用户id设置判题人
     */
    String selectJudgerByUserId(Long userId);
}
