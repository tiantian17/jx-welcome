package com.tiantian.topic.service;

import com.tiantian.topic.domain.entity.SysTopic;
import com.tiantian.topic.domain.vo.SysTopicVO;

import java.util.List;

/**
 * (SysTopic)表服务接口
 *
 * @author tiantian
 * @since 2023-10-12 17:00:21
 */
public interface SysTopicService {

    /**
     * 通过ID查询单条数据
     *
     * @param topicId 主键
     * @return 实例对象
     */
    SysTopic selectById(String topicId);

    /**
     * 通过实体作为筛选条件查询
     *
     * @param sysTopic 实例对象
     * @return 对象列表
     */
    List<SysTopic> selectList(SysTopic sysTopic);

    /**
     * 通过实体作为筛选条件查询查询VO集合
     *
     * @param sysTopic 实例对象
     */
    List<SysTopicVO> selectTopicVOList(SysTopic sysTopic);

    /**
     * 新增数据
     *
     * @param sysTopic 实例对象
     * @return 新增标记
     */
    Boolean setTopic(SysTopic sysTopic);

    /**
     * 修改数据
     *
     * @param sysTopic 实例对象
     * @return 删除标记
     */
    Boolean updateById(SysTopic sysTopic);

    /**
     * 通过主键删除数据
     *
     * @param topicId 主键
     * @return 修改标记
     */
    Boolean deleteById(String topicId, Long userId);

    int getTopicTotal();
}


