package com.tiantian.topic.domain.gpt;

import lombok.Data;

import java.util.Map;

/**
 * @author TianTian
 */
@Data
public class Choices {

    private int index;

    private Map<String, String> delta;

    private String finish_reason;

}
