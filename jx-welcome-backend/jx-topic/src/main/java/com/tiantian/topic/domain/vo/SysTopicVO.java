package com.tiantian.topic.domain.vo;

import com.tiantian.common.constant.TopicConstants;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.util.List;

/**
 * (SysTopicVO)实体类
 *
 * @author tiantian
 */

@Data
@AllArgsConstructor
@NoArgsConstructor
public class SysTopicVO implements Serializable {

    private static final long serialVersionUID = -1L;
    /**
     * 题目ID
     */
    private String topicId;

    /**
     * 题目标题
     */
    private String title;

    /**
     * 题目难度(0简单 1中等 2困难)
     */
    private String level;

    /**
     * 题目标签
     */
    private List<String> tags;

    /**
     * 出题人姓名
     */
    private String authorName;

    /**
     * 是否已经完成该题目 0未完成 1已完成 (默认未完成)
     */
    private String isComplete = TopicConstants.UNCOMPLETED;
}

