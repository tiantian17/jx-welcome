package com.tiantian.topic.mapper;

import com.tiantian.topic.domain.entity.SysReplay;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

/**
 * (SysReplay)表数据库访问层
 *
 * @author tiantian
 */
@Mapper
public interface SysReplayMapper {

    /**
     * 通过ID查询单条数据
     *
     * @param replyId 主键
     * @return 实例对象
     */
    SysReplay selectById(String replyId);

    /**
     * 通过实体作为筛选条件查询
     *
     * @param sysReplay 实例对象
     * @return 对象列表
     */
    List<SysReplay> selectList(SysReplay sysReplay);

    /**
     * 新增数据
     *
     * @param sysReplay 实例对象
     * @return 影响行数
     */
    Boolean insert(SysReplay sysReplay);

    /**
     * 修改数据
     *
     * @param sysReplay 实例对象
     * @return 影响行数
     */
    Boolean updateAnswer(SysReplay sysReplay);

    /**
     * 删除工作人员答题记录
     */
    void deleteAdminReplys();

    /**
     * 检查用是否已经进行提交该题目
     * @param topicId 题目id
     * @param userId  用户ID
     * @return 行数
     */
    int checkReplyUnique(String topicId, Long userId);

    /**
     * 根据题目ID和用户ID查询出用户答案
     * @param topicId 题目ID
     * @param userId 用户ID
     * @return 答案
     */
    String selectAnsByTopicIdAndUserId(String topicId, Long userId);

    /**
     * 根据用户Id查询答题记录
     *
     * @param userId 用户Id
     */
    List<SysReplay> selectByUserId(Long userId);

    /**
     * 根据replyId更新数据
     */
    int updateByReplyId(SysReplay sysReplay);

    /**
     * 查询最后提交的时间
     */
    String selectLastReplyTime(Long userId);

    /**
     * 根据用户Id获取提交记录数
     * @param userId 用户Id
     */
    int countReplyByUserId(Long userId);

    /**
     * 根据用户id获取判题人
     */
    String selectJudgerByUserId(Long userId);
}


