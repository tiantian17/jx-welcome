package com.tiantian.topic.domain.sdo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;


/**
 * 用户接收判题分数和返回
 *
 * @author TianTian
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class SysJudgeScoreSDO {

    /**
     * 答题人提交ID
     */
    private String replyId;

    /**
     * 标题由前端转避免查询数据库
     */
    private String title;

    /**
     * 单题分数
     */
    private Integer score;

    /**
     * 判题人姓名 用于后端设置属性
     */
    private String judger;

    /**
     * 判题状态 0进行中 1判题完成
     */
    private String status;
}
