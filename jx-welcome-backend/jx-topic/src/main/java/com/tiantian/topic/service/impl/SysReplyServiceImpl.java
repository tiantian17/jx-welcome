package com.tiantian.topic.service.impl;

import com.tiantian.topic.domain.entity.SysReplay;
import com.tiantian.topic.mapper.SysReplayMapper;
import com.tiantian.topic.service.SysReplyService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * (SysReplay表)服务实现类
 *
 * @author tiantian
 */
@Service
@RequiredArgsConstructor
public class SysReplyServiceImpl implements SysReplyService {


    private final SysReplayMapper sysReplayMapper;

    /**
     * 通过ID查询单条数据
     *
     * @param replyId 主键
     * @return 实例对象
     */
    @Override
    public SysReplay selectById(String replyId) {
        return sysReplayMapper.selectById(replyId);
    }

    /**
     * 根据条件查询
     *
     * @return 实例对象的集合
     */
    @Override
    public List<SysReplay> selectList(SysReplay sysReplay) {
        return sysReplayMapper.selectList(sysReplay);
    }

    /**
     * 新增数据
     *
     * @param sysReplay 实例对象
     * @return 实例对象
     */
    @Override
    public Boolean insert(SysReplay sysReplay) {
        return sysReplayMapper.insert(sysReplay);
    }

    /**
     * 修改数据
     *
     * @param sysReplay 实例对象
     * @return 实例对象
     */
    @Override
    public Boolean changAnswer(SysReplay sysReplay) {
        return sysReplayMapper.updateAnswer(sysReplay);
    }

    /**
     * 删除工作人员答题记录
     */
    @Override
    public Boolean deleteAdminReplys() {
        sysReplayMapper.deleteAdminReplys();
        return true;
    }

    @Override
    public int checkReplyUnique(String topicId, Long userId) {
        return sysReplayMapper.checkReplyUnique(topicId, userId);
    }

    @Override
    public String selectAnsByTopicIdAndUserId(String topicId, Long userId) {
        return sysReplayMapper.selectAnsByTopicIdAndUserId(topicId, userId);
    }

    @Override
    public int updateByReplyId(SysReplay sysReplay) {
        return sysReplayMapper.updateByReplyId(sysReplay);
    }

    @Override
    public int countReply(Long userId) {
        return sysReplayMapper.countReplyByUserId(userId);
    }

    @Override
    public List<SysReplay> selectByUserId(Long userId) {
        return sysReplayMapper.selectByUserId(userId);
    }

}


