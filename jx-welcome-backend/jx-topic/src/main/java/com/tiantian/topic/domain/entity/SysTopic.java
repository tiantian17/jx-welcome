package com.tiantian.topic.domain.entity;

import com.tiantian.common.core.domain.BaseEntity;
import com.tiantian.common.validator.annotation.LevelCheck;
import com.tiantian.common.validator.annotation.Xss;
import lombok.*;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotEmpty;
import java.io.Serializable;
import java.util.List;

/**
 * (SysTopic)实体类
 *
 * @author tiantian
 * @since 2023-10-12 17:00:20
 */

@EqualsAndHashCode(callSuper = true)
@Data
@AllArgsConstructor
@NoArgsConstructor
public class SysTopic extends BaseEntity implements Serializable {
    private static final long serialVersionUID = -48118181694057959L;

    /**
     * 题目ID
     */
    private String topicId;

    /**
     * 出题人Id(userId)
     */
    private Long authorId;

    /**
     * 出题人真实姓名
     */
    private String authorName;

    /**
     * 题目标题
     */
    @NotBlank(message = "题目标题不能为空")
    private String title;

    /**
     * 题目难度(0简单 1中等 2困难)
     */
    @LevelCheck
    private String level;

    /**
     * 题目标签(可以多个)
     */
    @NotEmpty(message = "题目标签至少设置一个")
    private List<String> tags;

    /**
     * 题目详细信息
     */
    @NotBlank(message = "题目详细信息不能为空")
    private String detail;

    /**
     * 代码模板
     */
    private String codeTemplate;

    /**
     * 题目状态(0待审核 1审核未通过 2审核通过)
     */
    private String status;

    /**
     * 删除标记(0未删除 1已删除)
     */
    private String delFlag;
}

