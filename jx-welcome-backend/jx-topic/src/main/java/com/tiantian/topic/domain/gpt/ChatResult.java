package com.tiantian.topic.domain.gpt;

import lombok.Data;

import java.util.List;

@Data
public class ChatResult {

    private String id;

    private String model;

    private List<Choices> choices;

}