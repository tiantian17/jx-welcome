package com.tiantian.topic.mapper;

import com.tiantian.topic.domain.entity.SysTopic;
import com.tiantian.topic.domain.vo.SysTopicVO;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

/**
 * (SysTopic)表数据库访问层
 *
 * @author tiantian
 * @since 2023-10-12 17:00:20
 */
@Mapper
public interface SysTopicMapper {

    /**
     * 通过ID查询单条数据
     *
     * @param topicId 主键
     * @return 实例对象
     */
    SysTopic selectById(String topicId);

    /**
     * 通过实体作为筛选条件查询
     *
     * @param sysTopic 实例对象
     * @return 对象列表
     */
    List<SysTopic> selectList(SysTopic sysTopic);

    /**
     * 获取sysTopicVO列表
     * @param sysTopic 查询参数
     */
    List<SysTopicVO> selectTopicVOList(SysTopic sysTopic);

    /**
     * 新增数据
     *
     * @param sysTopic 实例对象
     * @return 影响行数
     */
    Boolean insertBatch(SysTopic sysTopic);

    /**
     * 修改数据
     *
     * @param sysTopic 实例对象
     * @return 影响行数
     */
    Boolean updateById(SysTopic sysTopic);

    /**
     * 通过主键删除数据
     *
     * @param topicId 主键
     * @return 影响行数
     */
    Boolean deleteById(String topicId, Long userId);

    int selectTopicTotal();
}


