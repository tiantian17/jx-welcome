package com.tiantian.topic.domain.vo;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDateTime;
import java.util.List;

/**
 * @author TianTian
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class SysJudgeDetailVO {

    private static final long serialVersionUID = 1L;

    /**
     * 答题人的ID
     */
    private String userId;

    /**
     * 判题ID
     */
    private String replyId;

    /**
     * 题目标题
     */
    private String title;

    /**
     * 题目难度
     */
    private String level;

    /**
     * 题目标签
     */
    private List<String> tags;

    /**
     * 题目详细信息
     */
    private String detail;

    /**
     * 题目答案
     */
    private String answer;

    /**
     * 单题分数
     */
    private Integer score;

    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private LocalDateTime updateTime;
}
