package com.tiantian.topic.mapper;

import com.tiantian.common.core.domain.entity.SysJudger;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

/**
 * (SysJudger)表数据库访问层
 *
 * @author tiantian
 */
@Mapper
public interface SysJudgerMapper {

    /**
     * 通过ID查询单条数据
     *
     * @param judgerId 主键
     * @return 实例对象
     */
    SysJudger selectById(Long userId);

    /**
     * 通过实体作为筛选条件查询
     *
     * @param sysJudger 实例对象
     * @return 对象列表
     */
    List<SysJudger> selectList(SysJudger sysJudger);

    /**
     * 新增数据
     *
     * @param sysJudger 实例对象
     * @return 影响行数
     */
    int insert(SysJudger sysJudger);


    /**
     * 批量更新
     */
    int updateJudgerBatch(List<SysJudger> sysJudgerList);

    int updateById(SysJudger sysJudger);

    /**
     * 通过主键删除数据
     *
     * @param judgerId 主键
     * @return 影响行数
     */
    int deleteJudgerById(String judgerId);


    Integer selectSubTotal();

    Integer selectNotSubTotal();
}


