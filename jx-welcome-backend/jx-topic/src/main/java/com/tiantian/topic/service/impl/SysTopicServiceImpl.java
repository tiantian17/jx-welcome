package com.tiantian.topic.service.impl;

import com.tiantian.topic.domain.entity.SysTopic;
import com.tiantian.topic.domain.vo.SysTopicVO;
import com.tiantian.topic.mapper.SysTopicMapper;
import com.tiantian.topic.service.SysTopicService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;


import java.util.List;

/**
 * SysTopic表服务实现类
 *
 * @author tiantian
 */
@RequiredArgsConstructor
@Service
public class SysTopicServiceImpl implements SysTopicService {

    private final SysTopicMapper sysTopicMapper;

    /**
     * 通过ID查询单条数据
     *
     * @param topicId 主键
     * @return 实例对象
     */
    @Override
    public SysTopic selectById(String topicId) {
        return sysTopicMapper.selectById(topicId);
    }


    /**
     * 根据条件查询
     *
     * @return 实例对象的集合
     */
    @Override
    public List<SysTopic> selectList(SysTopic sysTopic) {
        return sysTopicMapper.selectList(sysTopic);
    }

    /**
     * 根据条件查询
     *
     * @return 实例对象的集合
     */
    @Override
    public List<SysTopicVO> selectTopicVOList(SysTopic sysTopic) {
        return sysTopicMapper.selectTopicVOList(sysTopic);
    }

    /**
     * 新增数据
     *
     * @param sysTopic 实例对象
     * @return 实例对象
     */
    @Override
    public Boolean setTopic(SysTopic sysTopic) {
        return sysTopicMapper.insertBatch(sysTopic);
    }

    /**
     * 修改数据
     *
     * @param sysTopic 实例对象
     * @return 实例对象
     */
    @Override
    public Boolean updateById(SysTopic sysTopic) {
        return sysTopicMapper.updateById(sysTopic);
    }

    /**
     * 通过主键删除数据
     *
     * @param topicId 主键
     * @return 是否成功
     */
    @Override
    public Boolean deleteById(String topicId, Long userId) {
        return sysTopicMapper.deleteById(topicId, userId);
    }

    @Override
    public int getTopicTotal() {
        return sysTopicMapper.selectTopicTotal();
    }

}


