package com.tiantian.topic.domain.vo;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDateTime;

/**
 * 用于前端构建表格
 *
 * @author TianTian
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class SysJudgeVO {
    private static final long serialVersionUID = 1L;
    /**
     * 答题人用户ID
     */
    private Long userId;

    /**
     * 答题人学号
     */
    private String studentId;

    /**
     * 总分
     */
    private Integer totalScore;

    /**
     * 答题人真实姓名
     */
    private String nickName;

    /**
     * 判题人真实姓名
     */
    private String judger;

    /**
     * 已经提交的数量
     */
    private Integer subCount;

    /**
     * 答题人班级
     */
    private String clasz;

    /**
     * 评分状态 (0未评分 1评分完毕)
     */
    private String judgeStatus;

    /**
     * 是否为我需要判的题
     */
    private Boolean needMe;

    /**
     * 最近一次的更新时间
     */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private LocalDateTime updateTime;
}
