package com.tiantian.topic.domain.vo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

/**
 * @author TianTian
 */
@EqualsAndHashCode(callSuper = true)
@Data
@AllArgsConstructor
@NoArgsConstructor
public class SubInfoVO extends SysJudgeVO {
    /**
     * 提交状态 0未提交 1部分提交 2完全提交
     */
    private String subStatus;
}
