package com.tiantian.topic.service;


import com.tiantian.topic.domain.entity.SysReplay;

import java.util.List;

/**
 * (SysReplay)表服务接口
 *
 * @author tiantian
 */
public interface SysReplyService {

    /**
     * 通过ID查询单条数据
     *
     * @param replyId 主键
     * @return 实例对象
     */
    SysReplay selectById(String replyId);


    /**
     * 通过实体作为筛选条件查询
     *
     * @param sysReplay 实例对象
     * @return 对象列表
     */
    List<SysReplay> selectList(SysReplay sysReplay);

    /**
     * 新增数据
     *
     * @param sysReplay 实例对象
     * @return 新增标记
     */
    Boolean insert(SysReplay sysReplay);

    /**
     * 修改答案
     *
     * @param sysReplay 实例对象
     * @return 删除标记
     */
    Boolean changAnswer(SysReplay sysReplay);



    /**
     * 通过主键删除数据
     *
     * @param replyId 主键
     * @return 修改标记
     */
    Boolean deleteAdminReplys();

    /**
     * 检查是否已经提交过该题目
     *
     * @param topicId 题目ID
     * @param userId  用户ID
     * @return 行数
     */
    int checkReplyUnique(String topicId, Long userId);

    /**
     * 根据题目Id和用户Id获取该答题
     *
     * @param topicId 题目Id
     * @param userId  用户Id
     */
    String selectAnsByTopicIdAndUserId(String topicId, Long userId);

    /**
     * 根据用户Id查询答题记录
     *
     * @param userId 用户Id
     */
    List<SysReplay> selectByUserId(Long userId);

    /**
     * 根据id修改数据
     * @param sysReplay sysReplay
     * @return 影响行数
     */
    int updateByReplyId(SysReplay sysReplay);

    int countReply(Long userId);
}


